<?php

use Illuminate\Database\Seeder;

class FeedBackTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feedback_types')->insert([
            [
                'name' => 'Smile',
                'emotion_level_id' => 1,
            ],
            [
                'name' => 'Smile Special',
                'emotion_level_id' => 2,
            ],
            [
                'name' => 'Angry',
                'emotion_level_id' => 3,
            ],
            [
                'name' => 'Angry Special',
                'emotion_level_id' => 4,
            ]
        ]);
    }
}
