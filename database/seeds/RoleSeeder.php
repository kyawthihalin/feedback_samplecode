<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Merchant',
                'guard_name' => 'web'
            ],
            [
                'name' => 'Analysis',
                'guard_name' => 'web',
            ],
            [
                'name' => 'Hotel',
                'guard_name' => 'web',
            ]
        ]);
    }
}
