<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Naung Ye Htet',
                'email' => 'naungyehtet@admin.com',
                'country_id' => 1,
                'industry_id' => 1,
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Pyae Shyan',
                'email' => 'pyaeshyan@admin.com',
                'country_id' => 1,
                'industry_id' => 1,
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Kyaw Thiha Lin',
                'email' => 'kyawthihalin@admin.com',
                'country_id' => 1,
                'industry_id' => 1,
                'password' => bcrypt('secret'),
            ],
            [
                'name' => 'Ei',
                'email' => 'ei@admin.com',
                'country_id' => 1,
                'industry_id' => 1,
                'password' => bcrypt('secret'),
            ]
        ]);
    }
}
