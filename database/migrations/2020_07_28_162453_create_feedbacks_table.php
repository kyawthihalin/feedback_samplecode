<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('question_id')->nullable();
            $table->unsignedBigInteger('feedback_type_answer_id')->nullable();
            $table->unsignedBigInteger('preanswer_id')->nullable();
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('sub_branch_id')->nullable();
            $table->text('other')->nullable();
            $table->time('feedback_time')->nullable();
            $table->date('feedback_date')->nullable();
            $table->unsignedBigInteger('feedback_type_id')->nullable();

            $table->index('question_id');
            $table->index('sub_branch_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
