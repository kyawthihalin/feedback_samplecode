<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->integer('country_id')->nullable();
            $table->integer('industry_id')->nullable();
            $table->integer('weekly_report')->default(0);
            $table->integer('daily_report')->default(0);
            $table->integer('smily_notification')->default(0);
            $table->integer('parent_id');
            $table->integer('first_login')->default(1);
            $table->integer('status')->default(1);
            $table->datetime('activated_at')->nullable();
            $table->datetime('deactivated_at')->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('change_password')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->string('password');
            $table->rememberToken();

            $table->index(['id', 'activated_at', 'deactivated_at']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
