@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">

@endsection
@section('content')

	<div class="page-body">
		<div class="card">
            <div class="card-header">{{ __('Change Password') }}</div>
            <div class="card-body">
                <form id="change-password" method="POST" action="{{ route('first.change-password') }}">
                	@csrf
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
                            @error('password')
		                     	<span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
		                    @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" required>
                            @error('confirm_password')
		                     	<span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
		                    @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                    	<div class="col-md-6 offset-md-4">
	                    	<input type="checkbox" name="terms_and_condition" class="@error('terms_and_condition') is-invalid @enderror">
	                    	<label for="terms_and_condition"> 
	                    		<a href="{{route('terms-and-condition')}}" target="_blank">
		                    		Here is terms and conditon
	                    		</a>
	                    	</label>
	                    	@error('terms_and_condition')
		                     	<span class="invalid-feedback" role="alert">
                                    <strong>Accept Terms and condition. </strong>
                                </span>
		                    @enderror
                    	</div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button class="btn btn-primary" id="save-btn" type="submit">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</div>

@endsection
@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>

<script>

</script>