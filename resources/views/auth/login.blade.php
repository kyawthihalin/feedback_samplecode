{{-- @extends('layouts.app') --}}

{{-- @section('content') --}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="Like Metric">
    <meta name="author" content="pyaeshyanphyoaung@gmail.com">
    <link rel="icon" href="../assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon.png" type="image/x-icon">
    <title>Likemetric</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
     <link rel="stylesheet" type="text/css" href="{{asset('assets/css/scrollable.css')}}">
    <!-- Plugins css start-->
    @yield('css')
   
  
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style-login.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
  </head>
  <body>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="page-wrapper">
    <div class="container-fluid p-0">
      <!-- login page start-->
      <div class="authentication-main no-bg">
        <div class="row">
            <div class="col-12 text-center">
              <img class="img-fluid pb-2" src="../assets/images/logo/logo.png" alt=""></a>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="auth-innerright">
              <div class="authentication-box">
                {{--  --}}
                {{--  --}}
                <div class="mt-4">
                  @if (session('status'))
                    <div>
                      <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin:auto; width:59%; border-radius: 10px;">
                          {{ session('status') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                    </div>
                  @endif
                  <div class="card-body">
                    <div class="cont shadow">
                      <div> 
                        <form  class="theme-form" method="POST" action="{{ route('login') }}">
                            @csrf
                          <h4>LOGIN</h4>
                          <p class="pb-2">Enter your Username and Password</p>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" required placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="form-group">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1" name="password" required placeholder="Password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                              </div>
                            </div>
                            <div class="checkbox col-md-12">
                              <input id="checkbox1" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                              <label for="checkbox1">Remember me</label>
                            </div>
                            <div class="col-md-4 pl-4">
                              <div class="form-group row mt-3 mb-0">
                                <button class="btn btn-primary btn-block" type="submit">LOGIN</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>  
                      <div class="sub-cont d-none d-sm-block">
                        <div class="img">
                          <div class="img__text m--in">
                            <h5>Login ?</h5>
                            <p>Use your Likemetric Account </p>
                          </div>
                          <div class="img__text m--up">
                            <h5>Forgot your password ?</h5>
                            <p>Don't worry! Just fill in your email and we'll send you a link to reset your password.</p>
                          </div>
                          <div class="img__btn"><span class="m--up">Forgot password?</span><span class="m--in">Login</span></div>
                        </div>
                        <div>
                          <form class="theme-form py-5 mt-3" method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <h4>Forgot your password ?</h4>
                            <p>Enter your email and we'll send you a link to reset your password</p>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group">
                                  <input class="form-control @error('email') is-invalid @enderror" type="email" placeholder="Email Address" name="email" value="{{ old('email') }}">
                                  @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-5">
                                <button class="btn btn-primary" type="submit">Reset password</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- login page end-->
    </div>
  </div>
  <!-- latest jquery-->
  <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
  <!-- Bootstrap js-->
  <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
  <!-- feather icon js-->
  <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
  <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>
  <!-- Sidebar jquery-->
  <script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
    <script src="{{asset('assets/js/login.js')}}"></script>
    <script src="{{asset('assets/js/config.js')}}"></script>
  <script src="{{asset('assets/js/scrollable/scrollable-custom.js')}}"></script>
</body>
</html>
{{-- @endsection --}}
