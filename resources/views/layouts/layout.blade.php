<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="LikeMetric">
    <meta name="author" content="pyaeshyanphyoaung@gmail.com">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>Likemetric</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/scrollable.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">

    <!-- Plugins css start-->
    @yield('css')
   
  
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Header Start-->
      <div class="page-main-header">
        <div class="main-header-right row p-0">
          <div class="main-header-left">
            <div class="logo-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('assets/images/logo/logo.png')}}" alt=""></a></div>
          </div>
          <div class="toggle-sidebar ml-5"><i class="status_toggle middle" data-feather="grid" id="sidebar-toggle"></i></div>
          <div class="nav-right col right-menu">
            <ul class="nav-menus">
            @if(Route::currentRouteName() == 'analysis' || Route::currentRouteName() == 'quick-view')
            @if (!auth()->user()->isHotel())    
            <li class="onhover-dropdown">
                <div>
                  <label for="">Location/Area</label>
                  @if (auth()->user()->isAnalysis())
                  @php
                    $analysiBranchArr = auth()->user()->analysisBranch->pluck('id')->toArray();
                    $analysiSubBranchArr = auth()->user()->analysisSubbranch->pluck('id')->toArray();
                  @endphp
                  <select class="form-control form-control-sm branch" id="branchId">
                    <option value="default">Select Location/Area</option>
                    @foreach ($branchs as $branch)
                    @if (in_array($branch->id ,$analysiBranchArr) && !empty(array_intersect($analysiSubBranchArr, $subbranchArr)))
                    <optgroup label="{{ $branch->name }}">
                      @if (in_array($branch->id ,$analysiBranchArr))  
                      <option value='all{{ $branch->id }}'
                        @if ($branchId == 'all'.$branch->id)
                        selected
                        @endif  
                        >All Subranch for {{ $branch->name }}</option>
                      @endif
                      @foreach ($branch->subBranch as $subBranch)
                      @if (in_array($subBranch->id ,$analysiSubBranchArr))
                      <option value="{{ $subBranch->id }}"
                        @if ($branchId == $subBranch->id)
                        selected
                        @endif  
                        > {{ $subBranch->name }} </option>
                        @endif
                      @endforeach
                    </optgroup>
                    @endif    
                    @endforeach
                  </select>
                  @else    
                  <select class="form-control form-control-sm branch" id="branchId">
                    <option value="default">Select Location/Area</option>
                    @foreach ($branchs as $branch)    
                    <optgroup label="{{ $branch->name }}">
                      <option value='all{{ $branch->id }}'
                      @if ($branchId == 'all'.$branch->id)
                        selected
                      @endif  
                      >All Subranch for {{ $branch->name }}</option>
                      @foreach ($branch->subBranch as $subBranch)
                      <option value="{{ $subBranch->id }}"
                      @if ($branchId == $subBranch->id)
                        selected
                      @endif  
                      > {{ $subBranch->name }} </option>
                      @endforeach
                    </optgroup>
                    @endforeach
                  </select>
                  @endif
                </div>
            </li>
            <li>
              <div>
                <label>Survey Question</label>
                <select class="form-control form-control-sm question" id="question" onchange="changeResult(
                  )">
                  <option value="all">Select Question</option>
                  @if ($questions)    
                  @foreach ($questions as $question)
                  <option value="{{$question->id}}"
                  @if ($questionId == $question->id)
                      selected
                  @endif  
                  >{{ $question->question_text }}</option>
                  @endforeach
                  @endif
                </select>
              </div>                  
            </li>
            <li>
              <!-- <div>
                <label for="">Time Period </label>
                <select class="form-control form-control-sm date" id="bootstrap-notify-enter" onchange="changeResult(
                  )">
                  <optgroup>
                    <option value="yesterday" 
                    @if ($date == "yesterday")
                        selected
                    @endif
                    >Yesterday </option>
                    <option value="last-week"
                    @if ($date == "last-week")
                        selected
                    @endif
                    >Last Week </option>
                    <option value="last-month"
                    @if ($date == "last-month")
                        selected
                    @endif
                    >Last Month </option>
                  </optgroup>
                </select>
              </div>  -->

              <div class="theme-form">
                <div class="form-group">
                    <label for="">Time Period </label>
                    <input class="form-control dataRange" id="time-period" value="" type="text">
                </div>
              </div>
            </li>

            @else
            <li>
              <div class="p-0">
                <label>Survey Question</label>
                <select class="form-control form-control-sm question" id="question" onchange="changeResultHotel(
                  )">
                  <option value="all">Select Question</option>
                  @if ($questions)    
                  @foreach ($questions as $question)
                  <option value="{{$question->id}}"
                  @if ($questionId == $question->id)
                      selected
                  @endif  
                  >
                  @if(is_array(json_decode($question->question_text)))
                  @foreach (json_decode($question->question_text) as $key=>$text)
                      {{$text}}
                      @if (count(json_decode($question->question_text)) > 1 && $key != count(json_decode($question->question_text)) - 1)
                          {{ ', ' }}
                      @endif
                  @endforeach
                  @else
                  {{$question->question_text}}
                  @endif
                  </option>
                  @endforeach
                  @endif
                </select>
              </div>                  
            </li>
            <li>
              <div class="theme-form p-0">
                <div class="form-group">
                    <label for="">Time Period </label>
                    <input class="form-control dataRange" id="time-period-hotel" value="" type="text">
                </div>
              </div>
            </li>
            @endif  
               
            @endif
              <li class="onhover-dropdown py-4">
                 <div class="media profile-media">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                  </a>
                </div>
                <ul class="profile-dropdown onhover-show-div">
                  <li> 
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                  </li>
                  <li>
                  </li>
                </ul>

              <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  {{ Auth::user()->name }}
              </a>

              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                  </form>
              </div> -->
              </li>
              <li><a class="text-dark" href="#!" onclick="javascript:toggleFullScreen()"><i data-feather="maximize"></i></a></li>
            </ul>
          </div>
          <div class="d-lg-none mobile-toggle pull-right"><i data-feather="more-horizontal"></i></div>
        </div>
      </div>
      <!-- Page Header Ends -->
      <!-- Page Body Start-->
      <div class="page-body-wrapper sidebar-icon">
        <!-- Page Sidebar Start-->
        <header class="main-nav">
          <div class="logo-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('assets/images/logo/logo.png')}}" alt=""></a></div>
          <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt=""></a></div>
          <nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Dashboard</span></a>
                    <ul class="nav-submenu menu-content">
                      @if (auth()->user()->isAdmin())
                      <li><a href="/admin/dashboard">Analytics</a></li>
                      @endif
                      @if (auth()->user()->isMerchant() || auth()->user()->isAnalysis() || auth()->user()->isHotel())
                      <li><a href="/admin/quick-view">Quick View</a></li>
                      @if (!auth()->user()->isHotel())
                      <li><a href="/admin/analysis">Analytics</a></li>
                      @endif
                      @endif
                    </ul>
                  </li>
                  @if (auth()->user()->isMerchant() || auth()->user()->isHotel())    
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="box"></i><span>Create Survey</span></a>
                    <ul class="nav-submenu menu-content">
                     @if(auth()->user()->isHotel() || auth()->user()->isAdmin())
                      <li><a href="/admin/create-template">Add Question</a></li>
                      <li><a href="/admin/template">Question List</a></li>
                      @endif
                      <li><a href="/admin/survey">Survey List</a></li>
                      <li><a href="/admin/plansurvey">Survey Plan</a></li>
                      @if(!auth()->user()->isHotel())
                      <li><a href="/admin/preanswers">Customize Survey</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif

                  @if (auth()->user()->isAdmin())  
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="box"></i><span>Create Point</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/branch">Experience Point</a></li>
                      <li><a href="/admin/sub-branch">Subgroup</a></li>
                    </ul>
                  </li>
                  @elseif(auth()->user()->isMerchant())
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="box"></i><span>Manage Point</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/branch">Experience Point</a></li>
                      <li><a href="/admin/sub-branch">Subgroup</a></li>
                    </ul>
                  </li>
                  @endif

                  <!-- <li class="drpopdown"><a class="nav-link menu-title" href="#"><i data-feather="box"></i><span>Manage Authentication</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/account">Users</a></li>
                      <li><a href="/admin/role">Roles</a></li>
                      <li><a href="/admin/permission">Permissions</a></li>
                    </ul>
                  </li> -->

                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="box"></i><span>Manage Authentication</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/account">Users</a></li>
                      @if (auth()->user()->isMerchant() || auth()->user()->isHotel())  
                      <li><a href="/admin/analysis-user">Analysis User</a></li>
                      @endif
                      @if (auth()->user()->isAdmin())
                      <li><a href="/admin/role">Roles</a></li>
                      @endif
                      {{-- <li><a href="/admin/permission">Permissions</a></li> --}}
                    </ul>
                  </li>

                  @if (auth()->user()->isAdmin())    
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="settings"></i><span>Manage Setting</span></a>
                    <ul class="nav-submenu menu-content">
                      <li class="active"><a href="/admin/country">Country</a></li>
                      <li><a href="/admin/industry">Industry</a></li>
                      <li><a href="/admin/create-template">Create Template</a></li>
                      <li><a href="/admin/template">Template List</a></li>
                    </ul>
                  </li>
                  @endif

                  @if (auth()->user()->isMerchant())
                  <li><a class="nav-link menu-title" href="#"><i data-feather="settings"></i><span>Help</span></a>
                    <ul class="nav-submenu menu-content">
                      <li class="active"><a href="/admin/support">Raise Support
                      </a></li>
                      <li class="active"><a href="#">Tour Dashboard</a></li>
                    </ul>
                  </li>
                  @endif
                  
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
        </header>
        <!-- Page Sidebar Ends-->
         
            @yield('content')
    
        <!-- footer start-->
        <footer class="footer">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 footer-copyright">
                <p class="mb-0">Copyright {{date('Y')}} © Likemetric All rights reserved.</p>
              </div>
              <div class="col-md-6">
               
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- latest jquery-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/polyfills.umd.js"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>

    <!-- Datarange js-->
    <script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
    <script src="{{asset('assets/js/tooltip-init.js')}}"></script>

    <!-- feather icon js-->
    <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>

    <!-- Sidebar jquery-->
    <script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
    <script src="{{asset('assets/js/config.js')}}"></script>
    <script src="{{asset('assets/js/scrollable/scrollable-custom.js')}}"></script>

    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/8964652.js"></script>

    <!-- Plugins JS start-->
    @yield('js')
   
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="{{asset('assets/js/script.js')}}"></script>
    <!-- login js-->
    <!-- Plugin used-->

    <script>

      var date = '';
      @if(Route::currentRouteName() == 'analysis' || Route::currentRouteName() == 'quick-view')
      var start = <?php echo !auth()->user()->isAdmin() ? json_encode($startDateStr) : ''; ?>;
      var end = <?php echo !auth()->user()->isAdmin() ? json_encode($endDateStr) : ''; ?>;
      @endif
      var startDate;
      var endDate;

      $('#branchId').change(function(){
        var branchId = $('.branch').val();

        $.ajax({
          url:'get-question?branch='+ branchId,
          type: 'get',
          async: false,
          success: function (responese) {
            $('#question').html('');
            var toAppend = '<option value="all">Select Question</option>';
              $.each(responese.data,function(i,o){
              toAppend += '<option value='+ o.id +'>'+o.name+'</option>';
              });

            $('#question').append(toAppend);
          },
          error: function (responese) {
            
          }
        });
      });

      function cb(start, end) {
          date = start.format('MM/D/YYYY') + ' - ' + end.format('MM/D/YYYY');
          changeResult();
      }

      function cbHotel(start, end) {
          date = start.format('MM/D/YYYY') + ' - ' + end.format('MM/D/YYYY');
          changeResultHotel();
      }

      function changeResult()
      {
        let urlOrigin = window.location.href;
        let urlNoparam = urlOrigin.substring(0, urlOrigin.indexOf('?'))
        let urlParam = urlNoparam + '?';
        let branch = $('.branch').val();
        let question = $('.question').val();
        if(branch == 'all') {
          swal({
              position: 'top-end',
              icon: 'warning',
              title: 'Choose location or area.',
              showConfirmButton: false,
          })
        } else if(question == 'all') {
          swal({
              position: 'top-end',
              icon: 'warning',
              title: 'Select question.',
              showConfirmButton: false,
          })
        } else {
        url = urlParam + 'branch=' + branch + '&question=' + question + '&date=' + date;
        
        window.location.href = url;
        }

      }

      function changeResultHotel()
      {
        let urlOrigin = window.location.href;
        let urlNoparam = urlOrigin.substring(0, urlOrigin.indexOf('?'))
        let urlParam = urlNoparam + '?';
        let question = $('.question').val();
        if(question == 'all') {
          swal({
              position: 'top-end',
              icon: 'warning',
              title: 'Select question.',
              showConfirmButton: false,
          })
        } else {
        url = urlParam + 'question=' + question + '&date=' + date;
        
        window.location.href = url;
        }

      }

      // var start = moment().subtract(0, 'days');
      // var end = moment();

        // function cb(start, end) {
        //     date = start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
        //     console.log(date);
        //     this.changeResult();
        // }

        $('#time-period').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
        }, cb);

        $('#time-period-hotel').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            },
        }, cbHotel);
        // cb(start, end);
    </script>

  <script>
    window.intercomSettings = {
      app_id: "xscv1ect"
    };
  </script>

  <script>
  // We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/xscv1ect'
  (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/xscv1ect';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
  </script>
  </body>
</html>
