@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
@endsection
@section('content')
<!-- Container-fluid starts-->
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Customize survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item active">Customize survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <form class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Add New Customize survey</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="theme-form">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label text-right"></label>
                                            <div class="col-xl-5 col-sm-7 col-lg-8">
                                                <input class="form-control" id="preName" type="text" placeholder="Enter Customize Survey">
                                                <small style="color: red;" id="errorName"></small>
                                            </div>
                                            <div class="col-md-3">
                                                <button class="btn btn-primary" id="addAnswer" type="submit">Create</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Customize survey List</h4>
                            <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                        </div>
                        <div class="table-responsive add-project">
                            <table class="table card-table table-vcenter text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Answers</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($answers as $answer)
                                        <tr>
                                            <td>{{$answer->description}}</td>
                                            <td>
                                            <a href="#">
                                                <span class="badge badge-warning badge-pill" id="editAns{{$answer->id}}" onclick="editAns('{{$answer->id}}')">Edit</span>
                                            </a>
                                            {{-- <a href="#">
                                                <span class="badge badge-danger badge-pill" id="deleteAns{{$answer->id}}" onclick="deleteAns('{{$answer->id}}')">Delete</span>
                                            </a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="edit-profile" id="edit">

        </div>

    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('js')

<script src="{{asset('assets/js/datepicker/date-time-picker/datetimepicker.custom.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-time-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-time-picker/tempusdominus-bootstrap-4.min.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script>
    $('#addAnswer').click(function(event) {
        event.preventDefault();
        let name = $('#preName').val();
        let token = '{{csrf_token()}}';
        $.ajax({
            url: '{{route('savePreAns')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                name,
            },
            success: function(responese) {
                swal({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Answer Successfully Added',
                    showConfirmButton: false,
                }).then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
                })
            },
            error:function(response){
                if(response.responseJSON.errors){
                    if(response.responseJSON.errors.name){
                        $('#errorName').text('Answer is Required');
                        $('#preName').css('border-color','red');
                    } else{
                        $('#errorName').text('');
                        $('#preName').css('border-color','black');
                    }
                }
            }
        });
    });

    let deleteAns = (id) =>{
	let token = '{{csrf_token()}}';
    swal({
        title: "Are you sure?",
        text: "This Answer will be deleted from your answer list.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
          url:'{{route('deleteAnswer')}}',
          type: 'POST',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Answer Successfully Deleted',
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          },
          error: function (responese) {
            swal("There is an error occoured!");
          }
          }); 
        } else {
            swal("Your branch is safe!");
        }
    })
};
let editAns = (id) => {
  let token = '{{csrf_token()}}';
  $.ajax({
    url:'{{route('editAnswer')}}',
    type: 'POST',
    async: false,
    headers: {
      'X-CSRF-Token': token 
      },
    data: {
      id
    },
    success: function (responese) {
        $('#edit').html(responese)
        $('#add').hide()
    },
    }); 
};
</script>
@endsection