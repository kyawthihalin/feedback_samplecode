<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>Likemetric</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <style>
   .counter{
        color: rgba(115,102,255,0.15);
        background: linear-gradient(to right, #7366ff 10%, #a927f9 100%);
        margin: 5px;
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
    svg{
        width: 16px;
        margin-right: 10px;
        vertical-align: bottom;
        float: none;
        stroke-width: 2px;
    }
  </style>
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Body Start-->
        <div class="container-fluid p-0">
            <div class="card">
                <div class="card-body">
                    <div class="row text-center" style="padding: 60px 0px;">
                        <div class="col-12">
                            <?php

use App\Question;
use Vinkla\Hashids\Facades\Hashids;

$questionId = Hashids::connection()->decode(request()->segment(2))[0];
$question = Question::find($questionId);
$survey_id = $question->survey_id;
$questions = $question->survey->questions->pluck('question_text', 'id')->toArray();
?>

                            <span class="rounded-circle mb-4 counter float-right">
                                <span class="currentCount">1</span>/{{count($questions)}}
                            </span>
                        </div>
                        <div class="col-12">
                            <?php $i = 0;?>
                            @foreach($questions as $key => $text)
                            <div class="question question-{{$i}}"><h4>{{$text}}</h4>
                                <div class="avatars py-5">
                                    <div class="avatar" style="cursor: pointer;">
                                        <img class="img-50 rounded-circle emoji" data-key="{{$i}}" data-question="{{$key}}" data-id="1" src="{{asset('assets/images//smile/emoji4.png')}}" alt="#">
                                    </div>
                                    <div class="avatar" style="cursor: pointer;">
                                        <img class="img-50 rounded-circle emoji" data-key="{{$i}}" data-question="{{$key}}" data-id="2" src="{{asset('assets/images/smile/emoji3.png')}}" alt="#">
                                    </div>
                                    <div class="avatar" style="cursor: pointer;">
                                        <img class="img-50 rounded-circle emoji" data-key="{{$i}}" data-question="{{$key}}" data-id="3" src="{{asset('assets/images//smile/emoji2.png')}}" alt="#">
                                    </div>
                                    <div class="avatar" style="cursor: pointer;">
                                        <img class="img-50 rounded-circle emoji" data-key="{{$i}}" data-question="{{$key}}" data-id="4" src="{{asset('assets/images//smile/emoji1.png')}}" alt="#">
                                    </div>
                                </div>
                            </div>
                            <?php $i++;?>
                            @endforeach
                        </div>

                        <div class="col-md-12 float-right">
                            <a href="#" class="back btn btn-light"><i class="fa fa-arrow-circle-o-left"></i> Back</a>
                            <a href="#" class="skip btn btn-light">Next <i class="fa fa-arrow-circle-o-right"></i></a>
                            <a href="#" class="finish btn btn-dark"><i class="fa fa-check-circle-o"></i> Finish</a>
                        </div>



                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <div>Power By <img  src="{{asset('assets/images/logo/logo.png')}}" alt="Likemetric" style="max-width: 30% !important;"> </div>
                </div>
            </div>
        </div>
    </div>
    <!-- latest jquery-->
    <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.ui.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
    <!-- feather icon js-->
    <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- Plugins JS Ends-->
    <script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert/app.js')}}"></script>
    <script src="{{asset('assets/js/tooltip-init.js')}}"></script>
    <!-- Theme js-->
    <script src="{{asset('assets/js/script.js')}}"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <script>
    Object.size = function(obj) {
        var size = 0,
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    $(document).ready(function() {
        var questionArr = [];
        var currentQKey = 0;
        var question_list = @json($questions);
        var url = "{{url('feedback', [request()->segment(2), request()->segment(3)])}}";

        var i = 0;
        $.each(question_list, (index, value) => {
            if(i){
                $(".question-" + i).hide();
            }
            i++;
        });

        $(".back").hide();
        $(".finish").hide();


        function submitFeedback(){
            var survey_id = '<?php echo $survey_id; ?>';
            var token = '{{csrf_token()}}';

            if(questionArr.filter(n => n) < 1){
                swal({
                    position: 'top-end',
                    icon: 'warning',
                    title: 'Submitted survey is empty!',
                    button: false,
                })

                window.location = url;
            }
            else{
                $.ajax({
                url:'{{url("feedback")}}',
                type: 'POST',
                data: {
                    isHotel: 1,
                    survey_id,
                    questions: questionArr
                },
                async: false,
                headers: {
                'X-CSRF-Token': token
                },
                    success: function (response) {
                        swal({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your feedback has been successfully submitted',
                            showConfirmButton: false,
                        })

                        window.location = url;
                    },
                    error: function(error) {
                        swal({
                            position: 'top-end',
                            icon: 'warning',
                            title: 'Something went wrong!',
                            button: false,
                        })

                        window.location = url;
                    }
                });
            }
        }

        $(".emoji").click(function(){
            var emojiId = $(this).data('id');
            var question = $(this).data('question');
            currentQKey = $(this).data('key');
            $(".question-" + currentQKey).hide();
            currentQKey++;
            $(".question-" + currentQKey).show();

            if(currentQKey){
                $(".back").show();
                $(".finish").show();
            }


            $(".currentCount").text(currentQKey+1);

            questionArr.push({
                'feedback_type_id': emojiId,
                'question_id': question
            })

            if(currentQKey === Object.size(question_list)-1){
                $(".skip").hide();
            }

            if(currentQKey === Object.size(question_list)){
                $(".skip").hide();
                submitFeedback();
            }
        });

        $(".back").click(function(){
            questionArr.splice(-1,1);
            $(".question-" + currentQKey).hide();
            $(".currentCount").text(currentQKey);

            currentQKey--;
            $(".question-" + currentQKey).show();

            if(currentQKey < Object.size(question_list)-1){
                $(".skip").show();
            }

            if(!currentQKey){
                $(".back").hide();
                $(".finish").hide();
            }
        });

        $(".skip").click(function(){
            $(".question-" + currentQKey).hide();
            currentQKey++;
            $(".question-" + currentQKey).show();

            if(currentQKey){
                $(".back").show();
                $(".finish").show();
            }

            if(currentQKey === Object.size(question_list)-1){
                $(".skip").hide();
            }

            $(".currentCount").text(currentQKey+1);

            questionArr.push(null);
        });

        $(".finish").click(function(){
            var left = Object.size(question_list) - currentQKey;
            while(left > 0){
                questionArr.push(null);
                left--
            }
            submitFeedback();
        });
    });
</script>
  </body>
</html>