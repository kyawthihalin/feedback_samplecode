<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>Likemetric</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/sweetalert2.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
    <style>
        #count-down{
            background-color: rgba(115,102,255,0.15);
            background: linear-gradient(to right, #7366ff 10%, #a927f9 100%);
            margin: 5px;
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;

        }
        .title{
            border: 1px solid #ffffff;
            border-radius: 2px;
            outline: none;
            background-color: #ffffff;
        }

        button:focus{
          outline: none;
        }

        input {
          border-top-style: hidden;
          border-right-style: hidden;
          border-left-style: hidden;
          border-bottom-style: groove;
          line-height: 29px;
        }

        input#other:focus {
          outline: none;
        }
        .clicked{
            border-color: #dbdbdb;
        }

    </style>
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <?php
use App\Question;
use App\SubBranch;
use Vinkla\Hashids\Facades\Hashids;

// Obtain the default connection instance
$questionId = Hashids::connection()->decode(request()->segment(2))[0];
$question = Question::find($questionId);
$subBranchId = null;
$arr = Hashids::connection()->decode(request()->segment(3));
$branch_id = null;
if (count($arr)) {
    $subBranchId = $arr[0];
    $branch_id = optional(SubBranch::find($arr[0]))->branch_id;
}

?>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Body Start-->
      <div class="container-sm" style="padding: 60px 0px;">
        <div class="card">
          <div class="card-body">
            <div class="row text-center">
              <div class="col-12">
                <div id="count-down" class="rounded-circle float-right">60s</div>
              </div>
              <div class="col-12 align-self-center">
                <h5 class=" mb-4">Would you like to chat about your experience?</h5>
                <div class="avatars pt-3">
                    <input type="hidden" id="preid">
                    @if($question->type == "custom")
                    @foreach($question->preAnswers as $preAnswer)
                    <!-- <div class="avatar"><img src="{{asset('assets/images/smile/f1.png')}}" alt=""><p class="title">{{$preAnswer->description}}</p></div> -->
                    <div class="avatar mb-3">
                        <button class="title title-main btn btn-light" style="cursor: pointer;" id="{{$preAnswer->id}}" data-preid="{{$preAnswer->id}}">
                        {{$preAnswer->description}}
                        </button>
                    </div>
                    @endforeach
                    @else
                    @foreach($question->template->preAnswers as $preAnswer)
                      <div class="avatar title mb-4 title-main" style="cursor: pointer;" id="{{$preAnswer->id}}" data-preid="{{$preAnswer->id}}">
                        <img src="{{url ($preAnswer->photo)}}" alt="" width="100">
                        <button class="d-flex btn btn-light mt-3">
                          {{$preAnswer->description}}
                        </button>
                      </div>
                    @endforeach
                    @endif
                    <div class="avatar">
                        <button class="title btn btn-light other" style="cursor: pointer;">
                        Something Else?
                        </button>
                    </div>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-center py-3">
              <input class="w-50" id="other" type="text" placeholder="Share your experience" required>
            </div>
            <div class="d-flex justify-content-center pb-4">
              <button class="btn btn-dark sweet-1" id="submit" type="button">Submit</button>
            </div>
            </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <div>Power By <img  src="{{asset('assets/images/logo/logo.png')}}" alt="Likemetric" style="max-width: 30% !important;"> </div>
          </div>
        </div>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
    <!-- feather icon js-->
    <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- Plugins JS start-->
    <script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert/app.js')}}"></script>
    <script src="{{asset('assets/js/tooltip-init.js')}}"></script>
    <!-- Theme js-->
    <script src="{{asset('assets/js/script.js')}}"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <script>
    $(document).ready(function() {
        $('#other').hide();
        var isHotel = '<?php echo $isHotel; ?>';
        var timeLeft = 60;
        var elem = document.getElementById('count-down');
        var url = "{{url('feedback', [request()->segment(2), request()->segment(3)])}}";

        var timerId = setInterval(countdown, 1000);

        function countdown() {
          if (timeLeft == -1) {
          clearTimeout(timerId);

          window.location = url;

          } else {
              elem.innerHTML = timeLeft + 's';
              timeLeft--;
          }
        }

        $(".other").click(function(){
          $('#other').toggle("fast");
          $("#preid").val("");

          var elems = document.querySelectorAll(".title.clicked");
            $(this).toggleClass('clicked');

            [].forEach.call(elems, function(el) {
                el.classList.remove("clicked");
            });

        });

        $(".title-main").click(function(){
            $('#other').hide('fast');
            var preid = $(this).data('preid');
            $("#preid").val(preid);

            var elems = document.querySelectorAll(".title.clicked");

            [].forEach.call(elems, function(el) {
                el.classList.remove("clicked");
            });

            $("#"+ preid).addClass('clicked');
        });

        $("#submit").click(function(){
            var preanswer_id = $("#preid").val();
            var other = $("#other").val();

              var question_id = '<?php echo $questionId; ?>';
              var branch_id = '<?php echo $branch_id; ?>';
              var sub_branch_id = '<?php echo $subBranchId; ?>';

              var feedback_type_id = "{{request()->emojiLevel}}";
              var token = '{{csrf_token()}}';

              $.ajax({
              url:'{{url("feedback")}}',
              type: 'POST',
              data: {
                  isHotel: 0,
                  preanswer_id,
                  question_id,
                  sub_branch_id,
                  branch_id,
                  feedback_type_id,
                  other
              },
              async: false,
              headers: {
              'X-CSRF-Token': token
              },
              success: function (response) {
                  swal({
                      position: 'top-end',
                      icon: 'success',
                      title: 'Your feedback has been successfully submitted',
                      showConfirmButton: false,
                  })

                  window.location = url;
              },
            });
        });

        function disableBack(){
          window.history.forward()
        }

        window.onload = disableBack();
        window.onpageshow = function(evt) {
          if(evt.persisted){
            disableBack();
          }
        }
    });
</script>
  </body>
</html>