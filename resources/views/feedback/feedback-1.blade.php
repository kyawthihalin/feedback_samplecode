<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <title>Likemetric</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/feather-icon.css')}}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
  </head>
  <body>
    <!-- Loader starts-->
    <div class="loader-wrapper">
      <div class="loader-index"><span></span></div>
      <svg>
        <defs></defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
          <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo">    </fecolormatrix>
        </filter>
      </svg>
    </div>
    <!-- Loader ends-->
    <!-- page-wrapper Start-->
    <div class="page-wrapper compact-wrapper" id="pageWrapper">
      <!-- Page Body Start-->
      <div class="container-sm" style="padding: 60px 0px;">
        <div class="row text-center">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <?php

                  use App\Question;
                  use Vinkla\Hashids\Facades\Hashids;

                  $questionId = request()->segment(2);
                  $subBranchId = request()->segment(3);
                  $question = Question::find(Hashids::connection()->decode(request()->segment(2))[0]);
                ?>
                @if($question->type == "custom")
                  <h5>{{$question->question_text}}</h5>
                @else
                <h5> {{$question->template->question_text}}</h5>
                @endif
                <form action="{{url('feedback2', [$questionId, $subBranchId])}}" method="POST" id="emojiForm">
                @csrf
                  <div class="avatars py-3">
                      <input type="hidden" name="emojiLevel" id="emojiLevel">
                      <div class="avatar" style="cursor: pointer;"><img class="img-50 rounded-circle emoji" data-id="1" src="{{asset('assets/images//smile/emoji4.png')}}" alt="#"></div>
                      <div class="avatar" style="cursor: pointer;"><img class="img-50 rounded-circle emoji" data-id="2" src="{{asset('assets/images/smile/emoji3.png')}}" alt="#"></div>
                      <div class="avatar" style="cursor: pointer;"><img class="img-50 rounded-circle emoji" data-id="3" src="{{asset('assets/images//smile/emoji2.png')}}" alt="#"></div>
                      <div class="avatar" style="cursor: pointer;"><img class="img-50 rounded-circle emoji" data-id="4" src="{{asset('assets/images//smile/emoji1.png')}}" alt="#"></div>
                  </div>
                </form>
              </div>
            </div>  
            <div class="col-12">
              <div>Power By <img  src="{{asset('assets/images/logo/logo.png')}}" alt="Likemetric" style="max-width: 30% !important;"> </div>
            </div>
          </div> 
        </div>
      </div>
    </div>
    <!-- latest jquery-->
    <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
    <!-- feather icon js-->
    <script src="{{asset('assets/js/icons/feather-icon/feather.min.js')}}"></script>
    <script src="{{asset('assets/js/icons/feather-icon/feather-icon.js')}}"></script>
    <!-- Plugins JS Ends-->
    <!-- Theme js-->
    <script src="{{asset('assets/js/script.js')}}"></script>
    <!-- login js-->
    <!-- Plugin used-->
    <script>
    $(document).ready(function() {
      $(".emoji").click(function(){
        var emojiId = $(this).data('id');
        $("#emojiLevel").val(emojiId);

        $("#emojiForm").submit();
      });

      function disableBack(){
        window.history.forward()
      }

      window.onload = disableBack();
      window.onpageshow = function(evt) {
        if(evt.persisted){
          disableBack();
        }
      }
    });
</script>
  </body>
</html>