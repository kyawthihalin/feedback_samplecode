@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Manage Role</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Authentication</li>
                        <li class="breadcrumb-item active">Role</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h5>Roles</h5>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal"> <i class="icon-plus"></i> Add New</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Role Name</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Role Name</label>
                        <input type="text" class="form-control" id="role-name" name="permission-name" placeholder="Enter Role Name" min="5" required>
                        <small id="errorName" style="color: red;"></small>
                    </div>
                    <p style="font-weight: bold;">Permissions</p>
                    <div class="row">
                    @foreach($permissions as $permission)
                    <div class="col-md-6">
                    <div class="form-group form-check">
                        <input type="checkbox" class="check" value="{{$permission->name}}" class="form-check-input" id="exampleCheck1{{$permission->id}}">
                        <label class="form-check-label" for="exampleCheck1{{$permission->id}}">{{$permission->name}}</label>
                    </div>
                    </div>
                    @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveRole">Save Role</button>
                </div>
            </div>
        </div>
    </div>

    <!-- edit modal -->

    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">Edit Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Role Name</label>
                        <input type="text" class="form-control" id="edit-role-name" name="edit-permission-name" placeholder="Enter Role Name" min="5" required>
                        <input type="hidden" id="editId">
                        <small id="errorName" style="color: red;"></small>
                    </div>
                    <p style="font-weight: bold;">Permissions</p>
                    <div class="row">
                    @foreach($permissions as $permission)
                    <div class="col-md-6">
                    <div class="form-group form-check">
                        <input type="checkbox" class="check1" data-id="{{$permission->id}}" value="{{$permission->name}}" class="form-check-input" id="exampleCheck2{{$permission->id}}">
                        <label class="form-check-label" for="exampleCheck2{{$permission->id}}">{{$permission->name}}</label>
                    </div>
                    </div>
                    @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateRole">Update Role</button>
                </div>
            </div>
        </div>
    </div>
    <!-- detail modal -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel3">Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Role Name:</p>
                                </div>
                                <div class="col-md-6">
                                    <p id="roleDetailName"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Permissions:</p>
                                </div>
                                <div class="col-md-6">
                                <p id="roleDetailPermissions"></p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updateRole">Update Role</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        var roleTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "role/getAll",
            columns: [{
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
        $('#saveRole').click(function(event) {
            event.preventDefault();
            let name = $('#role-name').val();
            let token = '{{csrf_token()}}';
            let permissions=[];
            $('.check').each(function(idx, el){
            if($(el).is(':checked'))
            { 
                permissions.push($(el).val());
            }

            });
            $.ajax({
                url: '{{route('saveRole')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    permissions
                },
                success: function(responese) {
                    $('#exampleModal').modal('hide');
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Role Successfully Added',
                        showConfirmButton: false,
                    })
                    roleTable.ajax.reload()
                    $('#role-name').val('');
                },
                error: function(response) {
                    console.log(response)
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $('#errorName').text('Name is Required');
                            $('#role-name').css('border-color', 'red');
                        }
                    } else {
                        if (response.responseJSON.message) {
                            $('#errorName').text(`${name} already exist!`);
                            $('#role-name').css('border-color', 'red');
                        }
                    }
                }
            });
        })
        $('#updateRole').click(function(event) {
            event.preventDefault();
            let name = $('#edit-role-name').val();
            let id = $('#editId').val();
            let permissions=[];
            $('.check1').each(function(idx, el){
            if($(el).is(':checked'))
            { 
                permissions.push($(el).val());
            }

            });
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('updateRole')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    id,
                    permissions
                },
                success: function(responese) {
                    $('#exampleModal1').modal('hide');
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Role Successfully Updated',
                        showConfirmButton: false,
                    })
                    roleTable.ajax.reload()
                },
                error: function(response) {
                    console.log(response)
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $('#errorName').text('Name is Required');
                            $('#role-name').css('border-color', 'red');
                        }
                    } else {
                        if (response.responseJSON.message) {
                            $('#errorName').text(`${name} already exist!`);
                            $('#role-name').css('border-color', 'red');
                        }
                    }
                }
            });
        })
        $(document).on('click', '.delete', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This Role will be deleted from your Role list.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{route('deleteRole')}}',
                            type: 'POST',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Role Successfully Deleted',
                                    showConfirmButton: false,
                                })
                                roleTable.ajax.reload()
                            },
                            error: function(responese) {
                                swal("There is an error occoured!");
                            }
                        });
                    } else {
                        swal("Your Role is safe!");
                    }
                })
        })
        $(document).on('click', '.edit', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('editRole')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    id,
                },
                success: function(response) {
                    $('#edit-role-name').val(response.role);
                    $('#editId').val(response.id);
                    for (var i = 0; i < response.rolePermissions.length; i++) {
                        let permission = response.rolePermissions[i].permission_id;
                        $('.check1').each(function(){
                            if ($(this).data('id') === permission) {
                                $(this).prop('checked', true);
                            }
                        });
                    }
                },
            });
        })
        $(document).on('click', '.detail', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('detailRole')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    id,
                },
                success: function(response) {
                    console.log(response)
                    $('#roleDetailName').text(response.role)
                    var pertext='';
                    for (var i = 0; i < response.rolePermissions.length; i++) {
                        pertext += response.rolePermissions[i].name + ',';
                      
                    }
                    $('#roleDetailPermissions').text(pertext)

                },
            });
        })
        $('#exampleModal1').on('hide.bs.modal', function (e) {
            $('.check1').each(function(){
                    $(this).prop('checked', false);
            });
        })
    });
</script>
@endsection