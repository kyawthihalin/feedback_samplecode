@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Manage Admins</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item"><a href="/admin/account">Manage Authentication</a></li>
                        <li class="breadcrumb-item active">Add User</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <form class="theme-form" id="analysis-form">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="pb-3">
                        <h4>User Detail</h4>
                      </div>
                      <div class="form-group mb-3">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" type="text" placeholder="Johan">
                      </div>
                      <div class="form-group mb-3">
                        <label for="email">Email address</label>
                        <input class="form-control" id="email" type="email" placeholder="name@example.com">
                      </div>
                      <div class="form-group mb-3">
                        <label for="password">Password</label>
                        <input class="form-control" id="password" type="password" placeholder="Enter Password">
                      </div>
                      <div class="form-group">
                        <div class="checkbox checkbox-primary pt-2">
                          <input id="checkbox-primary-1" name="smily_report" value="1" id="smile-report" type="checkbox">
                          <label for="checkbox-primary-1">Smilyes status notofications</label>
                        </div>
                      </div>
                    {{-- </form> --}}
                    {{-- <div class="text-right">
                      <button class="btn btn-light">Cancel</button>
                      <button class="btn btn-primary">Submit</button>
                    </div> --}}
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                        <div class="col-md-12 py-4">
                          <div>
                            <h4 class="pb-3">Data Access</h4>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Velit ducimus neque exercitationem esse ab minima
                            quisquam, explicabo facilis minus suscipit voluptatem, veniam repellat.
                          </div>
                          <div class="pt-3">
                            <h6>Select Subgroup</h6>
                          </div>
                          @foreach ($branchs as $branch)
                              
                          <div class="pt-3">
                            <div class="form-check-inline">
                              <div style="font-size: 16px;">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="branch" value="{{$branch->id}}">All Subgroup for {{$branch->name}}
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="p-3">
                            @foreach ($branch->subBranch as $subBranch)   
                            <div class="form-check-inline">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="subbranch" value="{{$subBranch->id}}">{{$subBranch->name}}
                              </label>
                            </div>
                            @endforeach
                          </div>
                          @endforeach
                        </div>
                      {{-- <div class="text-right">
                        <button class="btn btn-light">Cancel</button>
                        <button class="btn btn-primary">Submit</button>
                      </div> --}}
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <div>
                        <h4 class="pb-3">Subscription</h4>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Velit ducimus neque exercitationem esse ab minima
                        quisquam, explicabo facilis minus suscipit voluptatem, veniam repellat.
                      </div>
                      <div class="row pt-3">
                        <div class="col-md-6">
                          <div class="form-group mb-2">
                            <div class="checkbox checkbox-primary">
                              <input id="checkbox-primary-2" name="weekly_report" value="1" id="weekly-report" type="checkbox">
                              <label for="checkbox-primary-2">Weekly Performance Report</label>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group mb-2">
                            <div class="checkbox checkbox-primary">
                              <input id="checkbox-primary-3" name="daily_report" value="1" id="daily-report" type="checkbox">
                              <label for="checkbox-primary-3">Daily Performance Report</label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="pt-3 text-right">
                        <button class="btn btn-light">Cancel</button>
                        <button class="btn btn-primary">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection


@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script>
$('#analysis-form').submit(function(e){
  e.preventDefault();

  let branch = [];
  let subbranch = [];
  let name = $('#name').val();
  let email = $('#email').val();
  let password = $('#password').val();
  let smileNotification = $('#smile-report').val();
  let weeklyReport = $('#weekly-report').val();
  let dailyReport = $('#daily-report').val();

  $.each($("input[name='branch']:checked"), function(){
    branch.push($(this).val());
  });
  
  $.each($("input[name='subbranch']:checked"), function(){
    subbranch.push($(this).val());
  });

  var token = '{{csrf_token()}}';
  $.ajax({
      url:'{{route('create_new_analysis')}}',
      type: 'POST',
      async: false,
      headers: {
      'X-CSRF-Token': token 
      },
      data :{
        branch,
        subbranch,
        name,
        email,
        password,
        smileNotification,
        weeklyReport,
        dailyReport
      },
      success: function (response) {
          swal({
              position: 'top-end',
              icon: 'success',
              title: 'New Question Successfully Saved',
          }).then(function () {
            location.reload();
          });
      },
      error: function (response) {
        swal({
            position: 'top-end',
            icon: 'error',
            title: response.responseJSON.msg,
            showConfirmButton: false,
        });
      }
  }); 
});
</script>
@endsection
