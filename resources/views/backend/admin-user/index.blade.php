@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Manage Admins</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Authentication</li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        @if (auth()->user()->isAdmin())
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <form class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Add New User</h5>
                        <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="datetime-picker">
                            <form class="theme-form">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">Company Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Company Name" min="5" required>
                                    <small id="errorName" style="color: red;"></small>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" min="5" required>
                                    <small id="errorEmail" style="color: red;"></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="country">Country</label>
                                    <select class="form-control btn-square" id="country">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="errorCountry"></small>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="industry">Industry</label>
                                    <select class="form-control btn-square" id="industry">
                                    <option value="">Select Industry</option>
                                    @foreach($industries as $industry)
                                    <option value="{{$industry->id}}">{{$industry->title}}</option>
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="errorIndustry"></small>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="datefilter">Start Date - End Date</label>
                                    
                                    <input class="form-control dataRange" type="text" name="datefilter" id="datefilter" value="" placeholder="Start Date - End Date" autocomplete="off" required>
                                
                                    <small style="color: red;" id="errorDatefilter"></small>
                                </div>

                                <div class="form-group col-md-6">
                                <label class="col-form-label text-right">Role</label>
                                <div class="">
                                    <select class="form-control btn-square" id="roles" required>
                                    <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="errorRole"></small>
                                </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" min="5" required>
                                    <small id="errorPassword" style="color: red;"></small>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="cpassword">Confirm Password</label>
                                    <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Enter confirm password" min="5" required>
                                    <small id="errorCPassowrd" style="color: red;"></small>
                                </div>
                            </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" id="saveAdmin" type="submit">Create</button>
                        </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
        <div class="edit-profile" id="edit">
        
        </div>

        <div class="edit-profile" id="edit-password-con">

        </div>

        <div class="edit-profile" id="detail-con">

        </div>

    </div>

    <div class="container-fluid">
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    @if (auth()->user()->isAdmin())
                                    <h5>Users</h5>
                                    @else
                                    <h5>Manage Profile</h5>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (auth()->user()->isMerchant() || auth()->user()->isAnalysis())    
                            <div class="row">
                                <div class="col-md-10">
                                    <h5></h5>
                                </div>
                            </div>
                            @endif
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    

    <!-- edit modal -->

    
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        var userTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "account/getAll",
            columns: [
              
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data : 'roles',
                    name : 'roles'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });

        $('#saveAdmin').click(function(event) {
            event.preventDefault();
            let name = $('#name').val();
            let email = $('#email').val();
            let country_id = $('#country').val();
            let industry_id = $('#industry').val();
            let datefilter = $('#datefilter').val();
            let password = $('#password').val();
            let cpassword = $('#cpassword').val();
            let roles=[];
            roles = $('#roles').val();
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('saveAccount')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    email,
                    country_id,
                    industry_id,
                    datefilter,
                    password,
                    cpassword,
                    roles
                },
                success: function(responese) {
                    // $('#name').val('');
                    // $('#email').val('');
                    // $('#country').val('');
                    // $('#industry').val('');
                    // $('#datefilter').val('');
                    // $('#password').val('');
                    // $('#cpassword').val('');
                    // $('#role').val('');
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Admin Successfully Added',
                        showConfirmButton: false,
                    })
                    userTable.ajax.reload();
                    location.reload();
                },
                error: function(response) {
                    console.log(response)
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $('#errorName').text(response.responseJSON.errors.name);
                            $('#name').css('border-color', 'red');
                        }
                        else {
                            $('#errorName').text('');
                            $('#name').css('border-color', 'black');
                        }

                        if (response.responseJSON.errors.email) {
                            $('#errorEmail').text(response.responseJSON.errors.email);
                            $('#email').css('border-color', 'red');
                        }
                        else {
                            $('#errorEmail').text('');
                            $('#email').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.password) {
                            $('#errorPassword').text(response.responseJSON.errors.password);
                            $('#password').css('border-color', 'red');
                        }
                        else {
                            $('#errorPassword').text('');
                            $('#password').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.cpassword) {
                            $('#errorCPassowrd').text(response.responseJSON.errors.cpassword);
                            $('#cpassword').css('border-color', 'red');
                        }
                        else {
                            $('#errorCPassowrd').text('');
                            $('#cpassword').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.roles) {
                            $('#errorRole').text(response.responseJSON.errors.roles);
                            $('#roles').css('border-color', 'red');
                        }
                        else {
                            $('#errorRole').text('');
                            $('#roles').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.country) {
                            $('#errorCountry').text(response.responseJSON.errors.country);
                            $('#country').css('border-color', 'red');
                        }
                        else {
                            $('#errorCountry').text('');
                            $('#country').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.industry) {
                            $('#errorIndustry').text(response.responseJSON.errors.industry);
                            $('#industry').css('border-color', 'red');
                        }
                        else {
                            $('#errorIndustry').text('');
                            $('#industry').css('border-color', 'black');
                        }
                        if (response.responseJSON.errors.datefilter) {
                            $('#errorDatefilter').text(response.responseJSON.errors.datefilter);
                            $('#datefilter').css('border-color', 'red');
                        }
                        else {
                            $('#errorDatefilter').text('');
                            $('#datefilter').css('border-color', 'black');
                        }
                    }
                    
                }
            });
        })
        // $('#updateAdmin').click(function(event) {
        //     event.preventDefault();
        //     let name = $('#edit-name').val();
        //     let id = $('#editId').val();
        //     let email = $('#edit-email').val();
        //     let roles=[];
        //     roles = $('#edit-roles').val();
        //     let token = '{{csrf_token()}}';
        //     $.ajax({
        //         url: '{{route('updateAccount')}}',
        //         type: 'POST',
        //         async: false,
        //         headers: {
        //             'X-CSRF-Token': token
        //         },
        //         data: {
        //             name,
        //             id,
        //             email,
        //             roles
        //         },
        //         success: function(responese) {
        //             swal({
        //                 position: 'top-end',
        //                 icon: 'success',
        //                 title: 'User Successfully Updated',
        //                 showConfirmButton: false,
        //             })
        //             userTable.ajax.reload()
        //         },
        //         error: function(response) {
        //             console.log(response)
        //             if (response.responseJSON.errors) {
        //                 if (response.responseJSON.errors.name) {
        //                     $('#edit-errorName').text(response.responseJSON.errors.name);
        //                     $('#edit-name').css('border-color', 'red');
        //                 }
        //                 else {
        //                     $('#edit-errorName').text('');
        //                     $('#edit-name').css('border-color', 'black');
        //                 }

        //                 if (response.responseJSON.errors.email) {
        //                     $('#edit-errorEmail').text(response.responseJSON.errors.email);
        //                     $('#edit-email').css('border-color', 'red');
        //                 }
        //                 else {
        //                     $('#edit-errorEmail').text('');
        //                     $('#edit-email').css('border-color', 'black');
        //                 }
        //                 if (response.responseJSON.errors.roles) {
        //                     $('#edit-errorRole').text(response.responseJSON.errors.roles);
        //                     $('#edit-roles').css('border-color', 'red');
        //                 }
        //                 else {
        //                     $('#edit-errorRole').text('');
        //                     $('#edit-roles').css('border-color', 'black');
        //                 }
        //             }
        //         }
        //     });
        // })
        $(document).on('click', '.delete', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This User will be deleted from your User list.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{route('deleteAccount')}}',
                            type: 'POST',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'User Successfully Deleted',
                                    showConfirmButton: false,
                                })
                                userTable.ajax.reload()
                            },
                            error: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'error',
                                    title: responese.responseJSON.msg,
                                    showConfirmButton: false,
                                });
                            }
                        });
                    } else {
                        swal("User is safe!");
                    }
                })
        })
        $(document).on('click', '.edit', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('editAccount')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    id,
                },
                success: function(response) {
                    $('#edit').html(response);
                    $('#add').hide();
                    $('#edit-password-con').html('');
                    $('#detail-con').html('');
                },
            });
        });

        $(document).on('click', '.edit-password', function() {
            let id = $(this).data('id')
            $.ajax({
                url: '{{route('editPassword')}}',
                type: 'GET',
                async: false,
                data: {
                    id,
                },
                success: function(response) {
                    $('#edit-password-con').html(response);
                    $('#edit').html('');
                    $('#add').hide();
                    $('#detail-con').html('');
                },
            });
        })

        $(document).on('click', '#detail-btn', function() {
            let id = $(this).data('id')
            $.ajax({
                url: '{{route('userdetail')}}',
                type: 'GET',
                async: false,
                data: {
                    id,
                },
                success: function(response) {
                    $('#edit-password-con').html('');
                    $('#edit').html('');
                    $('#add').hide();
                    $('#detail-con').html(response);
                },
            });
        })

        $(document).on('click', '.deactivate', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This User will Dactivate.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{route('changeAccStatus')}}',
                            type: 'POST',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'User Successfully Deactivate',
                                    showConfirmButton: false,
                                })
                                userTable.ajax.reload()
                            },
                            error: function(responese) {
                                swal("There is an error occoured!");
                            }
                        });
                    } else {
                        swal("User is safe!");
                    }
                })
        })

        $(document).on('click', '.activate', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This User will Activate.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{route('changeAccStatus')}}',
                            type: 'POST',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'User Successfully Activate',
                                    showConfirmButton: false,
                                })
                                userTable.ajax.reload()
                            },
                            error: function(responese) {
                                swal("There is an error occoured!");
                            }
                        });
                    } else {
                        swal("User is safe!");
                    }
                })
        })
    });
</script>
@endsection