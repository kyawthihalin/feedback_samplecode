@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Manage Permission</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Authentication</li>
                        <li class="breadcrumb-item active">Permissions</li>
                    </ol>
                </div>
                <div class="col-lg-6">
                    <!-- Bookmark Start-->
                    <div class="bookmark pull-right">
                        <ul>
                            <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Chat"><i data-feather="message-square"></i></a></li>
                            <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Icons"><i data-feather="command"></i></a></li>
                            <li><a href="#" data-container="body" data-toggle="popover" data-placement="top" title="" data-original-title="Learning"><i data-feather="layers"></i></a></li>
                            <li><a href="#"><i class="bookmark-search" data-feather="star"></i></a>
                                <form class="form-inline search-form">
                                    <div class="form-group form-control-search">
                                        <input type="text" placeholder="Search..">
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <!-- Bookmark Ends-->
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h5>Permissions</h5>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal"> <i class="icon-plus"></i> Add New</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Permission Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Permission Name</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Permission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Permission Name</label>
                        <input type="text" class="form-control" id="permission-name" name="permission-name" placeholder="Enter Permission Name" min="5" required>
                        <small id="errorName" style="color: red;"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="savePermission">Save Permission</button>
                </div>
            </div>
        </div>
    </div>

    <!-- edit modal -->

    <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel2">Edit Permission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Permission Name</label>
                        <input type="text" class="form-control" id="edit-permission-name" name="edit-permission-name" placeholder="Enter Permission Name" min="5" required>
                        <input type="hidden" id="editId">
                        <small id="errorName" style="color: red;"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="updatePermission">Update Permission</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        var permissionTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "permission/getAll",
            columns: [{
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
        $('#savePermission').click(function(event) {
            event.preventDefault();
            let name = $('#permission-name').val();
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('savePermission')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                },
                success: function(responese) {
                    $('#exampleModal').modal('hide');
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Permission Successfully Added',
                        showConfirmButton: false,
                    })
                    permissionTable.ajax.reload()
                },
                error: function(response) {
                    console.log(response)
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $('#errorName').text('Name is Required');
                            $('#permission-name').css('border-color', 'red');
                        }
                    } else {
                        if (response.responseJSON.message) {
                            $('#errorName').text(`${name} already exist!`);
                            $('#permission-name').css('border-color', 'red');
                        }
                    }
                }
            });
        })
        $('#updatePermission').click(function(event) {
            event.preventDefault();
            let name = $('#edit-permission-name').val();
            let id = $('#editId').val();
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('updatePermission')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    id
                },
                success: function(responese) {
                    $('#exampleModal1').modal('hide');
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Permission Successfully Updated',
                        showConfirmButton: false,
                    })
                    permissionTable.ajax.reload()
                },
                error: function(response) {
                    console.log(response)
                    if (response.responseJSON.errors) {
                        if (response.responseJSON.errors.name) {
                            $('#errorName').text('Name is Required');
                            $('#permission-name').css('border-color', 'red');
                        }
                    } else {
                        if (response.responseJSON.message) {
                            $('#errorName').text(`${name} already exist!`);
                            $('#permission-name').css('border-color', 'red');
                        }
                    }
                }
            });
        })
        $(document).on('click', '.delete', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This permission will be deleted from your sub permission list.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: '{{route('deletePermission')}}',
                            type: 'POST',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            data: {
                                id
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Permission Successfully Deleted',
                                    showConfirmButton: false,
                                })
                                permissionTable.ajax.reload()
                            },
                            error: function(responese) {
                                swal("There is an error occoured!");
                            }
                        });
                    } else {
                        swal("Your Permission is safe!");
                    }
                })
        })
        $(document).on('click', '.edit', function() {
            let id = $(this).data('id')
            let token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route('editPermission')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    id,
                },
                success: function(response) {
                    $('#edit-permission-name').val(response.permission);
                    $('#editId').val(response.id);
                },
            });
        })
    });
</script>
@endsection