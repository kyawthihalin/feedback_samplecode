<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Themify icon-->
    {{-- <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet"> --}}
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">
    <!-- Plugins css start-->
   
  
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
  </head>
  <body>
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6">
                  <h3>Analytics</h3>
                </div>
                <div class="col-lg-6">

                </div>
              </div>
            </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 col-xl-12 box-col-12">
              <div class="card">
                <div class="card-body">
                  <h5>Result ({{$currentMonth}})</h5>
                  <div id="column-chart"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body">
                  <h5>Hourly Distribution</h5>
                  <div id="hourly-chart"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body">
                  <h5>Weekly Distribution</h5>
                  <div id="weekly-chart"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body chart-block">
                    <h5>Pie Chart</h5>
                  <div class="chart-overflow" id="pie-chart2"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body progress-showcase">
                  <div class="pro-head">
                    <h5 class="pb-4">Comprison</h5>
                  </div>
                  <div class="pro-body">
                    @if (count($comparisonAnsArr) > 0)
                    @foreach ($comparisonAnsArr as $name => $comparisonAns)    
                    <div class="progress" style="height: 11px;">
                      <div class="progress-bar bg-danger" role="progressbar" style="width: {{ isset($comparisonAns['bad']) ? $comparisonAns['bad'] : 0 }}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                      <div class="progress-bar bg-danger-half" role="progressbar" style="width: {{ isset($comparisonAns['poor']) ? $comparisonAns['poor'] : 0 }}%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                      <div class="progress-bar bg-sucess-half" role="progressbar" style="width: {{ isset($comparisonAns['excellent']) ? $comparisonAns['excellent'] : 0 }}%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                      <div class="progress-bar bg-success" role="progressbar" style="width: {{ isset($comparisonAns['good']) ? $comparisonAns['good'] : 0 }}%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div class="progress-text pb-2">
                      <small>
                        {{$name}}
                          <span class="pull-right m-l-10 txt-primary">{{ $comparisonAns['total'] ? $comparisonAns['total'] : 0 }}</span>
                      </small>
                    </div>
                    @endforeach
                    @else 
                    No Data
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body progress-showcase">
                  <div class="pro-head">
                    <h5 class="pb-4">Heighlights</h5>
                  </div>
                  <div class="pro-body">
                    @if (count($positiveAnsArr) > 0)    
                    @foreach ($positiveAnsArr as $name => $positiveAns) 
                    <div class="progress" style="height: 11px;">
                      <div class="progress-bar bg-sucess-half" role="progressbar" style="width: {{isset($positiveAns['good']) ? $positiveAns['good'] : 0}}%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                        <div class="progress-bar bg-success" role="progressbar" style="width: {{isset($positiveAns['excellent']) ? $positiveAns['excellent'] : 0}}%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    <div class="progress-text pb-2">
                      <small>
                        {{ $name }}
                      <span class="pull-right m-l-10 txt-primary">{{ $positiveAns['total'] ? $positiveAns['total'] : 0 }}</span>
                      </small>
                    </div>
                    @endforeach
                    @else 
                    No Data
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body progress-showcase">
                  <div class="pro-head">
                    <h5 class="pb-4">Pain Points</h5>
                  </div>
                  <div class="pro-body">
                    @if (count($negativeAnsArr) > 0)    
                    @foreach ($negativeAnsArr as $name => $negativeAns) 
                    <div class="progress" style="height: 11px;">
                      <div class="progress-bar bg-danger-half" role="progressbar" style="width: {{isset($negativeAns['poor']) ? $negativeAns['poor'] : 0}}%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{isset($negativeAns['bad']) ? $negativeAns['bad'] : 0}}%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    <div class="progress-text pb-2">
                      <small>
                        {{ $name }}
                      <span class="pull-right m-l-10 txt-primary">{{ isset($negativeAns['total']) ? $negativeAns['total'] : 0 }}</span>
                      </small>
                    </div>
                    @endforeach
                    @else 
                    No Data
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- latest jquery-->
    <script src="{{asset('assets/js/jquery-3.5.1.min.js')}}"></script>
    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/config.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/chart/google/google-chart-loader.js')}}"></script>
    <script src="{{asset('assets/js/chart/google/google-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/charttt/apex-chart.js')}}"></script>
    <script src="{{asset('assets/js/chart/charttt/stock-prices.js')}}"></script>
    <script src="{{asset('assets/js/chart/charttt/chart-custom.js')}}"></script>
    <script src="{{asset('assets/js/tooltip-init.js')}}"></script>

    <script>

      var dayMonthList = <?php echo json_encode($dayOfMonthList);?>;
      var databyMonthExcellent = <?php echo json_encode($databyMonthExcellent);?>;
      var databyMonthGood = <?php echo json_encode($databyMonthGood);?>;
      var databyMonthPoor = <?php echo json_encode($databyMonthPoor);?>;
      var databyMonthBad = <?php echo json_encode($databyMonthBad);?>;
      
      var options3 = {
        chart: {
            height: 350,
            type: 'bar',
            toolbar:{
              show: false
            }
        },
        plotOptions: {
            bar: {
                horizontal: false,
                endingShape: 'rounded',
                columnWidth: '55%',
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        series: [{
            name: 'Excellent',
            data: databyMonthExcellent
        }, {
            name: 'Good',
            data: databyMonthGood
        }, {
            name: 'Poor',
            data: databyMonthPoor
        }, {
            name: 'Bad',
            data: databyMonthBad
        }],
        xaxis: {
            categories: dayMonthList,
        },
        // yaxis: {
        //     title: {
        //         text: 'Percentage'
        //     }
        // },
        fill: {
            opacity: 1
      
        },
        tooltip: {
            y: {
                formatter: function (val) {
                    return val
                }
            }
        },
        colors:["#51bb25" , CubaAdminConfig.primary , "#f8d62b", CubaAdminConfig.secondary]
      }
      
      var chart3 = new ApexCharts(
          document.querySelector("#column-chart"),
          options3
      );
      
      chart3.render();
      
      
      var hourList = <?php echo json_encode($hourList);?>;
      var dataExcellent = <?php echo json_encode($hourlyExcellentArr);?>;
      var dataGood = <?php echo json_encode($hourlyGoodArr);?>;
      var dataPoor = <?php echo json_encode($hourlyPoorArr);?>;
      var dataBad = <?php echo json_encode($hourlyBadArr);?>;
      
      var optionsHourlyChart = {
          chart: {
              height: 350,
              type: 'bar',
              toolbar:{
                show: false
              }
          },
          plotOptions: {
              bar: {
                  horizontal: false,
                  endingShape: 'rounded',
                  columnWidth: '55%',
              },
          },
          dataLabels: {
              enabled: false
          },
          stroke: {
              show: true,
              width: 2,
              colors: ['transparent']
          },
          series: [{
              name: 'Excellent',
              data: dataExcellent
          }, {
              name: 'Good',
              data: dataGood
          }, {
              name: 'Poor',
              data: dataPoor
          }, {
              name: 'Bad',
              data: dataBad
          }],
          xaxis: {
              categories: hourList,
          },
          // yaxis: {
          //     title: {
          //         text: 'Percentage'
          //     }
          // },
          fill: {
              opacity: 1
      
          },
          tooltip: {
              y: {
                  formatter: function (val) {
                      return val
                  }
              }
          },
          colors:["#51bb25" , CubaAdminConfig.primary , "#f8d62b", CubaAdminConfig.secondary]
      }
      
      var hourlyChart = new ApexCharts(
          document.querySelector("#hourly-chart"),
          optionsHourlyChart
      );
      
      hourlyChart.render();
      
      var weekDayList = <?php echo json_encode($weekDayList);?>;
      var weeklyDataExcellent = <?php echo json_encode($weekExcellentArr);?>;
      var weeklyDataGood = <?php echo json_encode($weekGoodArr);?>;
      var weeklyDataPoor = <?php echo json_encode($weekPoorArr);?>;
      var weeklyDataBad = <?php echo json_encode($weekBadArr);?>;
      
      var weeklyOption = {
          chart: {
              height: 350,
              type: 'bar',
              toolbar:{
                show: false
              }
          },
          plotOptions: {
              bar: {
                  horizontal: false,
                  endingShape: 'rounded',
                  columnWidth: '55%',
              },
          },
          dataLabels: {
              enabled: false
          },
          stroke: {
              show: true,
              width: 2,
              colors: ['transparent']
          },
          series: [{
              name: 'Excellent',
              data: weeklyDataExcellent
          }, {
              name: 'Good',
              data: weeklyDataGood
          }, {
              name: 'Poor',
              data: weeklyDataPoor
          }, {
              name: 'Bad',
              data: weeklyDataBad
          }],
          xaxis: {
              categories: weekDayList,
          },
          // yaxis: {
          //     title: {
          //         text: 'Percentage'
          //     }
          // },
          fill: {
              opacity: 1
      
          },
          tooltip: {
              y: {
                  formatter: function (val) {
                      return val
                  }
              }
          },
          colors:["#51bb25" , CubaAdminConfig.primary , "#f8d62b", CubaAdminConfig.secondary]
      }
      
      var weeklyChart = new ApexCharts(
          document.querySelector("#weekly-chart"),
          weeklyOption
      );
      
      weeklyChart.render();
      
      
      google.charts.load('current', {packages: ['corechart', 'bar']});
      google.charts.load('current', {'packages':['line']});
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawBasic);
      
      var response = {{ json_encode($totalFeedback)}};
      
      function drawBasic() {
      
        if ($("#pie-chart2").length > 0) {
              var data = google.visualization.arrayToDataTable([
                ['Response', response.toString()],
                ['Excellent',  {{json_encode($percentExcellent)}}],
                ['Good',  {{json_encode($percentGood)}}],
                ['Poor',  {{json_encode($percentPoor)}}],
                ['Bad', {{json_encode($percentBad)}}]
              ]);
              var options = {
                title: 'Responses: ' + response.toString(),
                is3D: true,
                width:'100%',
                height: 300,
                colors: ["#51bb25" , CubaAdminConfig.primary , "#f8d62b", CubaAdminConfig.secondary]
              };
              var chart = new google.visualization.PieChart(document.getElementById('pie-chart2'));
              chart.draw(data, options);
          }
      }
      
      
      </script>
  </body>

</html>