@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

    <!-- Page Sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6">
                <h3>Create Template</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Template</li>
                  <li class="breadcrumb-item">Create Template</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fxluid starts-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h5>Create Template</h5>
                </div>
                <form class="form theme-form" action="{{url('admin/template')}}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 pb-2">
                        <label class="col-sm-12 col-form-label pl-0">Custom select <span class="text-danger">*</span></label>
                        <div class="form-group row mb-0">
                          <div class="col-sm-12">
                            <select class="custom-select form-control" name="industry_id">
                              <option value="" selected>Select Industries type</option>
                              @foreach($industries as $industry)
                              <option value="{{$industry->id}}">{{$industry->title}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 pb-2">
                        <label class="col-sm-12 col-form-label pl-0">Feedback Question <span class="text-danger">*</span></label>
                        <div class="form-group row mb-0">
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="question_text" type="text" placeholder="Type Feedback Question">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-12">
                        <label class="h6 pt-5 pb-2"><span class="text-danger">*</span> Please fill follow up reasons</label>
                      </div>
                      <div class="col">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Upload Photo 1</label>
                          <div class="col-sm-12">
                            <input class="form-control" name="photo1" type="file">
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group row mb-0">
                          <label class="col-sm-12 col-form-label">Follow Up 1</label>
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="description1" type="text" placeholder="Type follow up reason">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Upload Photo 2</label>
                          <div class="col-sm-12">
                            <input class="form-control" name="photo2" type="file">
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group row mb-0">
                          <label class="col-sm-12 col-form-label">Follow Up 2</label>
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="description2" type="text" placeholder="Type follow up reason">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Upload Photo 3</label>
                          <div class="col-sm-12">
                            <input class="form-control" name="photo3" type="file">
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group row mb-0">
                          <label class="col-sm-12 col-form-label">Follow Up 3</label>
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="description3" type="text" placeholder="Type follow up reason">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Upload Photo 4</label>
                          <div class="col-sm-12">
                            <input class="form-control" name="photo4" type="file">
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group row mb-0">
                          <label class="col-sm-12 col-form-label">Follow Up 4</label>
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="description4" type="text" placeholder="Type follow up reason">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col">
                          <div class="form-group mb-0">
                            <label for="exampleFormControlTextarea4">Details</label>
                            <textarea class="form-control" id="exampleFormControlTextarea4"  name="description" rows="3" placeholder="Please Detail information about survey"></textarea>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="card-footer text-right">
                    <input class="btn btn-light" type="reset" value="Cancel">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <!-- footer start-->
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        var userTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "template/getAll",
            "paging": true,
            columns: [
              
                {
                    data: 'question_text',
                    name: 'question_text'
                },
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });

        var errors = @json(array_unique($errors->all()));

        if(errors.length){
            var text = '';
            $.each(errors, function( index, value ) {
                text += value;
            });
            swal({
                position: 'top-end',
                icon: 'warning',
                title: text,
                showConfirmButton: false,
            });
        }

        var success = @json(\Session::get('success'));
        if(success){
            swal({
                position: 'top-end',
                icon: 'success',
                title: success,
                showConfirmButton: false,
            });
        }
    });
</script>
@endsection