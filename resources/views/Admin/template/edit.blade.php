@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

    <!-- Page Sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6">
                <h3>Edit Template</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Create Template</li>
                  <li class="breadcrumb-item">Edit</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fxluid starts-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h5>Edit Template</h5>
                </div>
                <form class="form theme-form" id="edit-form" action="{{url('admin/template', $template->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 pb-2">
                        <label class="col-sm-12 col-form-label pl-0">Custom select <span class="text-danger">*</span></label>
                        <div class="form-group row mb-0">
                          <div class="col-sm-12">
                            <select class="custom-select form-control" name="industry_id" required>
                              <option value="" selected>Select Industries type</option>
                              @foreach($industries as $industry)
                              <option value="{{$industry->id}}"  {{$template->industry_id != $industry->id ?: 'selected="selected"'}}>{{$industry->title}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 pb-2">
                        <label class="col-sm-12 col-form-label pl-0">Feedback Question <span class="text-danger">*</span></label>
                        <div class="form-group row mb-0">
                          <div class="col-sm-12">
                            <input class="form-control" id="" name="question_text" value="{{$template->question_text}}" type="text" placeholder="Type Feedback Question" required>
                          </div>
                        </div>
                      </div>
                    </div>

                    @foreach($template->preAnswers as $key => $preAnswer)
                    <div class="row">
                      <div class="col">
                        <div class="form-group row">
                          <label class="col-sm-12 col-form-label">Upload Photo {{$key+1}}</label>
                          <div class="col-sm-12">
                              <span id="oldphoto{{$preAnswer->id}}">
                              <img src="{{url($preAnswer->photo)}}" id="oldphoto{{$preAnswer->id}}" alt="" width="30"> photo {{$key+1}}
                              <button class="btn btn-close" data-id="{{$preAnswer->id}}" type="button">X</button>    
                              </span>
                              <input type="hidden" name="isoldphoto{{$preAnswer->id}}" id="isoldphoto{{$preAnswer->id}}" value="1">
                            <input class="form-control" id="photo{{$preAnswer->id}}" name="photo{{$preAnswer->id}}" type="file">
                          </div>
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group row mb-0">
                          <label class="col-sm-12 col-form-label">Follow Up {{$key+1}}</label>
                          <div class="col-sm-12">
                            <input class="form-control" id="" value="{{$preAnswer->description}}" name="followup{{$preAnswer->id}}" type="text" placeholder="Type follow up reason">
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col">
                          <div class="form-group mb-0">
                            <label for="exampleFormControlTextarea4">Details</label>
                            <textarea class="form-control" id="exampleFormControlTextarea4"  name="description" rows="3" placeholder="Please Detail information about survey">
                                {{$template->description}}
                            </textarea>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="card-footer text-right">
                    <input class="btn btn-light" type="reset" value="Cancel">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- Zero Configuration  Ends-->

          </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <!-- footer start-->
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function(){
        var preAnswers = @json($template->preAnswers->pluck('id')->toArray());
        $.each(preAnswers, function( index, value){
          $("#photo" + value).hide();
        })

        $(".btn-close").click(function(){
            var photoid = $(this).data('id');

            $("#photo"+ photoid).show();
            $("#isoldphoto"+ photoid).val(0);
            $("#oldphoto"+ photoid).remove();
        });

        var errors = @json(array_unique($errors->all()));

        if(errors.length){
            var text = '';
            $.each(errors, function( index, value ) {
                text += value;
            });
            swal({
                position: 'top-end',
                icon: 'warning',
                title: text,
                showConfirmButton: false,
            });
        }

        var success = @json(\Session::get('success'));
        if(success){
            swal({
                position: 'top-end',
                icon: 'success',
                title: success,
                showConfirmButton: false,
            });
            window.location = "/admin/template";  
        }
    });
</script>
@endsection