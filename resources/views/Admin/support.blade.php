@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Support</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Help</li>
                        <li class="breadcrumb-item active">Support</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row starter-main">
            <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                <h5>Looking for more assistance ?</h5>
                </div>
                <div class="card-body">
                <div class="pb-5">
                    <h5><span class="f-w-600">Send us an email with necessary screenshots and your account details to:</span></h5>
                    <button class="btn btn-pill btn-light btn-air-light mt-3" type="button"
                    data-original-title="btn btn-pill btn-light btn-air-light" title=""><a href="mailto:support@likemetric.io">support@likemetric.io</a></button>
                </div>
                <div class="py-2">
                    <h5><span class="f-w-600">For sales enquiries, contact us at:</span></h5>
                    <button class="btn btn-pill btn-light btn-air-light mt-3" type="button"
                    data-original-title="btn btn-pill btn-light btn-air-light" title=""><a href="mailto:sales@ikemetric.io">sales@ikemetric.io</a></button>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="{{asset('assets/js/chart/apex-chart/stock-prices.js')}}"></script>
@endsection