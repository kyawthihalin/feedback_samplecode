@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
@endsection
@section('content')
<!-- Container-fluid starts-->
<div class="page-body">
  <div class="container-fluid pt-5">
    <div class="row">
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-body">
            <div class="text-center">
              <div>
                <p>{{ isset($currentBranch) ? $currentBranch->name : 'No branch you have' }}</p>
                <h5>{{ isset($lastQuestion) ? $lastQuestion->question_text : 'No question' }}</h5>
                <p>{{ $currentDate }}</p>
              </div>
            </div>
            <div class="d-flex justify-content-center pt-4">
              <div style="margin-top: -50px;">
                <div id="chart-widget5" class="img-fluid"> </div>
                {{-- <img src="../assets/images/dashboard/happy.png" alt="" class="pr-4 img-fluid"> --}}
              </div>
              <div class="d-flex justify-content-center pt-5">
                <div class="p-2">
                  <img src="../assets/images/smile/s1.png" class="img-fluid pb-2">
                  <h6><span class="counter">{{$percentExcellent}}</span>%</h6>
                </div>
                <div class="p-2">
                  <img src="../assets/images/smile/s2.png" class="img-fluid pb-2">
                  <h6><span class="counter">{{$percentGood}}</span>%</h6>
                </div>
                <div class="p-2">
                  <img src="../assets/images/smile/s3.png" class="img-fluid pb-2">
                  <h6><span class="counter">{{$percentPoor}}</span>%</h6>
                </div>
                <div class="p-2">
                  <img src="../assets/images/smile/s4.png" class="img-fluid pb-2">
                  <h6><span class="counter">{{$percentBad}}</span>%</h6>
                </div>
              </div>
            </div>
            <div class="text-center">
              <div>
                <span>
                   How was your self-checkout experience today?
                </span>
                <p>
                   <b>{{$totalFeedback}}</b> responses, <b>{{$positivePercent}}%</b> positive, <b>{{$negativePercent}}%</b> negative
                </p>
                {{-- <p>Ranked <b>3 out of 9</b> in area Central</p>
                <p>Best hour: 12:00 Worst hour : 15:00</p> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
          
      <div class="col-md-6 col-sm-12">
        <div class="card">
          <div class="card-body progress-showcase">
            <div class="pro-head">
              <h5 class="pb-4">Top Follow-Ups</h5>
            </div>
            @if (count($positiveAnsArr) > 0 || count($negativeAnsArr) > 0)
            <div class="pro-body row">
              @if (count($positiveAnsArr) > 0)   
              <div class="col-6">
                @foreach ($positiveAnsArr as $name => $positiveAns)   
                <div class="progress" style="height: 11px;">
                <div class="progress-bar" role="progressbar" style="width: {{isset($positiveAns['good']) ? $positiveAns['good'] : 0}}%; background: #92c462;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                  <div class="progress-bar " role="progressbar" style="width: {{isset($positiveAns['excellent']) ? $positiveAns['excellent'] : 0}}%; background:#1ca185;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress-text pb-4">
                  <small>
                    {{$name}}
                  </small>
                </div>
                @endforeach
              </div>
              @endif
              @if (count($negativeAnsArr) > 0)    
              <div class="col-6">
                @foreach ($negativeAnsArr as $name => $negativeAns)   
                <div class="progress" style="height: 11px;">
                  <div class="progress-bar" role="progressbar" style="width: {{isset($negativeAns['poor']) ? $negativeAns['poor'] : 0}}%; background: #e94545;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                  <div class="progress-bar" role="progressbar" style="width: {{isset($negativeAns['bad']) ? $negativeAns['bad'] : 0}}%; background:#f39c26;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress-text pb-4">
                  <small>
                    {{$name}}
                  </small>
                </div>
                @endforeach
              </div>
              @endif
              {{-- <div class="col-12">
                <div class="message my-message mt-3"><img class="rounded-circle float-right chat-user-img img-30" src="../assets/images/smile/s1.png" alt="">
                  Actually everything was fine. I'm very excited.
                </div>
              </div> --}}
              
            </div>
            @else
            No Data
            @endif
          </div>
        </div>
      </div>

      <!-- Uses chart widget Start-->
      <div class="xl-50 col-md-6">
        <div class="small-chart-widget chart-widgets-small">
          <div class="card">
            <div class="card-body">
              <h5>Hourly Pattern</h5>
             
              <div class="chart-container">
                <div class="row">
                  <div class="col-12">
                    <div id="chart-widget9"></div>
                  </div>
                </div>
              </div>
              
              
            </div>
          </div>
        </div>
      </div>
      <!-- Uses chart widget Ends-->

      @if ($hourlyChart)  
      <div class="col-sm-12 col-xl-6 box-col-6">
        <div class="card">
          <div class="card-body">
            <h5>Hourly Distribution</h5>
            <div id="column-chart1"></div>
            
          </div>
        </div>
      </div>
      @endif         

      @if ($lastWeekChart)       
      <div class="col-sm-12 col-xl-6 box-col-6">
        <div class="card">
          <div class="card-body">
            <h5>Weekly Distribution</h5>
            <div id="lastWeekChart"></div>
            
          </div>
        </div>
      </div>
      @endif
      
      @if ($lastMonthChart)    
      <div class="col-lg-6 col-sm-12 box-col-12">
        <div class="card">
          <div class="card-header">
            <h5>Monthly Distribution</h5>
          </div>  
          <div class="card-body chart-block">
            <div class="flot-chart-container">
              <div class="flot-chart-placeholder" id="stacked-bar-chart"></div>
            </div>
          </div>
        </div>
      </div>
      @endif

    </div>
  </div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/chart/charttt/moment.min.js')}}"></script>
<script src="{{asset('assets/js/chart/charttt/apex-chart.js')}}"></script>
{{-- <script src="{{asset('assets/js/chart/charttt/stock-prices.js')}}"></script> --}}
<script src="{{asset('assets/js/chart/morris-chart/morris.js')}}"> </script>
<script src="{{asset("assets/js/chart/morris-chart/raphael.js")}}"></script>
<script src="{{asset("assets/js/chart/morris-chart/prettify.min.js")}}"></script>
<script src="{{asset('assets/js/prism/prism.min.js')}}"></script>
<script src="{{asset('assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('assets/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
  //bubble chart
  (function($) {
    "use strict";
    // var lastMonthList = <?php echo json_encode($lastMonthList);?>;


    var patternByDaySun = <?php echo json_encode(array_values($patternByDay['Sun']));?>;
    var patternByDayMon = <?php echo json_encode(array_values($patternByDay['Mon']));?>;
    var patternByDayTue = <?php echo json_encode(array_values($patternByDay['Tue']));?>;
    var patternByDayWed = <?php echo json_encode(array_values($patternByDay['Wed']));?>;
    var patternByDayThu = <?php echo json_encode(array_values($patternByDay['Thu']));?>;
    var patternByDayFri = <?php echo json_encode(array_values($patternByDay['Fri']));?>;
    var patternByDaySat = <?php echo json_encode(array_values($patternByDay['Sat']));?>;
    var patternHourList = <?php echo json_encode(array_values($patternHourList));?>;
    var optionsuserchart = {
      chart: {
        height: 350,
        type: 'heatmap',
        toolbar: {
          show: false
        }
      },
      series: [{
          name: 'Sun',
          data: patternByDaySun
        }, {
          name: 'Mon',
          data: patternByDayMon
        }, {
          name: 'Tue',
          data: patternByDayTue
        }, {
          name: 'Wed',
          data: patternByDayWed
        }, {
          name: 'Thu',
          data: patternByDayThu
        }, {
          name: 'Fri',
          data: patternByDayFri
        }, {
          name: 'Sat',
          data: patternByDaySat
        }],
      // series: [{
      //     name: 'Sun',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Mon',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Tue',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Wed',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Thu',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Fri',
      //     data: [{
      //       x:'hello',
      //       y:2
      //     }]
      //   }, {
      //     name: 'Sat',
      //     data: [{
      //       x:['hello1', 'hello2'],
      //       y:2
      //     }]
      //   }],
    dataLabels: {
        enabled: false
    },
    colors: ['#1ca185'],
    
    // xaxis: {
    //     categories: patternHourList,
    // },
    yaxis: {
        title: {
            text: 'Happy Index'
        }
    },
    tooltip: {
      y: {
          formatter: function (val) {
              return "Happy Index(" + val + ")";
          }
      }
    }
    }
    var chartuserchart = new ApexCharts(
      document.querySelector("#chart-widget9"),
      optionsuserchart
    );
    chartuserchart.render();

    // Hourly chart
    var hourList = <?php echo json_encode(array_values($hourList));?>;
    var dataExcellent = <?php echo json_encode(array_values($hourlyExcellentArr));?>;
    var dataGood = <?php echo json_encode(array_values($hourlyGoodArr));?>;
    var dataPoor = <?php echo json_encode(array_values($hourlyPoorArr));?>;
    var dataBad = <?php echo json_encode(array_values($hourlyBadArr));?>;

    var options4 = {
      chart: {
        height: 350,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '55%',
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Excellent',
        data: dataExcellent
      }, {
        name: 'Good',
        data: dataGood
      }, {
        name: 'Poor',
        data: dataPoor
      }, {
        name: 'Bad',
        data: dataBad
      }],
      xaxis: {
        categories: hourList,
      },
      // yaxis: {
      //   title: {
      //     text: 'Percentage'
      //   }
      // },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function(val) {
            return val + ""
          }
        }
      },
      colors: ["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
    }

    var chart4 = new ApexCharts(
      document.querySelector("#column-chart1"),
      options4
    );

    chart4.render();

//Last Week
    var lastWeekDayList = <?php echo json_encode(array_values($lastWeekDayList));?>;
    var lastWeekExcellent = <?php echo json_encode(array_values($lastWeekExcellentArr));?>;
    var lastWeekGood = <?php echo json_encode(array_values($lastWeekGoodArr));?>;
    var lastWeekPoor = <?php echo json_encode(array_values($lastWeekPoorArr));?>;
    var lastWeekBad = <?php echo json_encode(array_values($lastWeekBadArr));?>;

    var optionsLastWeekChart = {
      chart: {
        height: 350,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '55%',
        },
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [{
        name: 'Excellent',
        data: lastWeekExcellent
      }, {
        name: 'Good',
        data: lastWeekGood
      }, {
        name: 'Poor',
        data: lastWeekPoor
      }, {
        name: 'Bad',
        data: lastWeekBad
      }],
      xaxis: {
        categories: lastWeekDayList,
      },
      // yaxis: {
      //   title: {
      //     text: 'Percentage'
      //   }
      // },
      fill: {
        opacity: 1

      },
      tooltip: {
        y: {
          formatter: function(val) {
            return val + ""
          }
        }
      },
      colors: ["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
    }

    var lastWeekChart = new ApexCharts(
      document.querySelector("#lastWeekChart"),
      optionsLastWeekChart
    );

    lastWeekChart.render();


var optionsearningchart = {
  chart: {
      height: 360,
      type: 'radialBar',
  },
  plotOptions: {
      radialBar: {
          startAngle: -135,
          endAngle: 135,
          dataLabels: {
              name: {
                  fontSize: '16px',
                  color: '#000000',
                  offsetY: 120
              },
              value: {
                  offsetY: 76,
                  fontSize: '22px',
                  color: '#000000',
                  formatter: function (val) {
                      return val + "";
                  }
              }
          }
      }
  },

  fill: {
      opacity: 1
  },
  colors:['#1ca185'],

  stroke: {
      dashArray: 4
  },
  series: [{{$happyIndex}}],
  labels: ['Happy Index'],

}

var chartearningchart = new ApexCharts(
    document.querySelector("#chart-widget5"),
    optionsearningchart
);
chartearningchart.render();

var lastMonthList = <?php echo json_encode(array_values($lastMonthList));?>;
Morris.Bar({
      element: 'stacked-bar-chart',
      data: lastMonthList,
      xkey: "a",
      ykeys: ["c", "b", "y", "z"],
      labels: ["Excellent", "Good", "Poor", "Bad"],
      barColors: ["#1ca185" , "#92c462" , "#f39c26", "#e94545"],
      stacked: !0
  });

  })(jQuery);
</script>
@endsection