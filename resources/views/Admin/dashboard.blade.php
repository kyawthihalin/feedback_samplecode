@extends('layouts.layout')
@section('css')

@endsection
@section('content')
      <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6">
                  <h3>Dashboard</h3>
                </div>
                <div class="col-lg-6">
                <!-- <button class="btn btn-pill btn-primary btn-air-primary btn-sm float-right">
                  <a href="/admin/export-admin-excel" class="btn">Export Excel</a>
                </button> -->
                <a class="btn btn-pill btn-primary btn-air-primary btn-sm float-right" href="/admin/export-admin-excel" role="button">
                  Export Excel
                </a>
                </div>
                <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Analytics</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <div class="col-xl-4 col-md-12 box-col-12">
                <div class="card o-hidden">
                  <div class="chart-widget-top">
                    <div class="row card-body">
                      <div class="col-12">
                        <h4 class="f-w-600 font-primary">Revenue</h4>
                      </div>
                      <div class="col-12">
                        <h5 class="num total-value">$ <span class="counter">0</span>.00</h5>
                      </div>
                    </div>
                    <div>
                      <div id="chart-widget1"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-md-12 box-col-12">
                <div class="card o-hidden">
                  <div class="chart-widget-top">
                    <div class="row card-body">
                      <div class="col-12">
                        <h4 class="f-w-600 font-secondary">Active User</h4>
                      </div>
                      <div class="col-12">
                        <h4 class="num total-value counter">{{ $totalActiveUser }}</h4>
                      </div>
                    </div>
                    <div id="chart-widget2">
                      <div class="flot-chart-placeholder" id="chart-widget-top-second"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-md-12 box-col-12">
                <div class="card o-hidden">
                  <div class="chart-widget-top">
                    <div class="row card-body">
                      <div class="col-12">
                        <h4 class="f-w-600 font-success">Total Click</h4>
                      </div>
                      <div class="col-12">
                        <h4 class="num total-value"><span class="counter">{{ $totalClick }}</span></h4>
                      </div>
                    </div>
                    <div id="chart-widget3">
                      <div class="flot-chart-placeholder" id="chart-widget-top-third"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-6 col-sm-12 box-col-6">
                <div class="card">
                  <div class="card-header d-flex justify-content-between">
                    <h5>Country</h5>
                    <div class="text-right">
                      <button class="btn" id="country-pdf" data-chart="pain-point">
                         <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                      </button>
                       | 
                      <button class="btn" name="excel-single" id="single-excel-export" data-chart="country">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div id="basic-bar"></div>
                  </div>
                </div>
              </div>
              <div class="col-xl-6 col-sm-12 box-col-6">
                <div class="card">
                  <div class="card-header d-flex justify-content-between">
                      <h5>Active User</h5>
                      <div>
                        <button class="btn" id="month-pdf" data-chart="pain-point">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
                        </button>
                          | 
                        <button class="btn" name="excel-single" id="single-excel-export" data-chart="month">
                          <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                      </div>
                  </div>
                  <div class="card-body">
                    <div id="basic-apex"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-header d-flex justify-content-between">
                    <h5>Industry</h5>
                    <div>
                      <button class="btn" id="industry-pdf" data-chart="pain-point">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                      </button>
                       | 
                      <button class="btn" name="excel-single" id="single-excel-export" data-chart="industry">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body apex-chart pt-5">
                    <div id="piechart"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
     </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="{{asset('assets/js/chart/apex-chart/stock-prices.js')}}"></script>
<!-- <script src="{{asset('assets/js/chart/apex-chart/chart-custom.js')}}"></script> -->
<!-- <script src="{{asset('assets/js/chart-widget.js')}}"></script> -->
<script>
const { jsPDF } = window.jspdf;
/*Line chart*/
var optionslinechart = {
  chart: {
      toolbar: {
          show: false
      },
      height: 170,
      type: 'area'
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      curve: 'smooth'
  },
  xaxis: {
      show: false,
      type: 'datetime',
      categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],
      labels: {
          show: false,
      },
      axisBorder: {
          show: false,
      },
  },
  grid: {
      show: false,
      padding: {
          left: 0,
          right: 0,
          bottom: -40
      }
  },
  fill: {
      type: 'gradient',
      gradient: {
          shade: 'light',
          type: 'vertical',
          shadeIntensity: 0.4,
          inverseColors: false,
          opacityFrom: 0.8,
          opacityTo: 0.2,
          stops: [0, 100]
      },

  },
  colors:[CubaAdminConfig.primary],

  series: [
      {
          name: 'Revenue',
          data: [1.2, 2.3, 1.7, 3.2, 1.8, 3.2, 1]
      }
  ],
  tooltip: {
      x: {
          format: 'dd/MM/yy HH:mm'
      }
  }
};

var chartlinechart = new ApexCharts(
  document.querySelector("#chart-widget1"),
  optionslinechart
);

chartlinechart.render();


var userByHourCategory = <?php echo json_encode($userByHourCategory);?>; 
var userByHourData = <?php echo json_encode($userByHourData);?>; 
var optionslinechart2 = {
    chart: {
        toolbar: {
            show: false
        },
        height: 170,
        type: 'area'
    },
    // title: {
    //     text: 'Active users during last six hours',
    //     align: 'left'
    // },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'smooth'
    },
    xaxis: {
        show: false,
        type: 'text',
        categories: userByHourCategory,
        labels: {
            show: false,
        },
        axisBorder: {
            show: false,
        },
    },
    grid: {
        show: false,
        padding: {
            left: 0,
            right: 0,
            bottom: -40
        }
    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'light',
            type: 'vertical',
            shadeIntensity: 0.4,
            inverseColors: false,
            opacityFrom: 0.8,
            opacityTo: 0.2,
            stops: [0, 100]
        }

    },
    colors:[CubaAdminConfig.secondary],


    series: [
        {
            name: 'Active User',
            data: userByHourData
        }
    ],
    // tooltip: {
    //     x: {
    //         format: 'dd/MM/yy HH:mm'
    //     }
    // }
};

var chartlinechart2 = new ApexCharts(
    document.querySelector("#chart-widget2"),
    optionslinechart2
);
chartlinechart2.render();

/*Line chart3*/

var clickByHourCategory = <?php echo json_encode($clickByHourCategory);?>; 
var clickByHourData = <?php echo json_encode($clickByHourData);?>; 
var optionslinechart3 = {
        chart: {
            toolbar: {
                show: false
            },
            height: 170,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            show: false,
            type: 'text',
            categories: clickByHourCategory,
            labels: {
                show: false,
            },
            axisBorder: {
                show: false,
            },
        },
        grid: {
            show: false,
            padding: {
                left: 0,
                right: 0,
                bottom: -40
            }
        },
        fill: {
            colors:['#7366ff'],
            type: 'gradient',
            gradient: {
                shade: 'light',
                type: 'vertical',
                shadeIntensity: 0.4,
                inverseColors: false,
                opacityFrom: 0.8,
                opacityTo: 0.2,
                stops: [0, 100]
            },

        },
        colors:['#7366ff'],

        series: [
            {
                name: 'Total Click',
                data: clickByHourData
            }
        ],
        // tooltip: {
        //     x: {
        //         format: 'dd/MM/yy HH:mm'
        //     }
        // }
    };

    var chartlinechart3 = new ApexCharts(
        document.querySelector("#chart-widget3"),
        optionslinechart3
    );
    chartlinechart3.render();




var countryCategory = <?php echo json_encode($countryCategory);?>; 
var countryData = <?php echo json_encode($countryData);?>; 
var optionCountry = {
    chart: {
        height: 350,
        type: 'bar',
        toolbar:{
          show: false
        }
    },
    plotOptions: {
        bar: {
            horizontal: true,
        }
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        name: 'Active User',
        data: countryData
    }],
    xaxis: {
        categories: countryCategory,
    },
    colors:[ CubaAdminConfig.primary ]
}

var countryChart = new ApexCharts(
    document.querySelector("#basic-bar"),
    optionCountry
);

countryChart.render();


var industryCategory = <?php echo json_encode($industryCategory);?>; 
var industryData = <?php echo json_encode($industryData);?>; 
var industryOption = {
    chart: {
        width: 380,
        type: 'pie',
    },
    labels: industryCategory,
    series: industryData,
    responsive: [{
        breakpoint: 480,
        options: {
            chart: {
                width: 200
            },
            legend: {
                position: 'bottom'
            }
        }
    }],
    colors:[ CubaAdminConfig.primary , CubaAdminConfig.secondary , '#51bb25', '#a927f9', '#f8d62b']
}

var industryChart = new ApexCharts(
    document.querySelector("#piechart"),
    industryOption
);

industryChart.render();


var userLastSixMonthCategory = <?php echo json_encode($userByMonthCategory);?>;
var userByMonthData = <?php echo json_encode($userByMonthData);?>;
var activeUserByMonthOption = {
    chart: {
        height: 350,
        type: 'area',
        zoom: {
            enabled: false
        },
        toolbar:{
          show: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    series: [{
        name: "Active User",
        data: userByMonthData
    }],
    // title: {
    //     text: 'Active users during last six months',
    //     align: 'left'
    // },
    // subtitle: {
    //     text: 'Price Movements',
    //     align: 'left'
    // },
    labels: userLastSixMonthCategory,
    xaxis: {
        type: 'text',
    },
    yaxis: {
        opposite: true
    },
    legend: {
        horizontalAlign: 'left'
    },
    colors:[ CubaAdminConfig.primary ]

}

var activeUserByMonthChart = new ApexCharts(
    document.querySelector("#basic-apex"),
    activeUserByMonthOption
);

activeUserByMonthChart.render();

$("button[name='excel-single']").click(function() {
  var chart = $(this).attr('data-chart');

  window.location.href = '/admin/export-admin-excel?chart=' + chart;
});

$('#industry-pdf').click(function(){
    var dataUri = industryChart.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text('Dashboard', 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Industry', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("DashboardIndustryChartReport.pdf");
    });
  });

  $('#country-pdf').click(function(){
    var dataUri = countryChart.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text('Dashboard', 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Country', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("DashboardCountryChartReport.pdf");
    });
  });

  $('#month-pdf').click(function(){
    var dataUri = activeUserByMonthChart.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text('Dashboard', 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Month', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("DashboardMonthChartReport.pdf");
    });
  });

</script>
@endsection