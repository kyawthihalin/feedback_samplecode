@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">

@endsection
@section('content')
      
        <div class="page-body">
          <div class="container-fluid">
            <div class="page-header">
              <div class="row">
                <div class="col-lg-6">
                  <h3>Analytics</h3>
                </div>
                <div class="col-lg-6">
                  @if ($lastQuestion)
                  <button class="btn btn-pill btn-primary btn-air-primary btn-sm float-right" id="export-excel">Export Excel</button>
                  @endif
                </div>
                <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                    <li class="breadcrumb-item">Dashboard</li>
                    <li class="breadcrumb-item active">Analytics</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid starts-->
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-12 col-xl-12 box-col-12">
                <div class="card">
                  <div class="card-body" id="monthlyData">
                    <div class="d-flex justify-content-between">
                      {{-- <div><h5>Result </h5> <p class="mt-1"> ({{$currentMonth}})</p></div>  --}}
                      <div><h5>Result </h5></div> 
                      @if ($lastQuestion)
                      <div>
                        <button class="btn" id="month-pdf" data-chart="month">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </button>
                         | 
                        <button class="btn" id="single-excel-export" name="excel-single" data-chart="month">
                          <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                      </div>
                      @endif
                    </div>
                    <div id="column-chart"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between pb-2">
                      <h5>Hourly Distribution</h5>
                      @if ($lastQuestion)
                      <div>
                        <button class="btn" id="hourly-pdf" data-chart="hour">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </button>
                        | 
                        <button class="btn" id="single-excel-export" name="excel-single" data-chart="hour">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                      </div>
                      @endif
                    </div>
                    <div id="hourly-chart"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body">
                    <div class="d-flex justify-content-between pb-2">
                      <h5>Weekly Distribution</h5>
                      @if ($lastQuestion)
                      <div>
                        <button class="btn" id="weekly-pdf" data-chart="week">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </button>
                        | 
                        <button class="btn" name="excel-single" id="single-excel-export" data-chart="week">
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                      </div>
                      @endif
                    </div>
                    <div id="weekly-chart"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body chart-block">
                     <div class="d-flex justify-content-between pb-2">
                      <h5>Pie Chart</h5>
                      @if ($lastQuestion)
                      <div>
                        <button class="btn" id="pie-pdf" data-chart="pie">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                        </button>
                        | 
                        <button class="btn" name="excel-single" id="single-excel-export" data-chart="pie">
                          <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </button>
                      </div>
                      @endif
                     </div>
                    <div id="pie-editor"></div>
                    <div class="chart-overflow" id="pie-chart2"></div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body progress-showcase">
                    <div class="pro-head">
                      <div class="d-flex justify-content-between pb-2">
                        <h5 class="pb-4">Comprison</h5>
                        @if ($lastQuestion)
                        <div>
                          <button class="btn" id="comprison-pdf" data-chart="comprison">
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                          </button>
                          | 
                          <button class="btn" name="excel-single" id="single-excel-export" data-chart="comprison">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                          </button>
                        </div>
                        @endif
                      </div>
                    </div>
                    <div class="pro-body" id="comprison">
                      @if (count($comparisonArr) > 0)
                      @foreach ($comparisonArr as $name => $comparison)    
                      <div class="progress" style="height: 11px;">
                        <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['bad']) ? $comparison['bad'] : 0 }}%; background: #e94545;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['poor']) ? $comparison['poor'] : 0 }}%; background:#f39c26;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['excellent']) ? $comparison['excellent'] : 0 }}%; background: #92c462;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                        <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['good']) ? $comparison['good'] : 0 }}%; background:#1ca185;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                      <div class="progress-text pb-2">
                        <small>
                          {{$name}}
                            <span class="pull-right m-l-10 txt-primary">{{ $comparison['total'] ? $comparison['total'] : 0 }}</span>
                        </small>
                      </div>
                      @endforeach
                      @else 
                      No Data
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body progress-showcase">
                    <div class="pro-head">
                      <div class="d-flex justify-content-between pb-2">
                        <h5 class="pb-4">Highlights</h5>
                        @if ($lastQuestion)
                        <div>
                          <button class="btn" id="highlight-pdf" data-chart="highlight">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                          </button>
                          | 
                          <button class="btn" name="excel-single" id="single-excel-export" data-chart="highlight">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                          </button>
                        </div>
                        @endif
                      </div>
                    </div>
                    <div class="pro-body" id="highlight">
                      @if (count($positiveAnsArr) > 0)    
                      @foreach ($positiveAnsArr as $name => $positiveAns) 
                      <div class="progress" style="height: 11px;">
                        <div class="progress-bar" role="progressbar" style="width: {{isset($positiveAns['good']) ? $positiveAns['good'] : 0}}%; background: #92c462;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                          <div class="progress-bar" role="progressbar" style="width: {{isset($positiveAns['excellent']) ? $positiveAns['excellent'] : 0}}%; background:#1ca185;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      <div class="progress-text pb-2">
                        <small>
                          {{ $name }}
                        <span class="pull-right m-l-10 txt-primary">{{ $positiveAns['total'] ? $positiveAns['total'] : 0 }}</span>
                        </small>
                      </div>
                      @endforeach
                      @else 
                      No Data
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-body progress-showcase">
                    <div class="pro-head">
                      <div class="d-flex justify-content-between pb-2">
                        <h5 class="pb-4">Pain Points</h5>
                        @if ($lastQuestion)
                        <div>
                          <button class="btn" id="pain-point-pdf" data-chart="pain-point">
                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                          </button>
                          | 
                          <button class="btn" name="excel-single" id="single-excel-export" data-chart="pain-point">
                            <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                          </button>
                        </div>
                        @endif
                      </div>
                    </div>
                    <div class="pro-body" id="pain-point">
                      @if (count($negativeAnsArr) > 0)    
                      @foreach ($negativeAnsArr as $name => $negativeAns) 
                      <div class="progress" style="height: 11px;">
                        <div class="progress-bar" role="progressbar" style="width: {{isset($negativeAns['poor']) ? $negativeAns['poor'] : 0}}%; background:#f39c26;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="80"></div>
                          <div class="progress-bar" role="progressbar" style="width: {{isset($negativeAns['bad']) ? $negativeAns['bad'] : 0}}%; background: #e94545;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      <div class="progress-text pb-2">
                        <small>
                          {{ $name }}
                        <span class="pull-right m-l-10 txt-primary">{{ isset($negativeAns['total']) ? $negativeAns['total'] : 0 }}</span>
                        </small>
                      </div>
                      @endforeach
                      @else 
                      No Data
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6 notification box-col-6">
                <div class="card">
                  <div class="card-header card-no-border">
                    <div class="header-top">
                      <h5 class="m-0">Real Time Alerts</h5>
                      <div class="card-header-right-icon">
                        <!-- <select class="button btn btn-primary">
                          <option>Today</option>
                          <option>Tomorrow</option>
                          <option>Yesterday</option>
                        </select> -->
                      </div>
                    </div>
                  </div>
                  <div class="card-body pt-0" style="overflow-y: scroll; height:400px;" id="alert-con">
                    @if (count($alerts) > 0)
                      @foreach ($alerts as $alert)    
                      <div class="media">
                        <div class="media-body">
                        <p>{{ Timezone::convertToLocal(($alert->created_at), 'd-m-Y') }}<span class="pl-1">{{ Timezone::convertToLocal(($alert->created_at), 'h:i A') }}</span></p>
                          <h6>{{$alert->subbranch->name}}<span class="dot-notification"></span></h6><span>{{$alert->question->question_text}}</span>
                        </div>
                      </div>
                      @endforeach
                    @else
                     There's no notification for feeback.
                    @endif

                    {{-- <div class="media">
                      <div class="media-body">
                        <p>Today <span class="pl-1">12:30PM-1:00PM</span><span class="badge badge-secondary">New</span></p>
                        <h6>Checkout<span class="dot-notification"></span></h6><span>Negative feedback response rate is 60%. . .</span>
                      </div>
                    </div> --}}
                  </div>
                </div>
              </div>
              <div class="col-sm-12 col-xl-6 box-col-6">
                <div class="card">
                  <div class="card-header card-no-border">
                    <h5>Comments</h5>
                  </div>
                  <div class="card-body new-update pt-0" style="overflow-y: scroll; height:400px;">
                    <div class="activity-timeline">
                      @if (count($comments) > 0)
                      @foreach ($comments as $comment)   
                      <div class="media">
                        <div class="media-body"><span>{{$comment->other}}</span>
                          <p class="font-roboto">{{$comment->question->question_text}}</p>
                        </div>
                      </div>
                      @endforeach
                      @else
                      There's no comments.
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Container-fluid Ends-->
        
      </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/jspdf.umd.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.1.1/jspdf.umd.min.js"></script> --}}
{{-- <script src="https://unpkg.com/jspdf@latest/dist/jspdf.umd.min.js"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<script src="{{asset('assets/js/chart/google/google-chart-loader.js')}}"></script>
<script src="{{asset('assets/js/chart/google/google-chart.js')}}"></script>
<script src="{{asset('assets/js/chart/charttt/stock-prices.js')}}"></script>
<script src="{{asset('assets/js/chart/charttt/chart-custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<script>
  Pusher.logToConsole = true;
var pusher = new Pusher('a941dece349437db3518', {
  cluster: 'ap1',
});

var channel = pusher.subscribe('bad-feedback');

channel.bind('bad-feedback-event', function(data) {
  if(data.subbranch.user == {{auth()->user()->id}}){
    var oldAlert = $('#alert-con').html();
    var newAlert = '<div class="media"><div class="media-body"><p>'+ data.subbranch.date +' <span class="pl-1">'+ data.subbranch.time +'</span></p><h6>'+ data.subbranch.subbranch_name +'<span class="dot-notification"></span></h6><span>'+ data.subbranch.question+ '</span></div></div>';

    var finalAlert = newAlert + oldAlert;

    $('#alert-con').html(finalAlert);
  }
});

const { jsPDF } = window.jspdf;

var currentFilter = <?php echo json_encode($currentBranchName);?> + ', ' + <?php echo json_encode($lastQuestionName);?> + ', ' + <?php echo json_encode($currentDate);?>;

var dayMonthList = <?php echo json_encode(array_values($dayOfMonthList));?>;
var databyMonthExcellent = <?php echo json_encode(array_values($databyMonthExcellent));?>;
var databyMonthGood = <?php echo json_encode(array_values($databyMonthGood));?>;
var databyMonthPoor = <?php echo json_encode(array_values($databyMonthPoor));?>;
var databyMonthBad = <?php echo json_encode(array_values($databyMonthBad));?>;

var options3 = {
  chart: {
      height: 350,
      type: 'bar',
      toolbar: {
        show: false,
      },
  },
  plotOptions: {
      bar: {
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '55%',
      },
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      show: true,
      width: 2,
      colors: ['transparent']
  },
  series: [{
      name: 'Excellent',
      data: databyMonthExcellent
  }, {
      name: 'Good',
      data: databyMonthGood
  }, {
      name: 'Poor',
      data: databyMonthPoor
  }, {
      name: 'Bad',
      data: databyMonthBad
  }],
  xaxis: {
      categories: dayMonthList,
  },
  fill: {
      opacity: 1

  },
  tooltip: {
      y: {
          formatter: function (val) {
              return val
          }
      }
  },
  colors:["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
}

var chart3 = new ApexCharts(
    document.querySelector("#column-chart"),
    options3
);

chart3.render();

// $('#full-page-pdf').click(function(){
  // var doc = new jsPDF();
  // var element = document.getElementById('monthlyData');
  // doc.addHTML(element);
  // doc.save('div.pdf');

  // html2canvas($("#column-chart").get(0), {
  //     onrendered: function(canvas) {
  //       var img = canvas.toDataURL('image/png');
  //       console.log(img);
  //       var doc = new jsPDF('p', 'mm', 'a4');
  //       var width = doc.internal.pageSize.width;
  //       var height = doc.internal.pageSize.height;
  //       doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
  //       doc.save('test.pdf');
  //     }
  //  });


//   chart3.dataURI().then(({ imgURI, blob }) => {
//     var pdf = new jsPDF();
//     pdf.addImage(imgURI, 'PNG', 0, 0);
//     pdf.save("download.pdf");
//   })

// });


var hourList = <?php echo json_encode(array_values($hourList));?>;
var dataExcellent = <?php echo json_encode(array_values($hourlyExcellentArr));?>;
var dataGood = <?php echo json_encode(array_values($hourlyGoodArr));?>;
var dataPoor = <?php echo json_encode(array_values($hourlyPoorArr));?>;
var dataBad = <?php echo json_encode(array_values($hourlyBadArr));?>;

var optionsHourlyChart = {
    chart: {
        height: 350,
        type: 'bar',
        toolbar:{
          show: false
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Excellent',
        data: dataExcellent
    }, {
        name: 'Good',
        data: dataGood
    }, {
        name: 'Poor',
        data: dataPoor
    }, {
        name: 'Bad',
        data: dataBad
    }],
    xaxis: {
        categories: hourList,
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val
            }
        }
    },
    colors:["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
}

var hourlyChart = new ApexCharts(
    document.querySelector("#hourly-chart"),
    optionsHourlyChart
);

hourlyChart.render();

var weekDayList = <?php echo json_encode(array_values($weekDayList));?>;
var weeklyDataExcellent = <?php echo json_encode(array_values($weekExcellentArr));?>;
var weeklyDataGood = <?php echo json_encode(array_values($weekGoodArr));?>;
var weeklyDataPoor = <?php echo json_encode(array_values($weekPoorArr));?>;
var weeklyDataBad = <?php echo json_encode(array_values($weekBadArr));?>;

var weeklyOption = {
    chart: {
        height: 350,
        type: 'bar',
        toolbar:{
          show: false
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            endingShape: 'rounded',
            columnWidth: '55%',
        },
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
    },
    series: [{
        name: 'Excellent',
        data: weeklyDataExcellent
    }, {
        name: 'Good',
        data: weeklyDataGood
    }, {
        name: 'Poor',
        data: weeklyDataPoor
    }, {
        name: 'Bad',
        data: weeklyDataBad
    }],
    xaxis: {
        categories: weekDayList,
    },
    fill: {
        opacity: 1

    },
    tooltip: {
        y: {
            formatter: function (val) {
                return val
            }
        }
    },
    colors:["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
}

var weeklyChart = new ApexCharts(
    document.querySelector("#weekly-chart"),
    weeklyOption
);

weeklyChart.render();

      function fullPagePDF() {
        // chart3.dataURI().then(({ imgURI, blob }) => {
        //     var doc = new jsPDF('p', 'mm', 'a4');
        //     var width = doc.internal.pageSize.width;
        //     var height = doc.internal.pageSize.height;
        //     doc.addImage(imgURI, 'PNG', 10, 20, parseInt(width) - 20, 100);
        //   doc.save("download.pdf");
        // })
        // var img = chart.getImageURI();
        var doc = new jsPDF('p', 'mm', 'a4');
        
        // var width = doc.internal.pageSize.width;
        // var height = doc.internal.pageSize.height;
        // doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
        // doc.save('test.doc');

        // html2canvas($("#pain-point").get(0), {
        //     onrendered: function(canvas) {
        //       var img = canvas.toDataURL('image/png');
        //       console.log(img);
        //       var doc = new jsPDF('p', 'mm', 'a4');
        //       var width = doc.internal.pageSize.width;
        //       var height = doc.internal.pageSize.height;
        //       doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
              doc.save('test.pdf');
        //     }
        // });


      }

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', {'packages':['line']});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawBasic);

var response = {{ json_encode($totalFeedback)}};

function drawBasic() {

  if ($("#pie-chart2").length > 0) {
        var data = google.visualization.arrayToDataTable([
          ['Response', response.toString()],
          ['Excellent',  {{json_encode($percentExcellent)}}],
          ['Good',  {{json_encode($percentGood)}}],
          ['Poor',  {{json_encode($percentPoor)}}],
          ['Bad', {{json_encode($percentBad)}}]
        ]);
        var options = {
          title: 'Responses: ' + response.toString(),
          is3D: true,
          width:'100%',
          height: 300,
          colors: ["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
        };
        var chart = new google.visualization.PieChart(document.getElementById('pie-chart2'));
        chart.draw(data, options);
    }

    // $('#full-page-pdf').click(function(){
    //   var img = chart.getImageURI();
    //     // console.log(img);
    //     var doc = new jsPDF('p', 'mm', 'a4');
    //     var width = doc.internal.pageSize.width;
    //     var height = doc.internal.pageSize.height;
    //     // doc.addImage(img, 0, 0);
    //     chart3.dataURI().then(({ imgURI, blob }) => {
    //         doc.addImage(imgURI, 'PNG', 10, 20, parseInt(width) - 20, 100);
    //     })
    //     // doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
    //     doc.save("download.pdf");
    // });

    $('#pie-pdf').click(function(){
      var img = chart.getImageURI();
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Pie Chart', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(img, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("AnalysisPieChartReport.pdf");
  });

    
  }

  $('#month-pdf').click(function(){
    var dataUri = chart3.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Result', 10, 40);
      doc.line(10, 42, 30, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("AnalysisResultChartReport.pdf");
    });
  });

  $('#hourly-pdf').click(function(){
    var dataUri = hourlyChart.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Hourly Distribution', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("AnalysisHourlyChartReport.pdf");
    });
  });

  $('#weekly-pdf').click(function(){
    var dataUri = weeklyChart.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Weekly Distribution', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(imgURI, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("AnalysisWeeklyChartReport.pdf");
    });
  });

  $('#comprison-pdf').click(function(){
    html2canvas($("#comprison").get(0), {
      onrendered: function(canvas) {
        var img = canvas.toDataURL('image/png');
        console.log(img);
        var doc = new jsPDF();
        var width = doc.internal.pageSize.width;
        var height = doc.internal.pageSize.height;
        doc.setFontSize(13);
        doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
        doc.line(10, 22, parseInt(width) - 10, 22);
        doc.text('Comparison', 10, 40);
        doc.line(10, 42, 50, 42);
        doc.addImage(img, 'PNG', 10, 60, parseInt(width) - 20, 100);
        doc.save('AnalysisComparisonReport.pdf');
      }
    });
  });

  $('#highlight-pdf').click(function(){
    html2canvas($("#highlight").get(0), {
      onrendered: function(canvas) {
        var img = canvas.toDataURL('image/png');
        console.log(img);
        var doc = new jsPDF();
        var width = doc.internal.pageSize.width;
        var height = doc.internal.pageSize.height;
        doc.setFontSize(13);
        doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
        doc.line(10, 22, parseInt(width) - 10, 22);
        doc.text('Hight Light', 10, 40);
        doc.line(10, 42, 50, 42);
        doc.addImage(img, 'PNG', 10, 80, parseInt(width) - 20, 50);
        doc.save('AnalysisHighlightReport.pdf');
      }
    });
  });

  $('#pain-point-pdf').click(function(){
    html2canvas($("#pain-point").get(0), {
      onrendered: function(canvas) {
        var img = canvas.toDataURL('image/png');
        console.log(img);
        var doc = new jsPDF();
        var width = doc.internal.pageSize.width;
        var height = doc.internal.pageSize.height;
        doc.setFontSize(13);
        doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
        doc.line(10, 22, parseInt(width) - 10, 22);
        doc.text('Pain Point', 10, 40);
        doc.line(10, 42, 50, 42);
        doc.addImage(img, 'PNG', 10, 80, parseInt(width) - 20, 50);
        doc.save('AnalysisPainPointReport.pdf');
      }
    });
  });

function demoFromHTML() {
  var dataURL = chart3.dataURI().then(({ imgURI, blob }) => {
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.addImage(imgURI, 'PNG', 10, 20, parseInt(width) - 20, 100);
    doc.save("download.pdf");
  })
  // var img = chart3.dataURI();
  // var doc = new jsPDF('p', 'mm', 'a4');
  //     var width = doc.internal.pageSize.width;
  //     var height = doc.internal.pageSize.height;
  //     doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
  //   doc.save("download.pdf");

}

$("button[name='excel-single']").click(function() {
  var chart = $(this).attr('data-chart');

  let urlOrigin = window.location.href;
  var url = new URL(urlOrigin);

  let branch = url.searchParams.get("branch");
  let question = url.searchParams.get("question");
  let date = url.searchParams.get("date");
  let exportUrl = 'export-merchant-excel';
  
  if(branch == null && question == null && date == null) {

    exportUrl = 'export-merchant-excel?chart=' + chart;

  } else {
    
    exportUrl = exportUrl + '?' + 'branch=' + branch + '&question=' + question + '&date=' + date + '&chart=' + chart;
    
  }

  window.location.href = exportUrl;

});

$("#export-excel").click(function() {
  let urlOrigin = window.location.href;
  var url = new URL(urlOrigin);

  let branch = url.searchParams.get("branch");
  let question = url.searchParams.get("question");
  let date = url.searchParams.get("date");
  let exportUrl = 'export-merchant-excel';
  
  if(branch == null && question == null && date == null) {

    exportUrl = 'export-merchant-excel';

  } else {
    
    exportUrl = exportUrl + '?' + 'branch=' + branch + '&question=' + question + '&date=' + date;
    
  }

  window.location.href = exportUrl;
  // $.ajax({
  //   url: exportUrl,
  //   type: 'get',
  //   async: false,
  //   success: function (responese) {

  //   },
  //   error: function (responese) {
      
  //   }
  // });
})
</script>
@endsection