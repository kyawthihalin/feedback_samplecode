@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/sweetalert2.css')}}">
<style>
.cards{
  border-radius:10px !important;
  transition: all 0.4s;
}
.cards:hover{
    transform: scaleX(1.09);
}
::-webkit-scrollbar {
  width: 3px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
</style>
@endsection
@section('content')
<div class="page-body">

<div class="container-fluid">
  <div class="page-header">
    <div class="row">
      <div class="col-lg-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/admin"><i class="f-16 fa fa-home"></i></a></li>
          <li class="breadcrumb-item">Manage Branch </li>
          <li class="breadcrumb-item">Branch </li>
        </ol>
        <h3>Branch</h3>
      </div>
    </div>
  </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
  <div class="email-wrap bookmark-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="nav main-menu contact-options" role="tablist">
          <div class="nav-item float-right">

            <div class="modal fade modal-bookmark" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Branch</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                    <form class="form-bookmark needs-validation" id="bookmark-form" novalidate="">
                      <div class="form-row">
                        <div class="form-group mb-3 col-md-12">
                          <label for="con-name">Name</label>
                          <div class="row">
                            <div class="col-sm-12">
                              <input class="form-control" id="con-name" name="name" type="text" required placeholder="Branch Name" autocomplete="off">
                            <small id="errorName" style="color: red;"></small>

                            </div>
                          </div>
                        </div>
                        <div class="form-group mb-3 col-md-12">
                          <label for="con-user">User</label>
                          <select class="form-control" name="user" id="con-user">
                          <option>Choose A User</option>
                          @foreach($users as $user)
                          <option value="{{ $user->id }}">
                            {{ $user->name }}
                          </option>
                          @endforeach
                          </select>
                          <small id="errorUser" style="color: red;"></small>

                        </div>
                        <div class="form-group mb-3 col-md-12">
                          <label for="con-mail">Email Address</label>
                          <input class="form-control" id="con-mail" type="text" name="email" placeholder="Email Address" required autocomplete="off">
                          <small id="errorEmail" style="color: red;"></small>

                        </div>
                        
                        <div class="form-group mb-3 col-md-12 my-0">
                          <label for="con-phone">Phone</label>
                          <div class="row">
                            <div class="col-sm-12">
                              <input class="form-control" id="con-phone" type="number" name="phone" placeholder="Phone" required autocomplete="off">
                              <small id="errorPhone" style="color: red;"></small>
                            </div>
                          </div>
                        </div>
                        <div class="form-group mb-3 col-md-12 my-0">
                          <label class=" col-form-label text-right">Active Weekdays</label>
                          <div class="">
                            <div class="m-checkbox-inline">
                              <div class="checkbox checkbox-dark">
                                <input id="inline-1" class="days" value="Monday" name="day" type="checkbox">
                                <label for="inline-1">Mon</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-2" class="days" value="Tue" name="day" type="checkbox">
                                <label for="inline-2">Tue</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-3" class="days" value="Wed" name="day" type="checkbox">
                                <label for="inline-3"> Wed</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-4" class="days" value="Thu" name="day" type="checkbox">
                                <label for="inline-4">Thu</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-5" class="days"  value="Fri" name="day" type="checkbox">
                                <label for="inline-5"> Fri</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-6" class="days" value="Sat" name="day" type="checkbox">
                                <label for="inline-6"> Sat</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-7"  class="days" value="Sun" name="day" type="checkbox">
                                <label for="inline-7">Sun</label>
                              </div>
                            </div>
                            <small style="color: red;" id="errorDay"></small>

                          </div>
                        </div>
                      </div>
                      <input id="index_var" type="hidden" value="5">
                      <button class="btn btn-secondary" type="submit" id="saveBranch">Save</button>
                      <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- //edit modal -->

            <div class="modal fade modal-bookmark" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="editModal">Edit Branch</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body">
                    <form class="form-bookmark needs-validation" id="bookmark-form" novalidate="">
                      <div class="form-row">
                        <div class="form-group mb-3 col-md-12">
                          <label for="con-name">Name</label>
                          <div class="row">
                            <div class="col-sm-12">
                              <input class="form-control" id="edit_con_name" name="name" type="text" required="" placeholder="Branch Name" autocomplete="off">
                              <input type="hidden" id="b_id">
                            </div>
                          </div>
                        </div>
                        <div class="form-group mb-3 col-md-12">
                          <label for="edit-user">User</label>
                          <select class="form-control" name="user" id="edit-user">
                          <option>Choose A User</option>
                          @foreach($users as $user)
                          <option value="{{ $user->id }}" class="userList" data-name="{{ $user->name }}" data-id="{{ $user->id }}">
                            {{ $user->name }}
                          </option>
                          @endforeach
                          </select>

                        </div>
                        <div class="form-group mb-3 col-md-12">
                          <label for="con-mail">Email Address</label>
                          <input class="form-control" id="edit_con_mail" type="text" name="email" placeholder="Email Address" required="" autocomplete="off">
                        </div>
                        <div class="form-group mb-3 col-md-12 my-0">
                          <label for="con-phone">Phone</label>
                          <div class="row">
                            <div class="col-sm-12">
                              <input class="form-control" id="edit_con_phone" type="number" name="phone" placeholder="Phone" required="" autocomplete="off">
                            </div>
                          </div>
                        </div>
                      </div>
                      <input id="index_var" type="hidden" value="5">
                      <button class="btn btn-secondary" id="updateBranch">Update</button>
                      <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- end -->
          </div>
        </div>
      </div>
      <div class="col-xl-12 col-md-12 box-col-12">
        <div class="email-right-aside bookmark-tabcontent contacts-tabs">
          <div class="card email-body radius-left">
            <div class="pl-0">
              <div class="tab-content">
                <div class="tab-pane fade active show" id="pills-personal" role="tabpanel" aria-labelledby="pills-personal-tab">
                  <div class="card mb-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3" style="overflow-y: scroll;height: 300px;">
                            <h3 class="mt-3 text-center">Your Branches</h3>
                    <div class="card cards mt-2" >
                        <div class="card-body text-center indigo text-white" data-target="#exampleModal" data-toggle="modal">
                            <a href="#"><i class="icon-plus"></i> <b>Add New Branch</b>
                           
                            </a>
                        </div>
                    </div>
                    @if($branches)
                    @foreach($branches as $branch)
                    <div class="card cards mt-2">
                        <div class="card-body text-center">
                            <a href="#" target="_blank"> <b>{{$branch->name}}</b></a>
                            
                            <div class="row mt-2">
                                <div class="col-md-12 float-right">
                                    <a href="#">
                                        <span class="badge badge-warning badge-pill" id="branchEdit" data-target="#editModal" data-toggle="modal" 
                                        data-id="{{$branch->id}}">Edit</span>
                                    </a>
                                    <a href="#">
                                        <span class="badge badge-danger badge-pill" id="branchDelete{{$branch->id}}" onclick="branchDelete('{{$branch->id}}')" data-value="{{$branch->id}}">Delete</span>
                                    </a>
                                    <a href="#">
                                        <span class="badge badge-primary badge-pill" id="branchDetail{{$branch->id}}" onclick="branchDetail('{{$branch->id}}')">Detail</span>
                                    </a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    @endforeach
                    @endif
                            </div>
                            <div class="col-md-9 profile-mail">
                                  <div class="card-body">
                                      <div class="row pt-3 ">
                                        <div class="col-md-12 ">
                                        @if($first_branch)
                                        <div class="email-general" id="b_detail">
                                          @php
                                          $createdAt = Carbon\Carbon::parse($first_branch->created_at);
                                          @endphp
                                          <div id="first">

                                          <h6 class="mb-3">{{$first_branch->name}}</h6>
                                          <ul>
                                              <li style="padding-bottom: 20px;">
                                                Name :
                                                <span class="font-primary first_name_1">
                                                  {{$first_branch->name}}
                                                </span>
                                              </li>
                                              <li style="padding-bottom: 20px;">
                                                User Name :
                                                <span class="font-primary first_name_1">
                                                  {{$first_branch->user->name}}
                                                </span>
                                              </li>
                                              <li style="padding-bottom: 20px;">
                                                Created Date :
                                                <span class="font-primary"> 
                                                  <span class="birth_day_1">
                                                    {{$createdAt->format('d')}}
                                                  </span>
                                                  <span class="birth_month_1 ml-1">
                                                    {{$createdAt->format('M')}}
                                                  </span>
                                                  <span class="birth_year_1 ml-1">
                                                    {{$createdAt->format('Y')}}
                                                  </span>
                                                </span>
                                              </li>
                                              <li style="padding-bottom: 20px;">Mobile No :<span class="font-primary mobile_num_1">{{$first_branch->phone}}</span></li>
                                              <li style="padding-bottom: 20px;">Email Address :<span class="font-primary email_add_1">{{$first_branch->email}}</span></li>
                                          </ul>
                                          </div>

                                        </div>
                                        @endif
                                        </div>
                                      </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/select2/select2-custom.js')}}"></script>
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
<script src="{{asset('assets/js/bookmark/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/js/contacts/custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
$('#saveBranch').click(function(event){
	event.preventDefault();
	let branchName = $('#con-name').val();
	let email =  $('#con-mail').val();
	let user_id =  $('#con-user').val();
	let phone = $('#con-phone').val();
	let token = '{{csrf_token()}}';
  console.log(user_id);
	$.ajax({
  url:'{{route('saveBranch')}}',
  type: 'POST',
  async: false,
  headers: {
    'X-CSRF-Token': token 
    },
  data: {
    branchName,
    user_id,
    email,
    phone
  },
  success: function (responese) {
    $('#exampleModal').modal('hide');
    swal({
        position: 'top-end',
        icon: 'success',
        title: 'New Branch Successfully saved',
        showConfirmButton: false,
    }) .then((willDelete) => {
            if (willDelete) {
                location.reload();
            }
        })
  },
  error: function (responese) {
    console.log(responese.responseJSON.errors);
    if(responese.responseJSON.errors){
      if(responese.responseJSON.errors.branchName){
        $('#errorName').text('Branch Name is Required');
        $('#con-name').css('border-color','red');
      } else{
        $('#errorName').text('');
        $('#con-name').css('border-color','black');
      }
      if(responese.responseJSON.errors.user_id){
        $('#user').text('Please Choose A User');
        $('#con-user').css('border-color','red');
      }else{
        $('#errorUser').text('');
        $('#con-user').css('border-color','black');
      }
      if(responese.responseJSON.errors.email){
        $('#errorEmail').text('Email is Required');
        $('#con-mail').css('border-color','red');
      }else{
        $('#errorEmail').text('');
        $('#con-mail').css('border-color','black');
      }
      if(responese.responseJSON.errors.phone){
        $('#errorPhone').text('Phone is Required');
        $('#con-phone').css('border-color','red');
      }
      else{
        $('#errorPhone').text('');
        $('#con-phone').css('border-color','black');
      }
    }
  }
}); 
});

$('#updateBranch').click(function(event){
	event.preventDefault();
	let branchName = $('#edit_con_name').val();
	let user_id =  $('#edit-user').val();
	let email =  $('#edit_con_mail').val();
  let phone = $('#edit_con_phone').val();
	let id = $('#b_id').val();
  
	let token = '{{csrf_token()}}';
	$.ajax({
  url:'{{route('updateBranch')}}',
  type: 'POST',
  async: false,
  headers: {
    'X-CSRF-Token': token 
    },
  data: {
    branchName,
    user_id,
    email,
    phone,
    id
  },
  success: function (responese) {
    $('#editModal').modal('hide');
    swal({
        position: 'top-end',
        icon: 'success',
        title: 'New Branch Successfully Updated',
        showConfirmButton: false,
    }) .then((willDelete) => {
            if (willDelete) {
                location.reload();
            }
        })
  },
}); 
});

let branchDetail = (id) => {
  let token = '{{csrf_token()}}';
  $.ajax({
          url:'{{route('detailBranch')}}',
          type: 'POST',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            $('#first').hide();
            $('#b_detail').html(responese);
          },
          }); 
}

$(document).ready(function(){
  $(document).on('click', '#branchEdit', function(){
    let id = $(this).data('id');
    let token = '{{csrf_token()}}';
    $.ajax({
      url:'{{route('editBranch')}}',
      type: 'POST',
      async: false,
      headers: {
        'X-CSRF-Token': token 
        },
      data: {
        id
      },
      success: function (responese) {
          $('#edit_con_name').val(responese.name)
          $('#edit_con_phone').val(responese.phone)
          $('#edit_con_mail').val(responese.email)
          $('#b_id').val(responese.id)
          for (var i = 0; i < responese.users.length; i++) {
              let userId = responese.users[i];
              $('.userList').each(function(){
                alert(this.data('id'));
                  if ($(this).data('id') == userId) {
                      $(this).prop("selected", true);
                  }
              });
          }
      },
      }); 
  });
});


// let branchEdit = (id) => {
//   let token = '{{csrf_token()}}';
//   $.ajax({
//           url:'{{route('editBranch')}}',
//           type: 'POST',
//           async: false,
//           headers: {
//             'X-CSRF-Token': token 
//             },
//           data: {
//             id
//           },
//           success: function (responese) {
//               $('#edit_con_name').val(responese.name)
//               $('#edit_con_phone').val(responese.phone)
//               $('#edit_con_mail').val(responese.email)
//               $('#b_id').val(responese.id)
//               for (var i = 0; i < responese.users.length; i++) {
//                   let userId = responese.users[i];
//                   $('.userList').each(function(){
//                       if ($(this).data('id') === userId) {
//                           $(this).prop("selected", true);
//                       }
//                   });
//               }
//           },
//           }); 
// }


let branchDelete= (id) =>{
  let token = '{{csrf_token()}}';
  swal({
            title: "Are you sure?",
            text: "This branch will be deleted from your branch list.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
          url:'{{route('deleteBranch')}}',
          type: 'POST',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Branch Successfully Deleted',
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          },
          error: function (responese) {
            swal("There is an error occoured!");
          }
          }); 
        } else {
            swal("Your branch is safe!");
        }
    })

};
</script>
@endsection