@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
@endsection
@section('content')
<!-- Container-fluid starts-->
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Subgroup</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Point</li>
                        <li class="breadcrumb-item active">Subgroup</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
      <div class="edit-profile" id="add">
        <div class="row">
          @if (auth()->user()->isAdmin())   
          <div class="col-md-12">
            <form class="card">
              <div class="card-header">
                <h5 class="card-title mb-0">Add New Subgroup</h5>
                <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="datetime-picker">
                      <form class="theme-form">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label text-right">Subgroup Name</label>
                          <div class="col-xl-5 col-sm-7 col-lg-8">
                            <input class="form-control" id="subName" type="text" placeholder="Subgroup Name">
                            <small style="color: red;" id="errorName"></small>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label text-right">Public Name</label>
                          <div class="col-xl-5 col-sm-7 col-lg-8">
                            <input class="form-control" id="subPublicName" type="text" placeholder="Public Name">
                            <small style="color: red;" id="errorPublicName"></small>

                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label text-right">Experience Point</label>
                          <div class="col-xl-5 col-sm-7 col-lg-8">
                            <select class="form-control btn-square" id="subBranch">
                            <option value="">Select Experience Point</option>
                              @foreach($branches as $branch)
                              <option value="{{$branch->id}}">{{$branch->name}}</option>
                              @endforeach
                            </select>
                            <small style="color: red;" id="errorBranch"></small>
                          </div>
                        </div>
                        {{-- <div class="form-group row">
                          <label class="col-sm-3 col-form-label text-right">Active Weekdays</label>
                          <div class="col-xl-5 col-sm-7 col-lg-8">
                            <div class="m-checkbox-inline">
                              <div class="checkbox checkbox-dark">
                                <input id="inline-1" class="days" value="Monday" name="day" type="checkbox">
                                <label for="inline-1">Mon</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-2" class="days" value="Tue" name="day" type="checkbox">
                                <label for="inline-2">Tue</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-3" class="days" value="Wed" name="day" type="checkbox">
                                <label for="inline-3"> Wed</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-4" class="days" value="Thu" name="day" type="checkbox">
                                <label for="inline-4">Thu</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-5" class="days"  value="Fri" name="day" type="checkbox">
                                <label for="inline-5"> Fri</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-6" class="days" value="Sat" name="day" type="checkbox">
                                <label for="inline-6"> Sat</label>
                              </div>
                              <div class="checkbox checkbox-dark">
                                <input id="inline-7"  class="days" value="Sun" name="day" type="checkbox">
                                <label for="inline-7">Sun</label>
                              </div>
                            </div>
                            <small style="color: red;" id="errorDay"></small>

                          </div>
                        </div> --}}
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label text-right" for="datefilter">
                            Start Date - End Date
                          </label>
                          <div class="col-xl-5 col-sm-7 col-lg-8">
                            <input class="form-control dataRange" type="text" name="datefilter" id="datefilter" value="" placeholder="Start Date - End Date" autocomplete="off" required>
                        
                            <small style="color: red;" id="errorDatefilter"></small>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-sm-12 text-right">
                    <button class="btn btn-primary" id="addSubBranch" type="submit">Create</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
          @endif
          <div class="col-md-12">
            <div class="card">
              <div class="card-header d-flex justify-content-between">
                <h5 class="card-title mb-0">Subgroup List</h5>
                <div class="card-options">
                  <select class="form-control form-control-sm btn-square" name="" id="branch">
                    <option value="all">All Branch</option>
                    @foreach ($branches as $branch)
                  <option value="{{$branch->id}}">{{$branch->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="table-responsive add-project">
                <table class="table card-table table-vcenter text-nowrap">
                  <thead>
                    <tr>
                      <th>Public Name</th>
                      <th>Experience Point</th>
                      <th>Date Range (From - To)</th>
                      @if (auth()->user()->isMerchant())
                      <th>Status</th>
                      @endif
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody id="subbranch-list">
                  @if (count($subBranches) <= 0)
                    <tr>
                      <td>There's no subbranch!</td>
                    </tr>
                  @else
                  @foreach($subBranches as $sub)
                    <tr>
                      <td>{{$sub->public_name}}</td>
                      <td><a class="text-inherit" href="#">{{$sub->branch->name}}</a></td>
                      <td>
                         {{date('d/m/Y', strtotime($sub->start_date))}} - {{date('d/m/Y', strtotime($sub->end_date))}}
                      </td>

                      @if (auth()->user()->isMerchant())    
                      <td>
                        @if ($sub->status == 1)
                        <span class="badge badge-success badge-pill">Active</span>
                        @else
                        <span class="badge badge-primary badge-pill">Unactive</span>
                        @endif
                      </td>
                      @endif
                    
                      <td>
                        <a href="#">
                          <span class="badge badge-warning badge-pill" id="editSub{{$sub->id}}" onclick="editSub('{{$sub->id}}')">Edit</span>
                          </a>
                          @if (auth()->user()->isAdmin())   
                          <a href="#" disabled>
                          <span class="badge badge-danger badge-pill" id="deleteSub{{$sub->id}}" onclick="deleteSub('{{$sub->id}}')">Delete</span>
                          </a>

                          @if ($sub->status == 1)
                          <a href="#">
                          <span class="badge badge-primary badge-pill" onclick="cahangeStatus('{{$sub->id}}')">Deactivate</span>
                          </a>
                          @else 
                          <a href="#">
                          <span class="badge badge-success badge-pill" onclick="cahangeStatus('{{$sub->id}}')">Activate</span>
                          </a>
                          @endif
                          @endif
                      </td>
                    </tr>
                  @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="edit-profile" id="edit">
      
      </div>

    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('js')

<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
let editSub = (id) => {
  let token = '{{csrf_token()}}';
  $.ajax({
    url:'{{route('editSubBranch')}}',
    type: 'POST',
    async: false,
    headers: {
      'X-CSRF-Token': token 
      },
    data: {
      id
    },
    success: function (responese) {
        $('#edit').html(responese)
        $('#add').hide()
    },
    }); 
}
let deleteSub = (id) =>{
	let token = '{{csrf_token()}}';
  swal({
        title: "Are you sure?",
        text: "This subgroup will be deleted from your subgroup list.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
          url:'{{route('deleteSubBranch')}}',
          type: 'POST',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Subgroup Successfully Deleted',
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          },
          error: function (responese) {
            swal({
                position: 'top-end',
                icon: 'error',
                title: responese.responseJSON.msg,
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          }
          }); 
        } else {
            swal("Your subgroup is safe!");
        }
    })
}

let cahangeStatus = (id) =>{
	let token = '{{csrf_token()}}';
  swal({
        title: "Change Status",
        text: "This subgroup will be change status.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
          url:'{{route('changeSubbranchStatus')}}',
          type: 'GET',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Status Successfully Changed.',
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          },
          error: function (responese) {
            swal("There is an error occoured!");
          }
          }); 
        } else {
            swal("Your subgroup is safe!");
        }
    })
}

$('#addSubBranch').click(function(event){
	event.preventDefault();
	let name = $('#subName').val();
	let public_name=  $('#subPublicName').val();
  let branch = $('#subBranch').val();
  let datefilter = $('#datefilter').val();
  let days = [];
  // $('.days').each(function(idx, el){
  //   if($(el).is(':checked'))
  //   { 
  //       days.push($(el).val());
  //   }
  // });
	let token = '{{csrf_token()}}';
	$.ajax({
  url:'{{route('saveSubBranch')}}',
  type: 'POST',
  async: false,
  headers: {
    'X-CSRF-Token': token 
    },
  data: {
    name,
    public_name,
    branch,
    datefilter
  },
  success: function (responese) {
    swal({
        position: 'top-end',
        icon: 'success',
        title: 'New Subgroup Successfully Updated',
        showConfirmButton: false,
    }) .then((willDelete) => {
            if (willDelete) {
                location.reload();
            }
        })
  },
  error:function(response){
    if(response.responseJSON.errors){
      if(response.responseJSON.errors.name){
        $('#errorName').text('Name is Required');
        $('#subName').css('border-color','red');
      } else{
        $('#errorName').text('');
        $('#subName').css('border-color','black');
      }
      if(response.responseJSON.errors.public_name){
        $('#errorPublicName').text('Public Name is Required');
        $('#subPublicName').css('border-color','red');
      } else{
        $('#errorPublicName').text('');
        $('#subPublicName').css('border-color','black');
      }
      if(response.responseJSON.errors.branch){
        $('#errorBranch').text('Branch is Required');
        $('#subBranch').css('border-color','red');
      } else{
        $('#errorBranch').text('');
        $('#subBranch').css('border-color','black');
      }
      if (response.responseJSON.errors.datefilter) {
          $('#errorDatefilter').text(response.responseJSON.errors.datefilter);
          $('#datefilter').css('border-color', 'red');
      }
      else {
          $('#errorDatefilter').text('');
          $('#datefilter').css('border-color', 'black');
      }

    }
  }
}); 
});
@if (count($subBranches) > 0)
  $("#branch").on('change', function(){

    var branchId = $(this).val();


    $.ajax({
    url:'{{route('filterSubBranch')}}',
    type: 'GET',
    async: false,
    data: {
      branchId
    },
    success: function (responese) {
      $("#subbranch-list").html('');
      var result = '';
      $.each(responese, function(index, value){
        @if(auth()->user()->isAdmin())

          let status = '';
          
          if(value.status = 1){
            status = '<a href="#"><span class="badge badge-primary badge-pill" onclick="cahangeStatus('+ value.id +')">Deactivate</span></a>';
          } else {
            status = '<a href="#"><span class="badge badge-success badge-pill" onclick="cahangeStatus('+ value.id +')">Activate</span></a>';
          }

          result += '<tr><td>'+ value.name +'</td><td><a class="text-inherit" href="#">'+ value.branch +'</a></td><td>'+ value.date +'</td><td><a href="#"><span class="badge badge-warning badge-pill" id="editSub{{$sub->id}}" onclick="editSub('+ value.id +')">Edit</span></a><a href="#"><span class="badge badge-danger badge-pill" id="deleteSub{{$sub->id}}" onclick="deleteSub('+ value.id +')">Delete</span></a>' + status + '</td></tr>';

        @else
        if(value.status != 1){
          status = '<span class="badge badge-success badge-pill">Active</span>';
        } else {
          status = '<span class="badge badge-primary badge-pill">Unactive</span>';
        }
        result += '<tr><td>'+ value.name +'</td><td><a class="text-inherit" href="#">'+ value.branch +'</a></td><td>'+ value.date +'</td><td>' + status + '</td><td><a href="#"><span class="badge badge-warning badge-pill" id="editSub{{$sub->id}}" onclick="editSub('+ value.id +')">Edit</span></a>';
        @endif

      });
      $("#subbranch-list").html(result);
    },
  });

  });
@endif
</script>
@endsection