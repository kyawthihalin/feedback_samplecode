@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid pt-5">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card">
                  <div class="card-body">
                    <div class="text-center">
                      <div>
                        <h5>
                          @if (isset($lastQuestion))
                            {{$lastQuestion->question_text}}
                          @else
                            {{'No question'}}
                          @endif
                        </h5>
                        <p>{{ $currentDate }}</p>
                      </div>
                    </div>
                    <div class="d-flex justify-content-center pt-4">
                      <div style="margin-top: -50px;">
                        <div id="chart-widget5" class="img-fluid"> </div>
                      </div>
                      <div class="d-flex justify-content-center pt-5">
                        <div class="p-2">
                          <img src="../assets/images/smile/s1.png" class="img-fluid pb-2">
                          <h6><span class="counter">{{$percentExcellent}}</span>%</h6>
                        </div>
                        <div class="p-2">
                          <img src="../assets/images/smile/s2.png" class="img-fluid pb-2">
                          <h6><span class="counter">{{$percentGood}}</span>%</h6>
                        </div>
                        <div class="p-2">
                          <img src="../assets/images/smile/s3.png" class="img-fluid pb-2">
                          <h6><span class="counter">{{$percentPoor}}</span>%</h6>
                        </div>
                        <div class="p-2">
                          <img src="../assets/images/smile/s4.png" class="img-fluid pb-2">
                          <h6><span class="counter">{{$percentBad}}</span>%</h6>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div>
                        <span>
                           How was your self-checkout experience today?
                        </span>
                        <p>
                           <b>{{$totalFeedback}}</b> responses, <b>{{$positivePercent}}%</b> positive, <b>{{$negativePercent}}%</b> negative
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6 box-col-6">
              <div class="card">
                <div class="card-body chart-block">
                   <div class="d-flex justify-content-between pb-2">
                    <h5>Total Smiley</h5>
                   </div>
                  <div id="pie-editor"></div>
                  <div class="chart-overflow" id="pie-chart2"></div>
                </div>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="card"  style="overflow-y: scroll; height:400px;">
              <div class="card-body">
                <div class="pro-head">
                  <div class="d-flex justify-content-between pb-2">
                    <h5 class="pb-4">Compare the question</h5>
                  </div>
                </div>
                @if (count($comparisonArr) > 0)
                @foreach ($comparisonArr as $name => $comparison)    
                <div class="progress" style="height: 11px;">
                  <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['bad']) ? $comparison['bad'] : 0 }}%; background: #e94545;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
                  <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['poor']) ? $comparison['poor'] : 0 }}%; background:#f39c26;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                  <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['excellent']) ? $comparison['excellent'] : 0 }}%; background: #92c462;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
                  <div class="progress-bar" role="progressbar" style="width: {{ isset($comparison['good']) ? $comparison['good'] : 0 }}%; background:#1ca185;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <div class="progress-text pb-2">
                  <small>
                    {{$name}}
                      <span class="pull-right m-l-10 txt-primary">{{ $comparison['total'] ? $comparison['total'] : 0 }}</span>
                  </small>
                </div>
                @endforeach
                @else 
                No Data
                @endif
              </div>
            </div>
        </div>
          <div class="col-md-6 col-sm-12">
              <div class="card">
                <div class="card-body">
                  <div class="pro-head">
                    <div class="d-flex justify-content-between pb-2">
                      <h5 class="pb-4">Positive and negative feedback</h5>
                    </div>
                  </div>
                  <div class="progress" style="height: 11px;">
                    <div class="progress-bar" role="progressbar" style="width: {{ $positiveAnswerCount }}%; background:#1ca185;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div class="progress-text pb-2">
                    <small>
                      Positive
                        <span class="pull-right m-l-10 txt-primary">{{ $positiveAnswerCount }}</span>
                    </small>
                  </div>
                  <div class="progress" style="height: 11px;">
                    <div class="progress-bar" role="progressbar" style="width: {{ $negativeAnswerCount }}%; background:#f39c26;" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <div class="progress-text pb-2">
                    <small>
                      Negative
                        <span class="pull-right m-l-10 txt-primary">{{ $negativeAnswerCount }}</span>
                    </small>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </div>
</div>   
@endsection

@section('js')
<script src="{{asset('assets/js/chart/charttt/moment.min.js')}}"></script>
<script src="{{asset('assets/js/chart/charttt/apex-chart.js')}}"></script>
{{-- <script src="{{asset('assets/js/chart/charttt/stock-prices.js')}}"></script> --}}
<script src="{{asset('assets/js/chart/morris-chart/morris.js')}}"> </script>
<script src="{{asset("assets/js/chart/morris-chart/raphael.js")}}"></script>
<script src="{{asset("assets/js/chart/morris-chart/prettify.min.js")}}"></script>
<script src="{{asset('assets/js/prism/prism.min.js')}}"></script>
<script src="{{asset('assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{asset('assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/js/counter/counter-custom.js')}}"></script>
<script src="{{asset('assets/js/custom-card/custom-card.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="{{asset('assets/js/chart/google/google-chart-loader.js')}}"></script>
<script src="{{asset('assets/js/chart/google/google-chart.js')}}"></script>

<script>

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', {'packages':['line']});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawBasic);

var response = {{ json_encode($totalFeedback)}};

function drawBasic() {

  if ($("#pie-chart2").length > 0) {
        var data = google.visualization.arrayToDataTable([
          ['Response', response.toString()],
          ['Excellent',  {{json_encode($percentExcellent)}}],
          ['Good',  {{json_encode($percentGood)}}],
          ['Poor',  {{json_encode($percentPoor)}}],
          ['Bad', {{json_encode($percentBad)}}]
        ]);
        var options = {
          title: 'Responses: ' + response.toString(),
          is3D: true,
          width:'100%',
          height: 300,
          colors: ["#1ca185" , "#92c462" , "#f39c26", "#e94545"]
        };
        var chart = new google.visualization.PieChart(document.getElementById('pie-chart2'));
        chart.draw(data, options);
    }

    // $('#full-page-pdf').click(function(){
    //   var img = chart.getImageURI();
    //     // console.log(img);
    //     var doc = new jsPDF('p', 'mm', 'a4');
    //     var width = doc.internal.pageSize.width;
    //     var height = doc.internal.pageSize.height;
    //     // doc.addImage(img, 0, 0);
    //     chart3.dataURI().then(({ imgURI, blob }) => {
    //         doc.addImage(imgURI, 'PNG', 10, 20, parseInt(width) - 20, 100);
    //     })
    //     // doc.addImage(img, 'PNG', 10, 20, parseInt(width) - 20, 100);
    //     doc.save("download.pdf");
    // });

    $('#pie-pdf').click(function(){
      var img = chart.getImageURI();
      var doc = new jsPDF('p', 'mm', 'a4');
      var width = doc.internal.pageSize.width;
      var height = doc.internal.pageSize.height;
      doc.setFontSize(13);
      doc.text(currentFilter, 10, 20, {maxWidth: parseInt(width) - 10,});
      doc.line(10, 22, parseInt(width) - 10, 22);
      doc.text('Pie Chart', 10, 40);
      doc.line(10, 42, 50, 42);
      doc.addImage(img, 'PNG', 10, 60, parseInt(width) - 20, 100);
      doc.save("AnalysisPieChartReport.pdf");
  });

    
}

    (function($) {
        var optionsearningchart = {
        chart: {
            height: 360,
            type: 'radialBar',
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 135,
                dataLabels: {
                    name: {
                        fontSize: '16px',
                        color: '#000000',
                        offsetY: 120
                    },
                    value: {
                        offsetY: 76,
                        fontSize: '22px',
                        color: '#000000',
                        formatter: function (val) {
                            return val + "";
                        }
                    }
                }
            }
        },

        fill: {
            opacity: 1
        },
        colors:['#1ca185'],

        stroke: {
            dashArray: 4
        },
        series: [{{$totalSmiley}}],
        labels: ['Happy Index'],

        }

        var chartearningchart = new ApexCharts(
            document.querySelector("#chart-widget5"),
            optionsearningchart
        );
        chartearningchart.render();
    })(jQuery);
</script>

@endsection