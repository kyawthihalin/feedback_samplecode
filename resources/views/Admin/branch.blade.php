@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chartist.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/date-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/timepicker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')
<!-- Container-fluid starts-->
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Manage Point</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Point</li>
                        <li class="breadcrumb-item active">Experience Point</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
            @if(auth()->user()->isAdmin())
            <div class="edit-profile" id="add">
              <div class="row">
                <div class="col-md-12">
                  <form class="card theme-form">
                    <div class="card-header">
                      <h5 class="card-title mb-0">Add New Experience Point</h5>
                      <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="datetime-picker">
                            <form class="theme-form">
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Experience Point</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="name" type="text" placeholder="Experience Point">
                                  <small style="color: red;" id="errorName"></small>
                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Phone</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="phone" type="text" placeholder="Phone Number">
                                  <small style="color: red;" id="errorPhone"></small>

                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="email" type="text" placeholder="Email">
                                  <small style="color: red;" id="erroremail"></small>

                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Merchants</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <select class="form-control btn-square" id="userId">
                                  <option value="">Select Merchant Name</option>
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                  </select>
                                  <small style="color: red;" id="errorUser"></small>
                                </div>
                              </div>
                              
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">From</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8 clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                  <input class="form-control" type="text" id="from" value="09:00">
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                  <small style="color: red;" id="errorFrom"></small>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">To</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8 clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                  <input class="form-control" type="text" id="to" value="17:00">
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                  <small style="color: red;" id="errorTo"></small>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Active Weekdays</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <div class="m-checkbox-inline">
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-1" class="days" value="Monday" name="day" type="checkbox">
                                      <label for="inline-1">Mon</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-2" class="days" value="Tue" name="day" type="checkbox">
                                      <label for="inline-2">Tue</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-3" class="days" value="Wed" name="day" type="checkbox">
                                      <label for="inline-3"> Wed</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-4" class="days" value="Thu" name="day" type="checkbox">
                                      <label for="inline-4">Thu</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-5" class="days"  value="Fri" name="day" type="checkbox">
                                      <label for="inline-5"> Fri</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-6" class="days" value="Sat" name="day" type="checkbox">
                                      <label for="inline-6"> Sat</label>
                                    </div>
                                    <div class="checkbox checkbox-dark">
                                      <input id="inline-7"  class="days" value="Sun" name="day" type="checkbox">
                                      <label for="inline-7">Sun</label>
                                    </div>
                                  </div>
                                  <small style="color: red;" id="errorDay"></small>

                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                        <div class="col-sm-12 text-right">
                          <button class="btn btn-primary" id="addSubBranch" type="submit">Create</button>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            @endif
            <div class="edit-profile" id="edit">
            
            </div>

            <div class="edit-profile" id="detail">

            </div>
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title mb-0">Experience point List</h5>
                  <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                </div>
                <div class="card-body">
                  {{-- <table class="table card-table table-vcenter text-nowrap">
                    <thead> 
                      <tr>
                        <th>Public Name</th>
                        <th>Merchant Name</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    @if (count($branches) <= 0)
                        @if (auth()->user()->isAdmin())
                            <tr>
                              <td>
                                There's no branch.
                              </td>
                            </tr>
                        @else
                            <tr>
                              <td>
                                Your branch not yet create. Please contact admin to create your branch.
                              </td>
                            </tr>
                        @endif
                    @else
                    @foreach($branches as $branch)
                    <!-- @php
                      $activeDays = json_decode($branch->active_weekDay,true);
                    @endphp -->
                      <tr>
                        <td>{{$branch->name}}</td>
                        <td>
                          {{$branch->user->name}}
                        </td>
                      
                        <td>
                          <a href="#">
                          <span class="badge badge-warning badge-pill" id="editSub{{$branch->id}}" onclick="editSub('{{$branch->id}}')">Edit</span>
                          </a>
                          @if(auth()->user()->isAdmin())
                          <a href="#">
                          <span class="badge badge-danger badge-pill" id="deleteSub{{$branch->id}}" onclick="deleteSub('{{$branch->id}}')">Delete</span>
                          </a>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                    @endif
                    </tbody>
                  </table> --}}
                  <table id="branch" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                          <th>Experience Point</th>
                          <th>Merchant Name</th>
                          <th>Actions</th>
                        </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>

          </div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('js')

{{-- <script src="{{asset('assets/js/datepicker/date-time-picker/datetimepicker.custom.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-time-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-time-picker/tempusdominus-bootstrap-4.min.js')}}"></script> --}}
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/time-picker/jquery-clockpicker.min.js')}}"></script>
<script src="{{asset('assets/js/time-picker/highlight.min.js')}}"></script>
<script src="{{asset('assets/js/time-picker/clockpicker.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script>

$(document).ready(function() {
  var branchTable = $('#branch').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "branch/get-all",
      columns: [
          {
              data: 'name',
              name: 'name'
          },
          {
              data: 'user',
              name: 'user'
          },
          {
              data: 'action',
              name: 'action'
          }
      ]
  });
});

let editSub = (id) => {
  let token = '{{csrf_token()}}';
  $.ajax({
    url:'{{route('editBranch')}}',
    type: 'POST',
    async: false,
    headers: {
      'X-CSRF-Token': token 
      },
    data: {
      id
    },
    success: function (responese) {
        $('#edit').html(responese)
        $('#add').hide()
        $('#detail').html('')
    },
    }); 
}

let detail = (id) => {
  let token = '{{csrf_token()}}';
  $.ajax({
    url:'{{route('detailBranch')}}',
    type: 'POST',
    async: false,
    headers: {
      'X-CSRF-Token': token 
      },
    data: {
      id
    },
    success: function (responese) {
        $('#detail').html(responese)
        $('#add').hide()
        $('#edit').html('')
    },
    }); 
}
let deleteSub = (id) =>{
	let token = '{{csrf_token()}}';
  swal({
        title: "Are you sure?",
        text: "This points will be deleted from your subgroup list.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
          url:'{{route('deleteBranch')}}',
          type: 'POST',
          async: false,
          headers: {
            'X-CSRF-Token': token 
            },
          data: {
            id
          },
          success: function (responese) {
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Experience points Successfully Deleted',
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          },
          error: function (responese) {
            swal({
                position: 'top-end',
                icon: 'error',
                title: responese.responseJSON.msg,
                showConfirmButton: false,
            }) .then((willDelete) => {
                    if (willDelete) {
                        location.reload();
                    }
            })
          }
          }); 
        } else {
            swal("Your experience points is safe!");
        }
    })
}
$('#addSubBranch').click(function(event){
	event.preventDefault();
	let branchName = $('#name').val();
	let phone=  $('#phone').val();
	let email=  $('#email').val();
	let userId=  $('#userId').val();
	let from=  $('#from').val();
	let to=  $('#to').val();
  let days = [];
  $('.days').each(function(idx, el){
    if($(el).is(':checked'))
    { 
        days.push($(el).val());
    }
  });
	let token = '{{csrf_token()}}';
	$.ajax({
  url:'{{route('saveBranch')}}',
  type: 'POST',
  async: false,
  headers: {
    'X-CSRF-Token': token 
    },
  data: {
    branchName,
    phone,
    email,
    days,
    from,
    to,
    userId
  },
  success: function (responese) {
    swal({
        position: 'top-end',
        icon: 'success',
        title: 'New Experience Points Successfully Created.',
        showConfirmButton: false,
    }) .then((willDelete) => {
            if (willDelete) {
                location.reload();
            }
        })
  },
  error:function(response){
    if(response.responseJSON.errors){
      if(response.responseJSON.errors.branchName){
        $('#errorName').text('Experience points is Required');
        $('#name').css('border-color','red');
      } else{
        $('#errorName').text('');
        $('#name').css('border-color','black');
      }
      if(response.responseJSON.errors.phone){
        $('#errorPhone').text('Phone Number is Required');
        $('#phone').css('border-color','red');
      } else{
        $('#errorPhone').text('');
        $('#phone').css('border-color','black');
      }
      if(response.responseJSON.errors.email){
        $('#erroremail').text('Public Name is Required');
        $('#email').css('border-color','red');
      } else{
        $('#erroremail').text('');
        $('#email').css('border-color','black');
      }
      if(response.responseJSON.errors.userId){
        $('#errorUser').text('Merchant Name is Required');
        $('#userId').css('border-color','red');
      } else{
        $('#errorUser').text('');
        $('#userId').css('border-color','black');
      }
      if(response.responseJSON.errors.from){
        $('#errorFrom').text('Open Time is Required');
        $('#from').css('border-color','red');
      } else{
        $('#errorFrom').text('');
        $('#from').css('border-color','black');
      }
      if(response.responseJSON.errors.to){
        $('#errorTo').text('Close Time is Required');
        $('#to').css('border-color','red');
      } else{
        $('#errorTo').text('');
        $('#to').css('border-color','black');
      }
      if(response.responseJSON.errors.days){
        $('#errorDay').text('Day is Required');
      } else{
        $('#errorDay').text('');
      }

    }
  }
}); 
});

</script>
@endsection