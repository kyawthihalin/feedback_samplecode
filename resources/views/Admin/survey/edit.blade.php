@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/todo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Edit Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item">Edit survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="f1" method="post">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon"><i class="fa fa-question"></i></div>
                                    <p>Question</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-question-circle"></i></div>
                                    <p>Follow Up</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                    <p>Date</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-location-arrow"></i></div>
                                    <p>Location</p>
                                </div>
                            </div>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Create Survey</h5>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group m-t-15 custom-radio-ml">
                                            <div class="radio radio-primary">
                                                <input type="radio" id="radio1" name="radioGroup" value="Please rate our service today ?" data-language="1">
                                                <label for="radio1"> Please rate our service today</label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <input type="radio" id="radio2" name="radioGroup" value="How was our service today ?" data-language="1">
                                                <label for="radio2">Please rate your overall experience</label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <input type="radio" id="radioNew" name="radioGroup">
                                                <label for="radioNew">Customized your own question ( Create your own question)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 pt-4" id="createQuestion">
                                        <h5 class="pb-2">Customized your own question </h5>
                                        <div>
                                            <div>Question in main language</div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <select class="custom-select lang" id="validationCustom04" required>
                                                        <option selected disabled value="">Choose primary language</option>
                                                        @foreach($languages as $language)
                                                        <option value="{{$language->id}}" {{$question->language_id != $language->id ?: 'selected="selected"'}}>{{$language->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <input class="form-control" id="primaryQuestion"  placeholder="">
                                            </div>
                                        </div>
                                        <div>
                                            <div>Same question in other languages</div>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <select class="custom-select other_lang" id="validationCustom04" required>
                                                        <option selected disabled value=""> Choose second language </option>
                                                        @foreach($languages as $language)
                                                        <option value="{{$language->id}}" {{$question->other_language_id != $language->id ?: 'selected="selected"'}}>{{$language->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <input class="form-control" id="secondaryQuestion"  placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-next" id="step2" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12 pt-3">
                                        <h5>Follow-up options</h5>
                                    </div>
                                    <div class="col-sm-12 default-according" id="accordion">
                                        <div class="form-group m-t-15 custom-radio-ml">
                                            <div class="radio radio-primary" id="headingOne">
                                                <input id="radio3" type="radio" name="radio1" value="option1">
                                                <label for="radio3" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">After all feedback</label>
                                            </div>
                                            <div class="radio radio-primary" id="headingTwo">
                                                <input id="radio4" type="radio" name="radio1" value="option4">
                                                <label for="radio4" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">After negative feedback</label>
                                            </div>
                                            <div class="radio radio-primary">
                                                <input id="radio5" type="radio" name="radio1" value="option5"  {{count($question->preAnswer) > 0 ?: 'checked="checked"'}}>
                                                <label for="radio5" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">Don't Show</label>
                                            </div>
                                        </div>
                                        <div class="collapse" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group form-check pt-3">
                                                        <input type="checkbox" name="separate" class="form-check-input" id="separate">
                                                        <label class="form-check-label" for="separate">Separate Positive and Negative Feedbacks</label>
                                                    </div>
                                                    <div class="h6 pb-3" id="alltext">
                                                        For all Feedback
                                                    </div>
                                                    <div class="todo" id="all">
                                                        <div class="todo-list-wrapper">
                                                            <div class="todo-list-container">
                                                                <div class="todo-list-body">
                                                                    <ul id="todo-list">
                                                                        <li class="task">
                                                                            <div class="task-container">
                                                                                <div class="form-group task-label">
                                                                                <select class="form-control digits preanswerAll" id="preanswer5" requ requiredired>
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerAll mt-4" id="preanswer6" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select> 
                                                                                    <select class="form-control digits preanswerAll mt-4" id="preanswer7" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerAll mt-4" id="preanswer8" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                 </div>
                                                                            </div>
                                                                        </li>                                                            
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notification-popup hide">
                                                            <p><span class="task"></span><span class="notification-text"></span></p>
                                                        </div>
                                                    </div>  
                                                    <!-- .................... -->
                                                    <div class="h6 pb-3" id="positivetext">
                                                        For Positive Feedback
                                                    </div>
                                                    <div class="todo" id="separatePositive">
                                                        <div class="todo-list-wrapper">
                                                            <div class="todo-list-container">
                                                                <div class="todo-list-body">
                                                                    <ul id="todo-list">
                                                                        <li class="task">
                                                                            <div class="task-container">
                                                                                <div class="form-group task-label">
                                                                                <select class="form-control digits preanswerSp" id="preanswer9" requ requiredired>
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerSp mt-4" id="preanswer10" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select> 
                                                                                    <select class="form-control digits preanswerSp mt-4" id="preanswer11" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerSp mt-4" id="preanswer12" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                 </div>
                                                                            </div>
                                                                        </li>                                                            
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notification-popup hide">
                                                            <p><span class="task"></span><span class="notification-text"></span></p>
                                                        </div>
                                                    </div>    
                                                    <div class="h6 py-3" id="negativetext">
                                                        For Negative Feedback
                                                    </div>
                                                    <div class="todo" id="separateNegative">
                                                        <div class="todo-list-wrapper">
                                                            <div class="todo-list-container">
                                                                <div class="todo-list-body">
                                                                    <ul id="todo-list">
                                                                        <li class="task">
                                                                            <div class="task-container">
                                                                                <div class="form-group task-label">
                                                                                <select class="form-control digits preanswerNg" id="preanswer13" requ requiredired>
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerNg mt-4" id="preanswer14" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select> 
                                                                                    <select class="form-control digits preanswerNg mt-4" id="preanswer15" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswerNg mt-4" id="preanswer16" required>
                                                                                        <option value="">--Select--</option>

                                                                                    </select>
                                                                                 </div>
                                                                            </div>
                                                                        </li>                                                            
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notification-popup hide">
                                                            <p><span class="task"></span><span class="notification-text"></span></p>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="h6 py-3">
                                                        For Negative Feedback
                                                    </div>
                                                    <div class="todo">
                                                        <div class="todo-list-wrapper">
                                                            <div class="todo-list-container">
                                                                <div class="todo-list-body">
                                                                    <ul id="todo-list">
                                                                        <li class="task">
                                                                            <div class="task-container">
                                                                                <h4 class="form-group task-label">
                                                                                    <select class="form-control digits preanswer" id="preanswer1">
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswer mt-4" id="preanswer2">
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswer mt-4" id="preanswer3">
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                                    <select class="form-control digits preanswer mt-4" id="preanswer4">
                                                                                    <option value="">--Select--</option>

                                                                                    </select>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>                                                               
                                                            </div>
                                                        </div>
                                                        <div class="notification-popup hide">
                                                            <p><span class="task"></span><span class="notification-text"></span></p>
                                                        </div>
                                                    </div>
                                                    <!-- HTML Template for tasks-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collapse show" id="collapseThree" aria-labelledby="headingThree" data-parent="#accordion">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-next step3" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div>
                                    <h6>Time Period</h6>
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <input class="form-control digits dataRange" type="text" name="datefilter" value="" placeholder="Input Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-next step4" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="text-right">
                                    <input type="checkbox" name="select-all" id="all-sub">
                                    <label class="h6" for="all-sub">Select All</label>
                                </div>
                                @foreach($branches as $branch)
                                @php
                                    $sub_branches = \App\SubBranch::all()->where('branch_id',$branch->id);
                                @endphp 
                                <div class="media">
                                    <label class="p-t-50 p-b-50 h5">{{$branch->name}}</label>
                                </div>
                                <div class="timeline-small" id="b{{$branch->id}}">
                                    <div class="media pb-3">
                                        <div class="timeline-round m-r-30 timeline-line-1 bg-primary"><i data-feather="home"></i></div>
                                        
                                        <div class="media-body">
                                            @foreach($sub_branches as $sub)
                                            <div class="form-group form-check">
                                            <?php $checked = ''; 
                                              foreach($question->subBranch as $subBranch){
                                                if($subBranch->id == $sub->id){
                                                    $checked = 'checked'; 
                                                }
                                              }
                                              
                                            ?>
                                            <input type="checkbox" name="subbranch" {{$checked}} onclick="storeSubBranch({{$sub->id}})" value="{{$sub->id}}" class="form-check-input" id="sub{{$sub->id}}">
                                                <label class="form-check-label" for="sub{{$sub->id}}">{{$sub->name}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-submit" id="saveQuestion" type="submit">Submit</button>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@php
    
@endphp
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/form-wizard/form-wizard-three.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/todo/todo.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>

<script>
    var question1 = 'Please rate our service today ?';
    var question2 = 'How was our service today ?';
    var secondary='';
    var lang = '';
    var other_lang = '';
    var dateFromTo = '';
    var preanswersForAll = [];
    var preanswersForNegative = [];
    var preanswerForAll={};
    var preanswerForNegative={};
    var separateNegative=[];
    var separatePositive=[];
    var preanswerForSp={};
    var preanswerForSpPo={};
    function clickedSeperate(){
        if($('#separate').is(':checked')){
            $('#all').hide();   
            $('#alltext').hide(); 
            $('#separatePositive').show();
            $('#separateNegative').show();
            $('#negativetext').show();
            $('#positivetext').show();
        }else{
            $('#all').show();    
            $('#alltext').show(); 
            $('#separatePositive').hide();
            $('#separateNegative').hide();
            $('#negativetext').hide();
            $('#positivetext').hide();
        }
    }

    function getSelectedBranches(){
        var subBranches = [];

        var subList = document.getElementsByName("subbranch");
        for (var i = 0; i < subList.length; i++) {
            if (subList[i].checked) {
                subBranches.push(subList[i].value);
            } else {
                subBranches = subBranches.filter(function(subBranch) {
                    return subBranch != subList[i].value;
                });
            }
        }

        return subBranches;
    }
    $(document).ready(function() {
        $('#separatePositive').hide();
        $('#separateNegative').hide();
        $('#negativetext').hide();
        $('#positivetext').hide();
        $('#separate').click(function(){
            clickedSeperate();
        })
        $('#createQuestion').hide();
        
        var question = @json($question);
        var question_text = '<?php echo $question->question_text ;?>';
        var question_text_other = '<?php echo $question->question_text_other_language ;?>';

        var fromDate = new Date(question.from);
        var toDate = new Date(question.to);

        $('.dataRange').val(fromDate.toLocaleDateString("en-US") + ' - ' + toDate.toLocaleDateString("en-US"));

        if(question_text == question1){
            $("#radio1").attr("checked", true);
        }
        else if(question_text == question2){
            $("#radio2").attr("checked", true);
        }
        else{
            $("#radioNew").attr("checked", true);
            $('#createQuestion').show();

            $("#primaryQuestion").val(question_text);

            if(question_text_other != "secondaryQuestion"){
                $("#secondaryQuestion").val(question_text_other);
            }
        }

        var feedbackType = '<?php echo $feedbackType ;?>';
        
        var preAnswers = @json($question->preAnswer);

        if(feedbackType == 'all'){
            $("#radio3").attr('checked', true);
            $("#collapseOne").addClass('show');

            let token = '{{csrf_token()}}';

            $.ajax({
                url:'{{route("getPreAnswers")}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    console.log(preAnswers);
                    var length = preAnswers.length;
                    var startId = 5;
                    var index = 0;
                    while(length>0){
                        $("#preanswer" + startId).html(response);
                        $("#preanswer" + startId).val(preAnswers[index].pre_answer_id);
                        startId++;
                        length--;
                        index++;
                    }
                },
            }); 
        }
        else if(feedbackType == 'seperate'){
            $("#radio3").attr('checked', true);
            $("#collapseOne").addClass('show');
            $("#separate").attr('checked', true);
            clickedSeperate();

            console.log(preAnswers);

            let token = '{{csrf_token()}}';

            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    var length = preAnswers.length;
                    var posStartId = 9;
                    var negStartId = 13;
                    var index = 0;
                    while(length>0){
                        if(preAnswers[index].feedback_type_id == 1){
                            $("#preanswer" + posStartId).html(response);
                            $("#preanswer" + posStartId).val(preAnswers[index].pre_answer_id);
                            posStartId++;
                        }
                        else if(preAnswers[index].feedback_type_id == 2){
                            $("#preanswer" + negStartId).html(response);
                            $("#preanswer" + negStartId).val(preAnswers[index].pre_answer_id);
                            negStartId++;
                        }
                        
                        length--;
                        index++;
                    }
                },
            }); 
        }
        else if(feedbackType == 'negative'){
            $("#radio4").attr('checked', true);
            $("#collapseTwo").addClass('show');

            let token = '{{csrf_token()}}';

            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    var length = preAnswers.length;
                    var startId = 1;
                    var index = 0;
                    while(length>0){
                        $("#preanswer" + startId).html(response);
                        $("#preanswer" + startId).val(preAnswers[index].pre_answer_id);
                        startId++;
                        length--;
                        index++;
                    }
                },
            }); 
        }

        $('#preanswer1').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer1').html(response)
                },
                }); 
        })
        $('#preanswer2').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer2').html(response)
                },
                }); 
        })
        $('#preanswer3').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer3').html(response)
                },
                }); 
        })
        $('#preanswer4').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer4').html(response)
                },
                }); 
        })
        $('#preanswer5').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer5').html(response)
                },
                }); 
        })
        $('#preanswer6').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer6').html(response)
                },
                }); 
        })
        $('#preanswer7').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer7').html(response)
                },
                }); 
        })
        $('#preanswer8').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer8').html(response)
                },
                }); 
        })
        //more 8
        $('#preanswer9').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer9').html(response)
                },
                }); 
        })
        $('#preanswer10').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer10').html(response)
                },
                }); 
        })
        $('#preanswer11').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer11').html(response)
                },
                }); 
        })
        $('#preanswer12').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer12').html(response)
                },
                }); 
        })
        $('#preanswer13').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer13').html(response)
                },
                }); 
        })
        $('#preanswer14').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer14').html(response)
                },
                }); 
        })
        $('#preanswer15').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer15').html(response)
                },
                }); 
        })
        $('#preanswer16').focus(function(){
            let token = '{{csrf_token()}}';
            $.ajax({
                url:'{{route('getPreAnswers')}}',
                type: 'GET',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                success: function (response) {
                    $('#preanswer16').html(response)
                },
                }); 
        })
        //end 8
        if($(document).on('click','.step3',function(){
           if($('#radio3').is(':checked')){
                preanswerForNegative = '';
                if($('#separate').is(':checked')){
                    preanswerForAll='';
                    $('.preanswerNg').each(function(){
                        separateNegative.push($(this).val());
                    })
                    $('.preanswerSp').each(function(){
                        separatePositive.push($(this).val());
                    })
                    $('.preanswerAll').each(function() {
                        $(this).val('');
                    });
                    $('.preanswer').each(function() {
                        $(this).val('');
                    });
                }else{
                    preanswerForSpPo=''
                    preanswerForSpNg=''
                    $('.preanswer').each(function() {
                        $(this).val('');
                    });
                    $('.preanswerNg').each(function(){
                        $(this).val('');
                    })
                    $('.preanswerSp').each(function(){
                        $(this).val('');
                    })
                    $('.preanswerAll').each(function() {
                        if($(this).val()!=''){
                            preanswersForAll.push($(this).val());
                        }
                    });
                }
              
           }
           if($('#radio4').is(':checked')){
                preanswersForAll = '' ;
                $('.preanswerAll').each(function() {
                    $(this).val('');
                });

                $('.preanswer').each(function() {
                    if($(this).val()!=''){
                        preanswersForNegative.push($(this).val());
                    }
                });
           }
           if($('#radio5').is(':checked')){
                preanswerForNegative = '';
                preanswersForAll = '' ;
               $('.preanswerAll').each(function() {
                   $(this).val('');
               });

               $('.preanswer').each(function() {
                    $(this).val('');
               });
          }
           preanswerForAll =Object.assign(preanswersForAll)
           preanswerForNegative =Object.assign(preanswersForNegative)
           preanswerForSpNg = Object.assign(separateNegative)
           preanswerForSpPo = Object.assign(separatePositive)

        }))
        
        $('#radioNew').on('click', function() {
            if ($("#radioNew").prop("checked")) {
                $('#createQuestion').show();
            } else {
                $('#createQuestion').hide();
            }
        })
        $('#radio1').click(function() {
            $('#createQuestion').hide();
        })
        $('#radio2').click(function() {
            $('#createQuestion').hide();
        })

        $('#step2').click(function(){
            if($('#radio1').is(':checked')){
                question1 = $('#radio1').val();
                secondary = '';
                lang = $('#radio1').data('language');
                other_lang = '';

            }
            else if($('#radio2').is(':checked')){
                question1 = $('#radio2').val();
                secondary='';
                lang = $('#radio2').data('language');
                other_lang = '';

            }
            else{
                question1 =$('#primaryQuestion').val();
                if($('#secondaryQuestion').val()!=''){
                    secondary = $('#secondaryQuestion').val();
                    other_lang = $('.other_lang').val();
                }
                else{
                    secondary = '';
                    other_lang = '';
                }
                lang = $('.lang').val();
            }

        })
        $('.step4').click(function(){
            dateFromTo = $('.dataRange').val();
        })
        
    })
    var subBranches = [];
    let storeSubBranch  = (id) =>{
        if($('#sub'+id).is(':checked')){
            subBranches.push(id)
        }else{
            subBranches = subBranches.filter(function(subBranch) {
                return subBranch != id;
            });
        }
    }
    $("input[name='select-all']").click(function(){
        $("input[name='subbranch']").prop('checked', $(this).prop('checked'));

        subBranches = getSelectedBranches();
    });

    $('#saveQuestion').click(function(){
        var subBranch = getSelectedBranches();
        if(subBranch.length <= 0){
        swal({
            position: 'top-end',
            icon: 'warning',
            title: 'Please Select Atleast One Subbranch.',
            showConfirmButton: false,
        });
        } else {
            

        if(question1 != ''){
            var primary = question1
            var language = lang
        }else{
            var primary = ''
            var language = ''
        }
        if(secondary != ''){
            var second = secondary
            var other_language = other_lang
        }else{
            var second = ''
            var other_language = ''
        }
        if(dateFromTo!=''){
            var dat = dateFromTo
        }else{
            var dat = ''
        }
        if(preanswerForAll != ''){
            var preAns = preanswerForAll
        }else{
            var preAns = ''
        }
        if(preanswerForSpNg!=''){
            var preanswerForSpNeg = preanswerForSpNg
        }else{
            var preanswerForSpNeg = ''

        }
        if(preanswerForSpPo!=''){
            var preanswerForSpPos = preanswerForSpPo
        }else{
            var preanswerForSpPos = ''

        }
        if(preanswerForNegative != ''){
            var preAnsNeg = preanswerForNegative
        }else{
            var preAnsNeg = ''
        }  
        var token = '{{csrf_token()}}';
        $.ajax({
            url:'{{route('updateQuestion', $question->id)}}',
            type: 'PATCH',
            async: false,
            headers: {
            'X-CSRF-Token': token 
            },
            data :{
                primary,
                language,
                second,
                other_lang,
                dat,
                preAns,
                preAnsNeg,
                subBranch,
                preanswerForSpNeg,
                preanswerForSpPos
            },
            success: function (response) {
                swal({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Survey Successfully Updated',
                    showConfirmButton: false,
                })
                location.reload();
            },
            }); 
        }

    })
</script>
@endsection
