@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Survey List</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Create Survey</li>
                        <li class="breadcrumb-item active">Survey List</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
<?php
$isHotel = auth()->user()->isHotel();
?>
    
    <div class="container-fluid">
        <div class="edit-profile" id="add">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Survey Period</th>
                                        @if(!$isHotel)
                                        <th>Locations</th>
                                        @endif
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Survey Name</th>
                                        <th>Survey Period</th>
                                        @if(!$isHotel)
                                        <th>Locations</th>
                                        @endif
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- edit modal -->

    
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        var isHotel = '<?php echo $isHotel; ?>';

        var tcolumns = [
            {
                data: 'question_text',
                name: 'question_text'
            },
            {
                data: 'survey_period',
                name: 'survey_period'
            },
        ];

        if(!isHotel){
            tcolumns.push(
                {
                    data : 'location',
                    name : 'location'
                }
            );
        }

        tcolumns.push(
            {
                data: 'action',
                name: 'action'
            }
        )

        var userTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "survey/getAll",
            columns: tcolumns
        });
        $(document).on('click', '#delete-btn', function() {
            var id = $(this).data('id');
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This Survey will be deleted from your Survey list.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : '{{url("admin/survey")}}' + '/' + id,
                            type: 'DELETE',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Survey Successfully Deleted',
                                    button: false,
                                })
                                userTable.ajax.reload()
                            },
                            error: function(error) {
                                swal({
                                    position: 'top-end',
                                    icon: 'warning',
                                    title: JSON.parse(error.responseText)[1],
                                    button: false,
                                })
                            }
                        });
                    } else {
                        swal("Survey is safe!");
                    }
                })
        })
    });
</script>
@endsection