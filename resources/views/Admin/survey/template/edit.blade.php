@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/todo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Edit Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item">Edit survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="f1" method="post">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                    <p>Date</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-location-arrow"></i></div>
                                    <p>Location</p>
                                </div>
                            </div>
                            <fieldset>
                                <div>
                                    <h6>Time Period</h6>
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <input class="form-control digits dataRange" type="text" name="datefilter" value="" placeholder="Input Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-next step4" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="text-right">
                                    <input type="checkbox" name="select-all" id="all-sub">
                                    <label class="h6" for="all-sub">Select All</label>
                                </div>
                                @foreach($branches as $branch)
                                @php
                                    $sub_branches = \App\SubBranch::all()->where('branch_id',$branch->id);
                                @endphp 
                                <div class="media">
                                    <label class="p-t-50 p-b-50 h5">{{$branch->name}}</label>
                                </div>
                                <div class="timeline-small" id="b{{$branch->id}}">
                                    <div class="media pb-3">
                                        <div class="timeline-round m-r-30 timeline-line-1 bg-primary"><i data-feather="home"></i></div>
                                        
                                        <div class="media-body">
                                            @foreach($sub_branches as $sub)
                                            <div class="form-group form-check">
                                            <?php $checked = ''; 
                                              foreach($question->subBranch as $subBranch){
                                                if($subBranch->id == $sub->id){
                                                    $checked = 'checked'; 
                                                }
                                              }
                                              
                                            ?>
                                            <input type="checkbox" name="subbranch" {{$checked}} onclick="storeSubBranch({{$sub->id}})" value="{{$sub->id}}" class="form-check-input" id="sub{{$sub->id}}">
                                                <label class="form-check-label" for="sub{{$sub->id}}">{{$sub->name}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-submit" id="saveQuestion" type="button">Submit</button>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@php
    
@endphp
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/form-wizard/form-wizard-three.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/todo/todo.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>

<script>
    var dateFromTo = '';

    function getSelectedBranches(){
        var subBranches = [];

        var subList = document.getElementsByName("subbranch");
        for (var i = 0; i < subList.length; i++) {
            if (subList[i].checked) {
                subBranches.push(subList[i].value);
            } else {
                subBranches = subBranches.filter(function(subBranch) {
                    return subBranch != subList[i].value;
                });
            }
        }

        return subBranches;
    }

    $(document).ready(function() {
        
        var question = @json($question);

        var fromDate = new Date(question.from);
        var toDate = new Date(question.to);

        $('.dataRange').val(fromDate.toLocaleDateString("en-US") + ' - ' + toDate.toLocaleDateString("en-US"));

        $('.step4').click(function(){
            dateFromTo = $('.dataRange').val();
        })
        
    })
    var subBranches = [];
    let storeSubBranch  = (id) =>{
        if($('#sub'+id).is(':checked')){
            subBranches.push(id)
        }else{
            subBranches = subBranches.filter(function(subBranch) {
                return subBranch != id;
            });
        }
    }
    $("input[name='select-all']").click(function(){
        $("input[name='subbranch']").prop('checked', $(this).prop('checked'));

        subBranches = getSelectedBranches();
    });

    $('#saveQuestion').click(function(){
        var subBranch = getSelectedBranches();
        if(subBranch.length <= 0){
        swal({
            position: 'top-end',
            icon: 'warning',
            title: 'Please Select Atleast One Subbranch.',
            showConfirmButton: false,
        });
        } else {
        if(dateFromTo!=''){
            var dat = dateFromTo
        }else{
            var dat = ''
        }
        var token = '{{csrf_token()}}';
        $.ajax({
            url:'{{route("updateQuestion", $question->id)}}',
            type: 'PATCH',
            async: false,
            headers: {
            'X-CSRF-Token': token 
            },
            data :{
                dat,
                subBranch,
            },
            success: function (response) {
                swal({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Survey Successfully Updated',
                    showConfirmButton: false,
                })
                location.reload();
            },
            }); 
        }

    })
</script>
@endsection
