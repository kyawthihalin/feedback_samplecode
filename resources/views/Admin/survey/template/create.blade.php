@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/todo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/scrollable.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>


<style>

.scroll-demo {
    padding: 10px;
    position: relative;
    border: 0px solid #f4f4f4 !important;
    overflow: auto;
    height: 380px !important;
}
.app-border{
    border: 1px solid #c7c7c7;
    border-radius: 15px;
    border-radius: 15px;
    padding: 30px 20px;
}
.avatars .title {
    padding-top: 13px;
    font-size: 14px;
    font-weight: 400;
    color: rgba(43,43,43,0.7);
}
.tabbed-card .tab-content{
    padding: 5rem;
}
</style>

@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Add Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Survey</li>
                        <li class="breadcrumb-item">Add survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="f1" method="post">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon"><i class="fa fa-question"></i></div>
                                    <p>Choose</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-question"></i></div>
                                    <p>Template</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                    <p>Date</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-location-arrow"></i></div>
                                    <p>Location</p>
                                </div>
                            </div>

                            <fieldset>
                                <input type="hidden" id="template_id">
                               <!-- bookmark ribbon left side-->
                               <?php

use App\Industry;

$firstIndustry = null;
?>
                                <div class="row">
                                    <div class="col-2">
                                        <ul class="nav flex-column nav-pills" id="v-pills-tab" role="tablist">
                                            @foreach($industries as $key => $industry)
                                            <?php
if ($key == 0) {
    $firstIndustry = Industry::find($industry->id);
}
?>

                                            <li class="nav-item">
                                                <a class="nav-link {{ $key != 0 ?: 'active' }} industry-class" data-id="{{$industry->id}}" data-toggle="tab" href="#{{$industry->id}}" role="tab" aria-controls="{{$industry->id}}" aria-selected="{{ $key == 0 ? 'true': 'false' }}">{{$industry->title}}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-10">
                                        <div class="scroll-bar-wrap">
                                            <div class="horizontal-scroll scroll-demo">
                                                <div class="horz-scroll-content">
                                                    <div class="row" id="templates">
                                                        @foreach($firstIndustry->templates as $template)
                                                        <div class="col-sm-2 col-xl-2 btn-next step2 template" data-id="{{$template->id}}">
                                                            <div class="card">
                                                                <div class="card-body b-b-primary shadow-sm">
                                                                    <div class="h5">{{$template->question_text}}</div>
                                                                    <i>{{$template->description}}</i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="tabbed-card app-border">
                                    <div class="pull-right nav nav-pills nav-dark" id="pills-clrtabinfo" role="tablist">
                                        <div class="nav-item"><a class="nav-link" id="pills-clrhome-tabinfo" data-toggle="pill" href="#pills-clrhomeinfo" role="tab" aria-controls="pills-clrhome" aria-selected="true"><i class="fa fa-angle-left ml-2"></i></a></div>
                                        <div class="nav-item"><a class="nav-link" id="pills-clrprofile-tabinfo1" data-toggle="pill" href="#pills-clrprofileinfo1" role="tab" aria-controls="pills-clrprofile" aria-selected="false"><i class="fa fa-angle-right ml-2"></i></a></div>
                                    </div>
                                    <div class="tab-content" id="pills-clrtabContentinfo">
                                        <div class="tab-pane fade show active" id="pills-clrhomeinfo" role="tabpanel" aria-labelledby="pills-clrhome-tabinfo">
                                            <div class="text-center">
                                                <h4 class="template-title">Please rate our service today</h4>
                                                <div class="avatars pt-3">
                                                    <div class="avatar"><img class="img-70 rounded-circle" src="{{asset('assets/images//smile/emoji4.png')}}" alt="#"></div>
                                                    <div class="avatar"><img class="img-70 rounded-circle" src="{{asset('assets/images/smile/emoji3.png')}}" alt="#"></div>
                                                    <div class="avatar"><img class="img-70 rounded-circle" src="{{asset('assets/images//smile/emoji2.png')}}" alt="#"></div>
                                                    <div class="avatar"><img class="img-70 rounded-circle" src="{{asset('assets/images//smile/emoji1.png')}}" alt="#"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons mt-4">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-next" id="step2" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div>
                                    <h6>Time Period</h6>
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <input class="form-control digits dataRange" type="text" name="datefilter" value="" placeholder="Input Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-next step4" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div class="text-right">
                                    <input type="checkbox" name="select-all" id="all-sub">
                                    <label class="h6" for="all-sub">Select All</label>
                                </div>
                                @foreach($branches as $branch)
                                @php
                                    $sub_branches = \App\SubBranch::all()->where('branch_id',$branch->id);
                                @endphp
                                <div class="media">
                                    <label class="p-t-50 p-b-50 h5">{{$branch->name}}</label>
                                </div>
                                <div class="timeline-small" id="b{{$branch->id}}">
                                    <div class="media pb-3">
                                        <div class="timeline-round m-r-30 timeline-line-1 bg-primary"><i data-feather="home"></i></div>

                                        <div class="media-body">
                                            @foreach($sub_branches as $sub)
                                            <div class="form-group form-check">
                                            <input type="checkbox" name="subbranch"  onclick="storeSubBranch({{$sub->id}})" value="{{$sub->id}}" class="form-check-input" id="sub{{$sub->id}}">
                                                <label class="form-check-label" for="sub{{$sub->id}}">{{$sub->name}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-submit" id="saveQuestion" type="button">Submit</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@php

@endphp
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/form-wizard/form-wizard-three.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/scrollable/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('assets/js/scrollable/scrollable-custom.js')}}"></script>
<script src="{{asset('assets/js/todo/todo.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>

<script>
    $(document).ready(function() {
        var template_id = 0;
        var url = '<?= url('/'); ?>';
        $('.step4').click(function(){
            dateFromTo = $('.dataRange').val();
        });
        $('#templates').on('click', '.template',function(){
            template_id = $(this).data('id');
            // $('.avatars').text("");

            $.ajax({
                url: `/admin/template/getSingle/${template_id}`,
                type: 'GET',
                async: false,
                success: function (response) {
                    $('.template-title').text(response.template.question_text);
                    $('.avatars').html('');
                    $.each(response.template.pre_answers, (i, preAnswer) => {
                        $('.avatars').append(`<div class="avatar"><img class="img-70 rounded-circle" src="${url}/${preAnswer.photo}" alt="#">
                                            <p class="title">${preAnswer.description}</p>
                                            </div>`);
                    })
                },
            });
        });

        $(".industry-class").click(function(){
            var id = $(this).data('id');

            $.ajax({
                url: `/admin/template/getByIndustry/${id}`,
                type: 'GET',
                async: false,
                success: function (response) {
                    $("#templates").html("");
                    $(response.templates).each(function(index, template){
                        var description = template.description ? template.description : '';

                        var templateDiv = `
                        <div class="col-sm-2 col-xl-2 btn-next step2 template" data-id="${template.id}">
                            <div class="card">
                                <div class="card-body b-l-primary shadow">
                                    <div class="h5">${template.question_text}</div>
                                    <i>${description}</i>
                                </div>
                            </div>
                        </div>`;

                        $("#templates").append(templateDiv)
                    });
                },
            });
        });
        $("input[name='select-all']").click(function(){
            $("input[name='subbranch']").prop('checked', $(this).prop('checked'));

            var subList = document.getElementsByName("subbranch");
            for (var i = 0; i < subList.length; i++) {
                if (subList[i].checked) {
                    subBranches.push(subList[i].value);
                } else {
                    subBranches = subBranches.filter(function(subBranch) {
                        return subBranch != subList[i].value;
                    });
                }
            }

        });
        $('#saveQuestion').click(function(){
            var subBranch = subBranches;
            var type = 'template';
            if(subBranch.length <= 0){
            swal({
                position: 'top-end',
                icon: 'warning',
                title: 'Please Select Atleast One Subbranch.',
                showConfirmButton: false,
            });
            } else {
            if(dateFromTo!=''){
                var dat = dateFromTo
            }else{
                var dat = ''
            }
            var token = '{{csrf_token()}}';
            $.ajax({
                url:'{{url("admin/survey")}}',
                type: 'POST',
                async: false,
                headers: {
                'X-CSRF-Token': token
                },
                data :{
                    template_id,
                    dat,
                    subBranch,
                    type
                },
                success: function (response) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'New Question Successfully Saved',
                        showConfirmButton: false,
                    })
                    location.reload();
                },
                });
            }

        })

    });

    var subBranches = [];
    function storeSubBranch(id){
        if($('#sub'+id).is(':checked')){
            subBranches.push(id)
        }else{
            subBranches = subBranches.filter(function(subBranch) {
                return subBranch != id;
            });
        }
    }
</script>
@endsection
