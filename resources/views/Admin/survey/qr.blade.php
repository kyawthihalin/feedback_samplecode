@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<?php
  use Vinkla\Hashids\Facades\Hashids;
  
  $questionId =  Hashids::connection()->encode($question->id);
?>
<div class="page-body">
      <div class="container-fluid">
        <div class="page-header">
          <div class="row">
            <div class="col-lg-6">
              <h3>Generate QR</h3>
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                <li class="breadcrumb-item">Survey List</li>
                <li class="breadcrumb-item active">Generate QR</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      
      <div class="container-fluid">
      @if(auth()->user()->isHotel())
      <div class="row justify-content-md-center" id="p-id">
          <div class="col-md-7 text-center">
            <div class="card">
              <div class="card-body b-l-primary border-2">
                <div class="h5">SCAN THE QR CODE AND TELL US HOW WE DID</div>
                <div class="pt-3">
                  <!-- Get a Placeholder image initially, this will change according to the data entered later -->
                  {!! QrCode::size(100)->generate(url('feedback', [$questionId])); !!}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-md-center">
          <div class="col-md-7 input-group my-3">
            <input type="text" class="form-control" id={{$questionId}} value="{{url('feedback', [$questionId])}}" readonly>
            <span class="input-group-addon m-1">
              <button type="button" class="btn-copy" data-textid={{$questionId}} data-toggle="tooltip" data-trigger="click" data-placement="right" title="Copied" delay="9">
              <i class="fa fa-copy"></i>
              </button>
            </span>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 text-center mb-3">
            <button class="btn btn btn-primary mr-2 printQr" type="button" data-subbranch="p-id">Print</button>
          </div>
        </div>
      <!-- Container-fluid starts-->
      @else
      @foreach($question->subBranch as $subBranch)
      <?php
      $subBranchId = Hashids::connection()->encode($subBranch->id);
      ?>
        <div class="row justify-content-md-center" id="{{$subBranch->name}}">
          <div class="col-md-7 text-center">
            <div class="card">
              <div class="card-body b-l-primary border-2">
                <div class="h5">SCAN THE QR CODE AND TELL US HOW WE DID</div>
                <div class="pt-3">
                  <!-- Get a Placeholder image initially, this will change according to the data entered later -->
                  {!! QrCode::size(100)->generate(url('feedback', [$questionId, $subBranchId])); !!}
                </div>
                <div class="col-xl-12 pt-3 left_side_earning">
                  <span class="font-roboto h6">Location:</span>
                    <h5>{{$subBranch->name}}</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row justify-content-md-center">
          <div class="col-md-7 input-group my-3">
            <input type="text" class="form-control" id="{{$questionId.$subBranchId}}" value="{{url('feedback', [$questionId, $subBranchId])}}" readonly>
            <span class="input-group-addon m-1">
            <button type="button" class="btn-copy" data-textid="{{$questionId.$subBranchId}}" data-toggle="tooltip" data-trigger="click" data-placement="right" title="Copied" delay="9">
            <i class="fa fa-copy"></i>
            </button>
            </span>
            </div>
        </div>
        <div class="row">
          <div class="col-sm-12 text-center mb-3">
            <button class="btn btn btn-primary mr-2 printQr" type="button" data-subbranch="{{$subBranch->name}}">Print</button>
          </div>
        </div>
        @endforeach
      @endif
      </div>
      <!-- Container-fluid Ends-->
    </div>
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip();
      $('.printQr').click(function(){
        var subBranch = $(this).data('subbranch');

        var printContents = document.getElementById(subBranch).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
      })

      $(".btn-copy").click(function(){
        var textId = $(this).data('textid');
        console.log(textId);
        const url = document.querySelector("#"+ textId);
        console.log(url);
        url.select();
        document.execCommand("copy")
      })
    });
</script>
@endsection
