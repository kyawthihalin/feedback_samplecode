@extends('layouts.layout')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Add Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item">Add survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
            <div class="user-profile">
              <div class="row">
                <div class="col-md-{{auth()->user()->isHotel()?12:6}}">
                  <div class="card o-hidden profile-greeting">
                    <div class="card-body">
                      <div class="greeting-user text-center">
                        <div class="profile-vector"><img class="img-fluid" src="../../assets/images/dashboard/scratch.png" alt=""></div>
                        <h4 class="f-w-600 text-dark"><span id="greeting">Create from scratch</span> <span class="right-circle"><i
                              class="fa fa-check-circle f-14 middle"></i></span></h4>
                        <p><span class="text-dark">Design a custom survey that's tailored to your exact specifications.</span></p>
                        <div class="whatsnew-btn"><a href="/admin/survey/create?tp=custom" class="btn btn-primary">Select</a></div>
                      </div>
                    </div>
                  </div>
                </div>
                @if(!auth()->user()->isHotel())
                <div class="col-md-6">
                  <div class="card o-hidden profile-greeting">
                    <div class="card-body">
                      <div class="greeting-user text-center">
                        <div class="profile-vector"><img class="img-fluid" src="../../assets/images/dashboard/template.png" alt=""></div>
                        <h4 class="f-w-600 text-dark"><span id="greeting">Use a template</span> <span class="right-circle"><i
                              class="fa fa-check-circle f-14 middle"></i></span></h4>
                        <p><span class="text-dark"></span> Save time with a pre-built survey that can be used out-of-the-box or modified to fit your needs.</span></p>
                        <div class="whatsnew-btn"><a href="/admin/survey/create?tp=template" class="btn btn-primary">Select</a></div>
                      </div>
                    </div>
                  </div>
                </div>
                @endif
                <!-- <div class="col-md-12 text-center">    
                  <span>
                  <i class="fa fa-bell mr-1"> </i>You can also duplicate and edit an existing survey from your survey list.
                  </span>
                </div> -->
              </div>
            </div>
          </div>
    <!-- Container-fluid Ends-->
</div>
@endsection
