@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<style>
    .title{
        border: 1px solid #ffffff;
        border-radius: 2px;
        outline: none;
        background-color: #ffffff;
    }
    .clicked{
        border-color: #333;
    }
</style>
@endsection

@section('content')
<div class="page-body">
    <div class="container-fluid">
    <div class="page-header">
        <div class="row">
        <div class="col-lg-12">
            <h3>Preview</h3>
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
            <li class="breadcrumb-item">Survey List</li>
            <li class="breadcrumb-item active">Preview</li>
            </ol>
        </div>
        </div>
    </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="card">
            <div class="card-header">
            <h5>Preview Feedback</h5>
            </div>
            <div class="card-body">
            <form class="form-wizard" id="regForm" action="#" method="POST">    
                <div class="tab text-center">
                @if(auth()->user()->isHotel())
                    @if(!$question->template_id)
                    @foreach(json_decode($question->question_text) as $text)
                    <h5>{{$text}}</h5>
                    @endforeach
                    @else
                    <h5>{{$question->question_text}}</h5>
                    @endif
                @else
                <h5>{{$question->question_text}}</h5>
                @endif
                <div class="avatars pt-2">
                    <div class="avatar">
                    <img class="img-70 rounded-circle" src="{{asset('assets/images/smile/emoji1.png')}}" alt="#">
                    </div>
                    <div class="avatar">
                    <img class="img-70 rounded-circle" src="{{asset('assets/images/smile/emoji2.png')}}" alt="#">
                    </div>
                    <div class="avatar">
                    <img class="img-70 rounded-circle" src="{{asset('assets/images/smile/emoji3.png')}}" alt="#">
                    </div>
                    <div class="avatar">
                    <img class="img-70 rounded-circle" src="{{asset('assets/images/smile/emoji4.png')}}" alt="#">
                    </div>
                </div>
                </div>
                <div class="tab text-center">
                <h5>Would You like to chat about your experience</h5>
                <div class="avatars py-3">

                    @if($question->type == 'custom')
                        @foreach($question->preAnswers as $preAns)
                        <div class="avatar">
                        <img class="img-70 rounded-circle" src="{{asset('assets/images/feedback/f1.png')}}" alt="#">
                        <div class="pt-3">{{$preAns->description}}</div>
                        </div>
                        @endforeach
                    @else
                    @foreach($question->template->preAnswers as $preAnswer)
                    <div class="avatar title mb-2" style="cursor: pointer;" id="{{$preAnswer->id}}" data-preid="{{$preAnswer->id}}">
                        <img src="{{url ($preAnswer->photo)}}" alt="" width="100">
                        <p>
                        {{$preAnswer->description}}
                        </p>
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="row justify-content-md-center">
                    <div class="form-group mb-3 col-md-5">
                    <input class="form-control" id="feedback" type="text" placeholder="Share your experience">
                    </div>
                </div>
                <button class="btn btn-dark" id="" type="button">Submit</button>
                </div>
                <div>
                <div class="text-right btn-mb">
                    <button class="btn btn-light" id="prevBtn" type="button" onclick="nextPrev(-1)">Previous</button>
                    <button class="btn btn-primary" id="nextBtn" type="button" onclick="nextPrev(1)">Next</button>
                </div>
                </div>
                <!-- Circles which indicates the steps of the form:-->
                <div class="text-center"><span class="step"></span><span class="step"></span></div>
                <!-- Circles which indicates the steps of the form:-->
            </form>
            </div>
        </div>
        </div>
    </div>
    </div>
    <!-- Container-fluid Ends -->
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/form-wizard.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        $(".title").click(function(){
            var preid = $(this).data('preid');
            $("#preid").val(preid);

            var elems = document.querySelectorAll(".title.clicked");

            [].forEach.call(elems, function(el) {
                el.classList.remove("clicked");
            });

            $("#"+ preid).addClass('clicked');
        });
    });
</script>
@endsection
