@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/todo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Add Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item">Add survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="f1" method="post" autocomplete="off">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon"><i class="fa fa-question"></i></div>
                                    <p>Question</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                    <p>Date</p>
                                </div>
                            </div>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-10">
                                        <span style="font-size: 22px;">Add Questions</span>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-primary text-right" id="btn-add-q"> + Add </button>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group m-t-15 custom-radio-ml questions">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend col-md-6">
                                                        <input type="text" class="form-control input-question" id="input-1" placeholder="Fill a question">
                                                </div>
                                            </div>
                                            <div class="input-group mb-3" id="second">
                                                <div class="input-group-prepend col-md-6">
                                                    <input type="text" class="form-control input-question" id="input-2" placeholder="Fill a question">
                                                </div>
                                                <button type="button" class="btn btn-warning ml-1 btn-close-q" data-id="second">
                                                    <span class="fa fa-times"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-next" id="step2" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div>
                                    <h6>Time Period</h6>
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <input class="form-control digits dataRange" type="text" name="datefilter" value="" placeholder="Input Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-submit" id="saveQuestion" type="button">Submit</button>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@php
    
@endphp
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/form-wizard/form-wizard-three.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/todo/todo.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

<script>
    var questionList = [];
    var questions = @json($questions);

    $(document).ready(function() {
        autocomplete(document.getElementById('input-1'), questions);
        autocomplete(document.getElementById('input-2'), questions);

        function findDuplicates(array) {
            let valuesAlreadySeen = []

            for (let i = 0; i < array.length; i++) {
                let value = array[i]
                if (valuesAlreadySeen.indexOf(value) !== -1) {
                return true
                }
                valuesAlreadySeen.push(value)
            }
            return false
        }

        $("#btn-add-q").click(function(){
            var date = Date.now();
            var input = `<div class="input-group mb-3" id="${date}">
                            <div class="input-group-prepend col-md-6">
                                <input type="text" class="form-control input-question" id="input-${date}" placeholder="Fill a question">
                            </div>`;
            var button = `<button type="button" class="btn btn-light ml-1 btn-close-q" data-id="${date}">
                            <span class="fa fa-times"></span>
                          </button>
                        </div>`;
            $(".questions").append(`${input}${button}`);
            autocomplete(document.getElementById(`input-${date}`), questions);
        })

        $(".questions").on('click', '.btn-close-q', function(){
            var divid = $(this).data('id');
            $("#" + divid).remove();
        })

        $('#step2').click(function(){
            questionList = [];
            var inputs = $(".input-question");
            for(var i = 0; i < inputs.length; i++){
                if($(inputs[i]).val() != ''){
                    questionList.push($(inputs[i]).val());
                }
            }
            if(findDuplicates(questionList) && questionList.length > 0){
                alert('Questions must be unique');
                setTimeout(function() {
                $(".btn-previous").click();
                }, 3);
                questionList = [];
                $('.input-question').addClass('input-error');
            }
        })
        $('#saveQuestion').click(function(){
            dat = $('.dataRange').val();
                
            var token = '{{csrf_token()}}';
            $.ajax({
                url:'{{url("admin/survey")}}',
                type: 'POST',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                data :{
                    questionList,
                    dat,
                },
                success: function (response) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'New Question Successfully Saved',
                        showConfirmButton: false,
                    })
                    location.reload();
                },
            }); 
        })
    });
</script>
@endsection
