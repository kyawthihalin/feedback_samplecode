@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<style>
   .counter{
        display: inline-block;
        font-size: 18px;
        min-width: 45px;
        padding: 7px;
        color: white;
        background-color: #5fe921;
    }

    .pager{
        font-size: 21px;
    }

    svg{
        width: 18px !important;
        height: 18px !important;
    }
  </style>
@endsection

@section('content')
<?php 
$questions = $survey->questions->pluck('question_text')->toArray();
?>
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
            <div class="col-lg-12">
                <h3>Preview</h3>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                <li class="breadcrumb-item">Survey List</li>
                <li class="breadcrumb-item active">Preview</li>
                </ol>
            </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-12">
                <span class="rounded-circle mb-2 counter">
                    <span class="currentCount">1</span>/{{count($questions)}}
                </span>
                @foreach($questions as $key => $text)
                <div class="question question-{{$key}}"><h4>{{$text}}</h4>
                    <div class="avatars pt-3">
                        <div class="avatar" style="cursor: pointer;">
                            <img class="img-70 rounded-circle emoji" data-key="{{$key}}" data-question="{{$text}}" data-id="1" src="{{asset('assets/images//smile/emoji4.png')}}" alt="#">
                        </div>
                        <div class="avatar" style="cursor: pointer;">
                            <img class="img-70 rounded-circle emoji" data-key="{{$key}}" data-question="{{$text}}" data-id="2" src="{{asset('assets/images/smile/emoji3.png')}}" alt="#">
                        </div>
                        <div class="avatar" style="cursor: pointer;">
                            <img class="img-70 rounded-circle emoji" data-key="{{$key}}" data-question="{{$text}}" data-id="3" src="{{asset('assets/images//smile/emoji2.png')}}" alt="#">
                        </div>
                        <div class="avatar" style="cursor: pointer;">
                            <img class="img-70 rounded-circle emoji" data-key="{{$key}}" data-question="{{$text}}" data-id="4" src="{{asset('assets/images//smile/emoji1.png')}}" alt="#">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="col-md-12">
                <ul class="pager">
                    <li class="mr-3"><a href="#" class="back py-3"><i class="feathers" data-feather="skip-back"></i> Back</a></li>
                    <li class="mr-3"><a href="#" class="skip py-3">Skip <i class="feathers" data-feather="skip-forward"></i></a></li>
                    <li class="mr-3"><a href="#" class="finish py-3"><i class="feathers" data-feather="check-circle"></i> Finish</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
</div>

@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.ui.min.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/form-wizard.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        var questionArr = [];
        var currentQKey = 0;
        var questions = @json($questions);
        var survey_id = <?= request()->segment(3); ?>;
        var url = `{{url('admin/survey/${survey_id}/preview')}}`;

        $.each(questions, (index, value) => {
            if(index){
                $(".question-" + index).hide();
            }
        });

        $(".back").hide();
        $(".finish").hide();

        
        function submitQuestion(){
            if(questionArr[0] == null){
                swal({
                    position: 'top-end',
                    icon: 'warning',
                    title: 'Submitted survey is empty',
                    button: false,
                })
                window.location = url;
            }
            else{
                swal({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your feedback has been successfully submitted',
                    showConfirmButton: false,
                })

                window.location = url;
            }
        }

        $(".emoji").click(function(){
            var emojiId = $(this).data('id');
            var question = $(this).data('question');
            currentQKey = $(this).data('key');
            $(".question-" + currentQKey).hide();
            currentQKey++;
            $(".question-" + currentQKey).show();

            if(currentQKey){
                $(".back").show();
                $(".finish").show();
            }


            $(".currentCount").text(currentQKey+1);
            
            questionArr.push({
                'emojiId': emojiId,
                'question': question
            })

            if(currentQKey === questions.length-1){
                $(".skip").hide();
            }

            if(currentQKey === questions.length){
                $(".skip").hide();
                submitQuestion();
            }
        });

        $(".back").click(function(){
            questionArr.splice(-1,1);
            $(".question-" + currentQKey).hide();
            $(".currentCount").text(currentQKey);

            currentQKey--;
            $(".question-" + currentQKey).show();

            if(currentQKey < questions.length-1){
                $(".skip").show();
            }

            if(!currentQKey){
                $(".back").hide();
                $(".finish").hide();
            }
        });

        $(".skip").click(function(){
            $(".question-" + currentQKey).hide();
            currentQKey++;
            $(".question-" + currentQKey).show();

            if(currentQKey){
                $(".back").show();
                $(".finish").show();
            }

            if(currentQKey === questions.length-1){
                $(".skip").hide();
            }

            $(".currentCount").text(currentQKey+1);
            
            questionArr.push(null);
        });

        $(".finish").click(function(){
            var left = questions.length - currentQKey;
            while(left > 0){
                questionArr.push(null);
                left--
            }
            submitQuestion();
        });
    });
</script>
@endsection
