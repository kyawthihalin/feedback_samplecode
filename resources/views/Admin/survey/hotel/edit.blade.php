@extends('layouts.layout')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/todo.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/daterange-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Edit Survey</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">Manage Survey</li>
                        <li class="breadcrumb-item">Edit survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <form class="f1" method="post" autocomplete="off">
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon"><i class="fa fa-question"></i></div>
                                    <p>Question</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i class="fa fa-calendar"></i></div>
                                    <p>Date</p>
                                </div>
                            </div>
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5>Edit Survey</h5>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group m-t-15 custom-radio-ml questions">
                                            <button type="button" class="btn btn-primary mb-2" id="btn-add-q">+</button>

                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-next" id="step2" type="button">Next</button>
                                </div>
                            </fieldset>
                            <fieldset>
                                <div>
                                    <h6>Time Period</h6>
                                    <div class="theme-form">
                                        <div class="form-group">
                                            <input class="form-control digits dataRange" type="text" name="datefilter" value="" placeholder="Input Date">
                                        </div>
                                    </div>
                                </div>
                                <div class="f1-buttons">
                                    <button class="btn btn-primary btn-previous" type="button">Previous</button>
                                    <button class="btn btn-primary btn-submit" id="saveQuestion" type="button">Submit</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@php

@endphp
<!-- Container-fluid Ends-->
@endsection
@section('js')
<script src="{{asset('assets/js/form-wizard/form-wizard-three.js')}}"></script>
<script src="{{asset('assets/js/form-wizard/jquery.backstretch.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/todo/todo.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script>
    var dateFromTo = '';
    var questionList = [];
    var removedQuestions = [];
    var preQuestions = @json($preQuestions);

    $(document).ready(function() {
        var survey = @json($survey);
        var questionIds = @json($survey->questions->pluck('id')->toArray());
        var questions = @json($survey->questions->pluck('question_text', 'id')->toArray());

        var i = 0;
        for(var id in questions){
            if (i === 0) {
                $(".questions").append(
                                `<div class="input-group mb-3" id="${date}">
                                    <div class="input-group-prepend col-md-6">
                                        <input type="text" value="${questions[id]}" id="${id}" class="form-control input-question">
                                    </div>
                                </div>`);
                autocomplete(document.getElementById(`${id}`), preQuestions);
            }
            else{
                var input = `<div class="input-group mb-3" id="${id}">
                                <div class="input-group-prepend col-md-6" id="${date}">
                                    <input type="text" value="${questions[id]}" id="${id}" class="form-control input-question">
                                </div>`;
                var button = `<button type="button" class="btn btn-warning ml-1 btn-close-q" data-id="${id}">
                                <span class="fa fa-times"></span>
                                </button>
                            </div>`;
                $(".questions").append(input + button);
                autocomplete(document.getElementById(`${id}`), preQuestions);
            }
            i++;
        }

        $("#btn-add-q").click(function(){
            var date = Date.now();
            var input = `<div class="input-group mb-3" id="${date}">
                            <div class="input-group-prepend col-md-6">
                                <input type="text" class="form-control input-question" id="new${date}">
                            </div>`;
            var button = `<button type="button" class="btn btn-warning ml-1 btn-close-q" data-id="${date}">
                            <span class="fa fa-times"></span>
                          </button>
                        </div>`;
            $(".questions").append(`${input}${button}`);
            autocomplete(document.getElementById(`new${date}`), preQuestions);
        })

        $(".questions").on('click', '.btn-close-q', function(){
            var divid = $(this).data('id');
            if(questionIds.includes(divid)){
                removedQuestions.push(divid);
            }

            $("#" + divid).remove();
        })
        
        $('#step2').click(function(){
            questionList = [];
            var inputs = $(".input-question");
            for(var i = 0; i < inputs.length; i++){
                var value = $(inputs[i]).val();
                var id = $(inputs[i]).attr('id'); 
                questionList.push({id , value});
            }
        })

        var fromDate = new Date(survey.from);
        var toDate = new Date(survey.to);

        $('.dataRange').val(fromDate.toLocaleDateString("en-US") + ' - ' + toDate.toLocaleDateString("en-US"));

        $('#saveQuestion').click(function(){
            date = $('.dataRange').val();

            var token = '{{csrf_token()}}';

            var i = 0;
            for(var index in questionList){
                
                var data = {text : questionList[index].value};
                var id = questionList[index].id;
                if(i == 0){
                    data.date = date;
                    removedQuestions.length > 0 ? data.removedQuestions = removedQuestions: '';
                }
                
                if(id.replace(/[0-9]/g, '') == "new"){
                    var survey_id = survey.id
                    questions = [data.text];
                
                    $.ajax({
                        url:'{{url("admin/survey")}}',
                        type: 'POST',
                        async: false,
                        headers: {
                        'X-CSRF-Token': token 
                        },
                        data :{
                            questionList : questions,
                            survey_id,
                            dat: date
                        },
                        success: function (response) {
                            swal({
                                position: 'top-end',
                                icon: 'success',
                                title: 'New Question Successfully Saved',
                                showConfirmButton: false,
                            })
                            location.reload();
                        },
                    }); 
                }
                else{
                    $.ajax({
                        url:'{{url("admin/survey")}}/'+ id,
                        type: 'PATCH',
                        async: false,
                        headers: {
                        'X-CSRF-Token': token
                        },
                        data :data,
                        success: function (response) {
                            swal({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Survey Successfully Updated',
                                showConfirmButton: false,
                            })
                            location.reload();
                        },
                    });
                }
                i++;
            }

        })

    })
</script>
@endsection
