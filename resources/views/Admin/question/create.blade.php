@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

    <!-- Page Sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6">
                <h3>Create Feedback Question</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Question</li>
                  <li class="breadcrumb-item">Create Feedback Question</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fxluid starts-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-10">
                      <h5>Feedback Question </h5>
                    </div>
                    <div class="col-md-2 text-right">
                      <button type="button" class="btn btn-primary" id="btn-add-q">+ Add</button>
                    </div>

                  </div>
                </div>
                <form class="f1">
                  <div class="card-body">
                    <div class="row justify-content-md-center">
                      <div class="col-12">
                        <div class="form-group row mb-0">
                          <div class="col-sm-12 text-center">
                          <div class="form-group m-t-15 custom-radio-ml questions">
                              <div class="input-group mb-3">
                                  <div class="input-group-prepend col-md-6 offset-md-3">
                                      <input type="text" class="form-control input-question" placeholder="Fill a question">
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <input class="btn btn-light" type="reset" value="Cancel">
                    <button class="btn btn-primary" id="submit" type="button">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <!-- footer start-->
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    var questionList = [];
    $(document).ready(function() {

        var errors = @json(array_unique($errors->all()));

        function findDuplicates(array) {
            let valuesAlreadySeen = []

            for (let i = 0; i < array.length; i++) {
                let value = array[i]
                if (valuesAlreadySeen.indexOf(value) !== -1) {
                return true
                }
                valuesAlreadySeen.push(value)
            }
            return false
        }

        $("#btn-add-q").click(function(){
            var date = Date.now();
            var input = `<div class="input-group mb-3" id="${date}">
                            <div class="input-group-prepend col-md-6 offset-md-3">
                                <input type="text" class="form-control input-question" placeholder="Fill a question">
                            </div>`;
            var button = `<button type="button" class="btn btn-light ml-1 btn-close-q" data-id="${date}">
                            <span class="fa fa-times"></span>
                          </button>
                        </div>`;
            $(".questions").append(`${input}${button}`);
        })

        $(".questions").on('click', '.btn-close-q', function(){
            var divid = $(this).data('id');
            $("#" + divid).remove();
        })

        $('#submit').click(function(){
            questionList = [];
            var inputs = $(".input-question");
            for(var i = 0; i < inputs.length; i++){
                if($(inputs[i]).val() != ''){
                    questionList.push($(inputs[i]).val());
                }
            }
            if(!questionList.length){
                $('.input-question').addClass('input-error');
            }
            if(findDuplicates(questionList) && questionList.length > 0){
                alert('Questions must be unique');
                questionList = [];
                $('.input-question').addClass('input-error');
            }
                
            var token = '{{csrf_token()}}';
            $.ajax({
                url:'{{url("admin/template")}}',
                type: 'POST',
                async: false,
                headers: {
                'X-CSRF-Token': token 
                },
                data :{
                   question: questionList,
                },
                success: function (response) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Questions Successfully Saved',
                        button: false,
                    })
                    location.reload();
                },
                error: function (error) {
                    swal({
                        position: 'top-end',
                        icon: 'warning',
                        title: error.responseJSON.error,
                        button: false,
                    });
                    var questions = error.responseJSON.questions;
                    var inputs = $('.input-question');
                    $.each(inputs, (id, val) => {
                        var text = $(val).val();
                        const found = questions.find(element => element == text);
                        console.log(found);
                        if(found){
                            console.log(val);
                            $(val).addClass('input-error');
                        }
                    });
                }
            }); 

        })
    });
</script>
@endsection