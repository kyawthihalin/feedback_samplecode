@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

    <!-- Page Sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6">
                <h3>Question List</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Question</li>
                  <li class="breadcrumb-item">Question List</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fxluid starts-->
        <div class="container-fluid">
          <div class="row">
            <!-- Zero Configuration Starts-->
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-12">
                      <h5>Question List</h5>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="display" id="example">
                      <thead>
                        <tr>
                          <th>Question</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                          <th>Question</th>
                          <th>Actions</th>
                    </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Zero Configuration  Ends-->

          </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <!-- footer start-->
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function() {
        var userTable = $('#example').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "template/getAll",
            "paging": true,
            columns: [
              
                {
                    data: 'question_text',
                    name: 'question_text'
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });

        var errors = @json(array_unique($errors->all()));

        if(errors.length){
            var text = '';
            $.each(errors, function( index, value ) {
                text += value;
            });
            swal({
                position: 'top-end',
                icon: 'warning',
                title: text,
                showConfirmButton: false,
            });
        }

        var success = @json(\Session::get('success'));
        if(success){
            swal({
                position: 'top-end',
                icon: 'success',
                title: success,
                showConfirmButton: false,
            });
        }

        $(document).on('click', '#delete-btn', function() {
            var id = $(this).data('id');
            let token = '{{csrf_token()}}';
            swal({
                    title: "Are you sure?",
                    text: "This question will be deleted from your question list.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : '{{url("admin/template")}}' + '/' + id,
                            type: 'DELETE',
                            async: false,
                            headers: {
                                'X-CSRF-Token': token
                            },
                            success: function(responese) {
                                swal({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Question Successfully Deleted',
                                    button: false,
                                })
                                userTable.ajax.reload()
                            },
                            error: function(error) {
                                swal({
                                    position: 'top-end',
                                    icon: 'warning',
                                    title: JSON.parse(error.responseText)[1],
                                    button: false,
                                })
                            }
                        });
                    } else {
                        swal("Question is safe!");
                    }
                })
        })
    });
</script>
@endsection