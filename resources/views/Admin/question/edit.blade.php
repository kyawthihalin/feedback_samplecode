@extends('layouts.layout')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')

    <!-- Page Sidebar Ends-->
    <div class="page-body">
        <div class="container-fluid">
          <div class="page-header">
            <div class="row">
              <div class="col-lg-6">
                <h3>Edit Question</h3>
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                  <li class="breadcrumb-item">Question List</li>
                  <li class="breadcrumb-item">Edit</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <!-- Container-fxluid starts-->
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-header">
                  <h5>Edit Question</h5>
                </div>
                <form class="form theme-form" id="edit-form" action="{{url('admin/template', $template->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 pb-2">
                        <label class="col-sm-12 col-form-label pl-0">Feedback Question <span class="text-danger">*</span></label>
                        <div class="form-group row mb-0">
                          <div class="col-sm-6">
                            <input class="form-control" id="" name="question_text" value="{{$template->question_text}}" type="text" placeholder="Type Feedback Question" required>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer text-right">
                    <input class="btn btn-light" type="reset" value="Cancel">
                    <button class="btn btn-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
            <!-- Zero Configuration  Ends-->

          </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
    <!-- footer start-->
@endsection

@section('js')
<script src="{{asset('assets/js/sweet-alert/sweetalert.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
    $(document).ready(function(){

        var errors = @json(array_unique($errors->all()));

        if(errors.length){
            var text = '';
            $.each(errors, function( index, value ) {
                text += value;
            });
            swal({
                position: 'top-end',
                icon: 'warning',
                title: text,
                showConfirmButton: false,
            });
        }

        var success = @json(\Session::get('success'));
        if(success){
            swal({
                position: 'top-end',
                icon: 'success',
                title: success,
                showConfirmButton: false,
            });
        }
    });
</script>
@endsection