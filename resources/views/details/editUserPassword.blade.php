<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-0">Change Password</h4>
                <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
            </div>
            <div class="card-body">
                <div class="row">
                <div class="col-md-12">
                        <div class="datetime-picker">
                        <form class="theme-form">
                            <div class="form-group col-md-6">
                                <label for="oldPassword">Old Password</label>
                                <input type="password" class="form-control" id="oldPassword" name="password" placeholder="Enter old password" min="5" required>
                                <small id="editErrorPassword" style="color: red;"></small>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="editPassword">Password</label>
                                <input type="password" class="form-control" id="editPassword" name="password" placeholder="Enter password" min="5" required>
                                <small id="editErrorPassword" style="color: red;"></small>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="editCpassword">Confirm Password</label>
                                <input type="password" class="form-control" id="editCpassword" name="cpassword" placeholder="Enter confirm password" min="5" required>
                                <small id="editErrorCPassowrd" style="color: red;"></small>
                            </div>
                        </div>
                        <input type="hidden" id="user_id" value="{{$user->id}}">
                        </form>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right">
                        <button class="btn btn-primary" id="updateAdmin" type="submit">Update</button>
                        <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                    </div>
                </div>
            </div>
</div>
    </div>
</div>

<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
let Cancel = () => {
    location.reload()
}

$(document).on('click', '#updateAdmin',function(event) {
    event.preventDefault();
    let id = $('#user_id').val();
    let name = $('#editName').val();
    let email = $('#editEmail').val();
    let country_id = $('#editCountry').val();
    let industry_id = $('#editIndustry').val();
    let datefilter = $('#editDatefilter').val();
    let password = $('#editPassword').val();
    let cpassword = $('#editCpassword').val();
    let roles=[];
    roles = $('#editRoles').val();
    let token = '{{csrf_token()}}';
    $.ajax({
        url: '{{route('updateAccount')}}',
        type: 'POST',
        async: false,
        headers: {
            'X-CSRF-Token': token
        },
        data: {
            id,
            name,
            email,
            country_id,
            industry_id,
            datefilter,
            password,
            cpassword,
            roles
        },
        success: function(responese) {
            // $('#exampleModal1').modal('hide');
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Admin Successfully Updated',
                showConfirmButton: false,
            })
            // userTable.ajax.reload();
            location.reload();
        },
        error: function(response) {
            console.log(response)
            if (response.responseJSON.errors) {
                if (response.responseJSON.errors.name) {
                    $('#editErrorName').text(response.responseJSON.errors.name);
                    $('#editName').css('border-color', 'red');
                }
                else {
                    $('#editErrorName').text('');
                    $('#editName').css('border-color', 'black');
                }

                if (response.responseJSON.errors.email) {
                    $('#editErrorEmail').text(response.responseJSON.errors.email);
                    $('#editEmail').css('border-color', 'red');
                }
                else {
                    $('#editErrorEmail').text('');
                    $('#editEmail').css('border-color', 'black');
                }
                if (response.responseJSON.errors.password) {
                    $('#editErrorPassword').text(response.responseJSON.errors.password);
                    $('#editPassword').css('border-color', 'red');
                }
                else {
                    $('#editErrorPassword').text('');
                    $('#editPassword').css('border-color', 'black');
                }
                if (response.responseJSON.errors.cpassword) {
                    $('#errorCPassowrd').text(response.responseJSON.errors.cpassword);
                    $('#editCpassword').css('border-color', 'red');
                }
                else {
                    $('#errorCPassowrd').text('');
                    $('#editCpassword').css('border-color', 'black');
                }
                if (response.responseJSON.errors.roles) {
                    $('#editErrorRole').text(response.responseJSON.errors.roles);
                    $('#editRoles').css('border-color', 'red');
                }
                else {
                    $('#editErrorRole').text('');
                    $('#editRoles').css('border-color', 'black');
                }
                if (response.responseJSON.errors.country) {
                    $('#editErrorCountry').text(response.responseJSON.errors.country);
                    $('#editCountry').css('border-color', 'red');
                }
                else {
                    $('#editErrorCountry').text('');
                    $('#editCountry').css('border-color', 'black');
                }
                if (response.responseJSON.errors.industry) {
                    $('#editErrorIndustry').text(response.responseJSON.errors.industry);
                    $('#editIndustry').css('border-color', 'red');
                }
                else {
                    $('#editErrorIndustry').text('');
                    $('#editIndustry').css('border-color', 'black');
                }
                if (response.responseJSON.errors.datefilter) {
                    $('#editErrorDatefilter').text(response.responseJSON.errors.datefilter);
                    $('#editDatefilter').css('border-color', 'red');
                }
                else {
                    $('#editErrorDatefilter').text('');
                    $('#editDatefilter').css('border-color', 'black');
                }
            }
        }
    });
})
</script>