<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-0">User detail</h4>
                <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
            </div>
            <div class="card-body">
                <h5 class="mb-4">Company Information</span></h5>
                <ul>
                    <li style="padding-bottom: 20px;">Name :<span class="font-primary first_name_1">{{$user->name}}</span></li>
                    <li style="padding-bottom: 20px;">Country :<span class="font-primary first_name_1">{{$user->country->name}}</span></li>
                    <li style="padding-bottom: 20px;">Industry :<span class="font-primary first_name_1">{{$user->industry->title}}</span></li>
                </ul>
                
                @foreach ($user->branch as $branch)
                <div class="col-md-12">
                    <div class="card-body p-3">
                        <h6 class="card-title py-2">Experience Point</h6>
                        <ul>
                            <li style="padding-bottom: 20px;">Experience Point : <span class="font-primary first_name_1">{{$branch->name}}</span></li>
                            <li style="padding-bottom: 20px;">Mobile No : <span class="font-primary mobile_num_1">{{$branch->phone}}</span></li>
                            <li style="padding-bottom: 20px;">Email Address : <span class="font-primary email_add_1">{{$branch->email}}</span></li>
                            @if (count($branch->subBranch) > 0)    
                            @endif
                        </ul>
                        <div class="col-12 row">
                            <div class="col-12">
                                <h6 class="py-4">Subgroup List</h6>
                            </div>
                            @foreach ($branch->subBranch as $subbranch)    
                            <div class="col-4">
                                <div class="card">
                                <div class="card-body p-4">
                                    <h6 class="card-title pb-2">{{$subbranch->name}}</h6>
                                    <ul>
                                        <li style="padding-bottom: 15px;">Start Date : <span class="font-primary email_add_1">{{date('d/m/Y',strtotime($subbranch->start_date))}}</span></li>
                                        <li style="padding-bottom: 15px;">End Date : <span class="font-primary email_add_1">{{date('d/m/Y',strtotime($subbranch->end_date))}}</span></li>
                                    </ul>
                                    @if ($subbranch->status == 1)
                                    <span class="badge badge-success badge-pill">Active</span>
                                    @else
                                    <span class="badge badge-primary badge-pill">UnActive</span>
                                    @endif
                                </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>