<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Edit User</h4>
                    <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                </div>
                <div class="card-body">
                    <div class="row">
                    <div class="col-md-12">
                            <div class="datetime-picker">
                            <form class="theme-form">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="name">Company Name</label>
                                    <input type="text" class="form-control" id="editName" name="name" placeholder="Enter Company Name" value="{{$user->name}}" required>
                                    <small id="editErrorName" style="color: red;"></small>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="editEmail" name="email" placeholder="Enter Email" value="{{$user->email}}" required>
                                    <small id="editErrorEmail" style="color: red;"></small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="country">Country</label>
                                    <select class="form-control btn-square" id="editCountry">
                                    <option value="">Select Country</option>
                                    @foreach($countries as $country)
                                    @if($country->id == $user->country_id)
                                    <option value="{{$country->id}}" selected>{{$country->name}}</option>
                                    @else
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endif
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="editErrorCountry"></small>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="industry">Industry</label>
                                    <select class="form-control btn-square" id="editIndustry">
                                    <option value="">Select Industry</option>
                                    @foreach($industries as $industry)
                                    @if($industry->id == $user->industry_id)
                                    <option value="{{$industry->id}}" selected>{{$industry->title}}</option>
                                    @else
                                    <option value="{{$industry->id}}">{{$industry->title}}</option>
                                    @endif
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="errorIndustry"></small>
                                </div>
                                @if (auth()->user()->isAdmin())    
                                <div class="form-group col-md-6">
                                    <label for="datefilter">Start Date - End Date</label>
                                    
                                <input class="form-control dataRange" type="text" name="datefilter" id="editDatefilter" value="{{$start_date}}-{{$end_date}}" placeholder="Start Date - End Date" autocomplete="off" required>
                                
                                    <small style="color: red;" id="editErrorDatefilter"></small>
                                </div>

                                <div class="form-group col-md-6">
                                <label class="col-form-label text-right">Role</label>
                                <div class="">
                                    <select class="form-control btn-square" id="editRoles" required>
                                    <option value="">Select Role</option>
                                    @foreach($roles as $role)
                                    @if($role->id == $roleId)
                                    <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                    @else
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endif
                                    @endforeach
                                    </select>
                                    <small style="color: red;" id="editErrorRole"></small>
                                </div>
                                </div>
                                @endif

                                {{-- <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="editPassword" name="password" placeholder="Enter password" min="5" required>
                                    <small id="editErrorPassword" style="color: red;"></small>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="exampleInputPassword1">Confirm Password</label>
                                    <input type="password" class="form-control" id="editCpassword" name="cpassword" placeholder="Enter confirm password" min="5" required>
                                    <small id="editErrorCPassowrd" style="color: red;"></small>
                                </div> --}}
                            </div>
                            <input type="hidden" id="user_id" value="{{$user->id}}">
                            </form>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" id="updateAdmin" type="submit">Update</button>
                            <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
{{-- <script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script> --}}
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>

    function Cancel() {
        location.reload();
    }

(function($) {
    "use strict";
    $(function() {
        var start = moment().subtract(29, 'days');
        var end = moment();
        $('input[name="datefilter"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        });

        $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

})(jQuery);

    $(document).on('click', '#updateAdmin',function(event) {
        event.preventDefault();
        let id = $('#user_id').val();
        let name = $('#editName').val();
        let email = $('#editEmail').val();
        let country_id = $('#editCountry').val();
        let industry_id = $('#editIndustry').val();
        let datefilter = $('#editDatefilter').val();
        // let password = $('#editPassword').val();
        // let cpassword = $('#editCpassword').val();
        let roles=[];
        roles = $('#editRoles').val();
        let token = '{{csrf_token()}}';
        $.ajax({
            url: '{{route('updateAccount')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                id,
                name,
                email,
                country_id,
                industry_id,
                datefilter,
                roles
            },
            success: function(responese) {
                // $('#exampleModal1').modal('hide');
                swal({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Admin Successfully Updated',
                    showConfirmButton: false,
                })
                // userTable.ajax.reload();
                location.reload();
            },
            error: function(response) {
                console.log(response)
                if (response.responseJSON.errors) {
                    if (response.responseJSON.errors.name) {
                        $('#editErrorName').text(response.responseJSON.errors.name);
                        $('#editName').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorName').text('');
                        $('#editName').css('border-color', 'black');
                    }

                    if (response.responseJSON.errors.email) {
                        $('#editErrorEmail').text(response.responseJSON.errors.email);
                        $('#editEmail').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorEmail').text('');
                        $('#editEmail').css('border-color', 'black');
                    }
                    // if (response.responseJSON.errors.password) {
                    //     $('#editErrorPassword').text(response.responseJSON.errors.password);
                    //     $('#editPassword').css('border-color', 'red');
                    // }
                    // else {
                    //     $('#editErrorPassword').text('');
                    //     $('#editPassword').css('border-color', 'black');
                    // }
                    // if (response.responseJSON.errors.cpassword) {
                    //     $('#errorCPassowrd').text(response.responseJSON.errors.cpassword);
                    //     $('#editCpassword').css('border-color', 'red');
                    // }
                    // else {
                    //     $('#errorCPassowrd').text('');
                    //     $('#editCpassword').css('border-color', 'black');
                    // }
                    if (response.responseJSON.errors.roles) {
                        $('#editErrorRole').text(response.responseJSON.errors.roles);
                        $('#editRoles').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorRole').text('');
                        $('#editRoles').css('border-color', 'black');
                    }
                    if (response.responseJSON.errors.country) {
                        $('#editErrorCountry').text(response.responseJSON.errors.country);
                        $('#editCountry').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorCountry').text('');
                        $('#editCountry').css('border-color', 'black');
                    }
                    if (response.responseJSON.errors.industry) {
                        $('#editErrorIndustry').text(response.responseJSON.errors.industry);
                        $('#editIndustry').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorIndustry').text('');
                        $('#editIndustry').css('border-color', 'black');
                    }
                    if (response.responseJSON.errors.datefilter) {
                        $('#editErrorDatefilter').text(response.responseJSON.errors.datefilter);
                        $('#editDatefilter').css('border-color', 'red');
                    }
                    else {
                        $('#editErrorDatefilter').text('');
                        $('#editDatefilter').css('border-color', 'black');
                    }
                }
            }
        });
    })
</script>