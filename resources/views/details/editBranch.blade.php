<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Edit Experience Point</h4>
                    <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                </div>
                <div class="card-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="datetime-picker">
                            <form class="theme-form">
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Experience Point</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="editName" type="text" placeholder="Experience Point" value="{{ $branch->name }}">
                                </div>
                              </div>

                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Phone</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="editPhone" type="text" placeholder="Phone Number" value="{{ $branch->phone }}">

                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Email</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <input class="form-control" id="editEmail" type="text" placeholder="Email" value="{{ $branch->email }}">
                                  <small style="color: red;" id="erroremail"></small>

                                </div>
                              </div>
                            @if(auth()->user()->isAdmin())
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Merchants</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                  <select class="form-control btn-square" id="edituserId">
                                  <option value="">Select Merchant Name</option>
                                    @foreach($users as $user)
                                    @if($user->id == $branch->user_id)
                                    <option value="{{$user->id}}" selected>{{$user->name}}</option>
                                    @else
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endif
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            @endif
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">From</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8 clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                  <input class="form-control" type="text" id="editFrom" value="{{ $branch->from }}">
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">To</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8 clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                  <input class="form-control" type="text" id="editTo" value="{{ $branch->to }}">
                                  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                  </span>
                                  <small style="color: red;" id="errorTo"></small>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Active Weekdays</label>
                                <div class="col-xl-5 col-sm-7 col-lg-8">
                                    <div class="m-checkbox-inline">
                                        @php

                                        $actives = json_decode($branch->active_week_day,true);
                                        @endphp
                                        @if(in_array("Monday", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Monday')
                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-1" class="edit_days" checked value="Monday" name="days" type="checkbox">
                                            <label for="edit_inline-1">Mon</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else
                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-1" class="edit_days" value="Monday" name="days" type="checkbox">
                                            <label for="edit_inline-1">Mon</label>
                                        </div>
                                        @endif

                                        @if(in_array("Tue", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Tue')
                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-2" class="edit_days" checked value="Tue" name="days" type="checkbox">
                                            <label for="edit_inline-2">Tue</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else
                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-2" class="edit_days" value="Tue" name="days" type="checkbox">
                                            <label for="edit_inline-2">Tue</label>
                                        </div>
                                        @endif

                                        @if(in_array("Wed", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Wed')

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-3" class="edit_days" checked value="Wed" name="days" type="checkbox">
                                            <label for="edit_inline-3"> Wed</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-3" class="edit_days" value="Wed" name="days" type="checkbox">
                                            <label for="edit_inline-3"> Wed</label>
                                        </div>
                                        @endif

                                        @if(in_array("Thu", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Thu')

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-4" class="edit_days" checked value="Thu" name="days" type="checkbox">
                                            <label for="edit_inline-4">Thu</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-4" class="edit_days" value="Thu" name="days" type="checkbox">
                                            <label for="edit_inline-4">Thu</label>
                                        </div>
                                        @endif



                                        @if(in_array("Fri", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Fri')

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-5" class="edit_days" checked value="Fri" name="days" type="checkbox">
                                            <label for="edit_inline-5"> Fri</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-5" class="edit_days" value="Fri" name="days" type="checkbox">
                                            <label for="edit_inline-5"> Fri</label>
                                        </div>
                                        @endif


                                        @if(in_array("Sat", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Sat')

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-6" class="edit_days" checked value="Sat" name="days" type="checkbox">
                                            <label for="edit_inline-6"> Sat</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-6" class="edit_days" value="Sat" name="days" type="checkbox">
                                            <label for="edit_inline-6"> Sat</label>
                                        </div>
                                        @endif

                                        @if(in_array("Sun", $actives))
                                        @foreach($actives as $day)
                                        @if($day=='Sun')

                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-7" class="edit_days" checked value="Sun" name="days" type="checkbox">
                                            <label for="edit_inline-7">Sun</label>
                                        </div>
                                        @break
                                        @else
                                        @continue
                                        @endif
                                        @endforeach
                                        @else
                                        <div class="checkbox checkbox-dark">
                                            <input id="edit_inline-7" class="edit_days" value="Sun" name="days" type="checkbox">
                                            <label for="edit_inline-7">Sun</label>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                              </div>
                              <input type="hidden" id="branch_id" value="{{$branch->id}}">
                            </form>
                          </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" id="updateBranch" type="submit">Update</button>
                            <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                        </div>
                      </div>
                    </div>
</div>
        </div>
    </div>
<script src="{{asset('assets/js/time-picker/jquery-clockpicker.min.js')}}"></script>
<script src="{{asset('assets/js/time-picker/highlight.min.js')}}"></script>
<script src="{{asset('assets/js/time-picker/clockpicker.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
    <script>
        let Cancel = () => {
            location.reload()
        }
        $(document).on('click','#updateBranch',function(event){
            event.preventDefault();
            let token = '{{csrf_token()}}';
            let branchName = $('#editName').val();
            let phone=  $('#editPhone').val();
            let email=  $('#editEmail').val();
            let userId=  $('#edituserId').val();
            let from=  $('#editFrom').val();
            let to=  $('#editTo').val();
            let id = $('#branch_id').val();
            let days = [];
            $('.edit_days').each(function(idx, el) {
                if ($(el).is(':checked')) {
                    days.push($(el).val());
                }
            });
            $.ajax({
                url: '{{route('updateBranch')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    branchName,
                    phone,
                    email,
                    days,
                    from,
                    to,
                    userId,
                    id
                },
                success: function(responese) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Experience Points Successfully Updated',
                        showConfirmButton: false,
                    }).then((willDelete) => {
                        if (willDelete) {
                            location.reload();
                        }
                    })
                },
              
            });
        })
     
    </script>