@php
$createdAt = Carbon\Carbon::parse($branch->created_at);
@endphp

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-0">Experience Point Detail</h4>
                <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
            </div>
            <div class="card-body">
                <h6 class="pb-3">{{$branch->name}}</h6>
                <div class="row">
                    <div class="col-6">
                        <div style="padding-bottom: 20px;">Experience Point : <span class="font-primary first_name_1">{{$branch->name}}</span></div>
                        <div style="padding-bottom: 20px;">Email Address : <span class="font-primary email_add_1">{{$branch->email}}</span></div>
                    </div>
                    <div class="col-6">
                        <div style="padding-bottom: 20px;">Mobile No : <span class="font-primary mobile_num_1">{{$branch->phone}}</span></div>
                        <div style="padding-bottom: 20px;">Created Date : <span class="font-primary"> <span class="birth_day_1">{{$createdAt->format('d')}}</span><span class="birth_month_1 ml-1">{{$createdAt->format('M')}}</span><span class="birth_year_1 ml-1">{{$createdAt->format('Y')}}</span></span></div>
                    </div>
                </div>

                <div class="col-12 row">

                    <div class="col-12">
                        <h6 class="py-4">Subgroup List</h6>
                    </div>
                    @foreach ($branch->subBranch as $subbranch)    
                    <div class="col-4">
                        <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">{{$subbranch->name}}</h5>
                            <ul>
                                <li style="padding-bottom: 20px;">Start Date :<span class="font-primary email_add_1">{{date('d/m/Y',strtotime($subbranch->start_date))}}</span></li>
                                <li style="padding-bottom: 20px;">End Date :<span class="font-primary email_add_1">{{date('d/m/Y',strtotime($subbranch->end_date))}}</span></li>
                            </ul>
                            @if ($subbranch->status == 1)
                            <span class="badge badge-success badge-pill">Active</span>
                            @else
                            <span class="badge badge-primary badge-pill">UnActive</span>
                            @endif
                        </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

