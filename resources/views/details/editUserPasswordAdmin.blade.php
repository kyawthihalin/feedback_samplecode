<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-0">Change Password</h4>
                <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
            </div>
            <div class="card-body">
                <div class="row">
                <div class="col-md-12">
                        <div class="datetime-picker">
                        <form class="theme-form">
                            <div class="form-group col-md-6">
                                <label for="editPassword">Password</label>
                                <input type="password" class="form-control" id="editPassword" name="password" placeholder="Enter password" min="5" required>
                                <small id="editErrorPassword" style="color: red;"></small>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="editCpassword">Confirm Password</label>
                                <input type="password" class="form-control" id="editCpassword" name="cpassword" placeholder="Enter confirm password" min="5" required>
                                <small id="editErrorCPassowrd" style="color: red;"></small>
                            </div>
                        </div>
                        <input type="hidden" id="user_id" value="{{$user->id}}">
                        </form>
                        </div>
                    </div>
                    <div class="col-sm-12 text-right">
                        <button class="btn btn-primary" id="updateAdmin" type="submit">Update</button>
                        <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                    </div>
                </div>
            </div>
</div>
    </div>
</div>

<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
<script src="{{asset('assets/js/tooltip-init.js')}}"></script>
<script>
let Cancel = () => {
    location.reload()
}

$(document).on('click', '#updateAdmin',function(event) {
    event.preventDefault();
    let id = $('#user_id').val();
    let password = $('#editPassword').val();
    let cpassword = $('#editCpassword').val();
    let roles=[];
    roles = $('#editRoles').val();
    let token = '{{csrf_token()}}';
    $.ajax({
        url: '{{route('updatePasswordAdmin')}}',
        type: 'POST',
        async: false,
        headers: {
            'X-CSRF-Token': token
        },
        data: {
            id,
            password,
            cpassword
        },
        success: function(responese) {
            // $('#exampleModal1').modal('hide');
            swal({
                position: 'top-end',
                icon: 'success',
                title: 'Password Successfully Updated',
                showConfirmButton: false,
            })
            // userTable.ajax.reload();
            location.reload();
        },
        error: function(response) {
            console.log(response)
            if (response.responseJSON) {
                if (response.responseJSON.password) {
                    $('#editErrorPassword').text(response.responseJSON.password);
                    $('#editPassword').css('border-color', 'red');
                }
                else {
                    $('#editErrorPassword').text('');
                    $('#editPassword').css('border-color', 'black');
                }
                if (response.responseJSON.cpassword) {
                    $('#errorCPassowrd').text(response.responseJSON.cpassword);
                    $('#editCpassword').css('border-color', 'red');
                }
                else {
                    $('#errorCPassowrd').text('');
                    $('#editCpassword').css('border-color', 'black');
                }
            }
        }
    });
})
</script>