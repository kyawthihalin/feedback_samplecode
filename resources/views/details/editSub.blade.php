    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Edit Subgroup</h4>
                    <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="datetime-picker">
                                <form class="theme-form">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Subgroup Name</label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                            <input class="form-control" id="edit_subName" type="text" placeholder="Subgroup Name" value="{{$sub->name}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Public Name</label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                            <input class="form-control" id="edit_subPublicName" type="text" placeholder="Name" value="{{$sub->public_name}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Experience Point</label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                            <select class="form-control btn-square" id="edit_subBranch">
                                                @foreach($branches as $branch)
                                                @if($branch->id == $sub->branch_id)
                                                <option value="{{$branch->id}}" selected>{{$branch->name}}</option>
                                                @else
                                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Active Weekdays</label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                            <div class="m-checkbox-inline">
                                                @php

                                                $actives = json_decode($sub->active_weekDay,true);
                                                @endphp
                                                @if(in_array("Monday", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Monday')
                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-1" class="edit_days" checked value="Monday" name="days" type="checkbox">
                                                    <label for="edit_inline-1">Mon</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else
                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-1" class="edit_days" value="Monday" name="days" type="checkbox">
                                                    <label for="edit_inline-1">Mon</label>
                                                </div>
                                                @endif

                                                @if(in_array("Tue", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Tue')
                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-2" class="edit_days" checked value="Tue" name="days" type="checkbox">
                                                    <label for="edit_inline-2">Tue</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else
                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-2" class="edit_days" value="Tue" name="days" type="checkbox">
                                                    <label for="edit_inline-2">Tue</label>
                                                </div>
                                                @endif

                                                @if(in_array("Wed", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Wed')

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-3" class="edit_days" checked value="Wed" name="days" type="checkbox">
                                                    <label for="edit_inline-3"> Wed</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-3" class="edit_days" value="Wed" name="days" type="checkbox">
                                                    <label for="edit_inline-3"> Wed</label>
                                                </div>
                                                @endif

                                                @if(in_array("Thu", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Thu')

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-4" class="edit_days" checked value="Thu" name="days" type="checkbox">
                                                    <label for="edit_inline-4">Thu</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-4" class="edit_days" value="Thu" name="days" type="checkbox">
                                                    <label for="edit_inline-4">Thu</label>
                                                </div>
                                                @endif



                                                @if(in_array("Fri", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Fri')

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-5" class="edit_days" checked value="Fri" name="days" type="checkbox">
                                                    <label for="edit_inline-5"> Fri</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-5" class="edit_days" value="Fri" name="days" type="checkbox">
                                                    <label for="edit_inline-5"> Fri</label>
                                                </div>
                                                @endif


                                                @if(in_array("Sat", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Sat')

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-6" class="edit_days" checked value="Sat" name="days" type="checkbox">
                                                    <label for="edit_inline-6"> Sat</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-6" class="edit_days" value="Sat" name="days" type="checkbox">
                                                    <label for="edit_inline-6"> Sat</label>
                                                </div>
                                                @endif

                                                @if(in_array("Sun", $actives))
                                                @foreach($actives as $day)
                                                @if($day=='Sun')

                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-7" class="edit_days" checked value="Sun" name="days" type="checkbox">
                                                    <label for="edit_inline-7">Sun</label>
                                                </div>
                                                @break
                                                @else
                                                @continue
                                                @endif
                                                @endforeach
                                                @else
                                                <div class="checkbox checkbox-dark">
                                                    <input id="edit_inline-7" class="edit_days" value="Sun" name="days" type="checkbox">
                                                    <label for="edit_inline-7">Sun</label>
                                                </div>
                                                @endif

                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right" for="datefilter">
                                          Start Date - End Date
                                        </label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                        <input class="form-control dataRange" type="text" name="datefilter" id="editDatefilter" value="{{$start_date}}-{{$end_date}}" placeholder="Start Date - End Date" autocomplete="off" required>
                                      
                                          <small style="color: red;" id="errorDatefilter"></small>
                                        </div>
                                      </div>
                                    <input type="hidden" id="sub_id" value="{{$sub->id}}">
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" id="updateSubBranch" type="submit">Update</button>
                            <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                        </div>
                    </div>
                </div>
</div>
        </div>
    </div>

<script src="{{asset('assets/js/datepicker/daterange-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/daterange-picker/daterange-picker.custom.js')}}"></script>
    <script>
        let Cancel = () => {
            location.reload()
        }
        $(document).on('click','#updateSubBranch',function(event){
            event.preventDefault();
            let token = '{{csrf_token()}}';
            let name = $('#edit_subName').val();
            let public_name = $('#edit_subPublicName').val();
            let branch = $('#edit_subBranch').val();
            let from = $('#edit_from').val();
            let id = $('#sub_id').val();
            let datefilter = $('#editDatefilter').val();
            let days = [];
            // $('.edit_days').each(function(idx, el) {
            //     if ($(el).is(':checked')) {
            //         days.push($(el).val());
            //     }
            // });
            $.ajax({
                url: '{{route('updateSubBranch')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    public_name,
                    branch,
                    datefilter,
                    id
                },
                success: function(responese) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'New Subgroup Successfully Updated',
                        showConfirmButton: false,
                    }).then((willDelete) => {
                        if (willDelete) {
                            location.reload();
                        }
                    })
                },
              
            });
        })
     
    </script>