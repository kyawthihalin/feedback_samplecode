<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-0">Edit Customize survey</h4>
                    <div class="card-options"><a class="card-options-collapse" href="#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="datetime-picker">
                                <form class="theme-form">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Customize Survey Name</label>
                                        <div class="col-xl-5 col-sm-7 col-lg-8">
                                            <input class="form-control" id="edit_ansName" type="text" placeholder="Name" value="{{$answer->description}}">
                                        </div>
                                    </div>
                                    <input type="hidden" id="ans_id" value="{{$answer->id}}">
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-primary" id="updateAnswer" type="submit">Update</button>
                            <button class="btn btn-secondary" onclick="Cancel()" type="submit">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        let Cancel = () => {
            location.reload()
        }
        $(document).on('click','#updateAnswer',function(event){
            event.preventDefault();
            let token = '{{csrf_token()}}';
            let name = $('#edit_ansName').val();
            let id = $('#ans_id').val();
            $.ajax({
                url: '{{route('updateAnswer')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    name,
                    id
                },
                success: function(responese) {
                    swal({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Answer Successfully Updated',
                        showConfirmButton: false,
                    }).then((willDelete) => {
                        if (willDelete) {
                            location.reload();
                        }
                    })
                },
              
            });
        });
     
    </script>