<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([

    'middleware' => 'auth:api',
    'prefix' => 'auth',

], function ($router) {

    Route::post('logout', 'Api\AuthController@logout');
    Route::post('refresh', 'Api\AuthController@refresh');
    Route::get('user', 'Api\AuthController@me');
    
    Route::get('branchs', 'Api\BranchController@getBranch');
    
});

Route::post('login', 'Api\AuthController@login');
Route::post('submit-feedback', 'Api\FeedBackController@store');

Route::group([
    'middleware' => ['auth:api'],
], function ($router) {
    Route::get('get-question', 'Api\QuestionController@getQuestion');
    Route::get('dashboard', 'Api\DashboardController@index');
    Route::get('export-excel', 'Api\DashboardController@export');

    Route::get('analysis', 'Api\AnalysisController@index');
    Route::get('export-merchant-excel', 'Api\AnalysisController@export');

    Route::get('quick-view', 'Api\QuickViewController@quickView');
});
