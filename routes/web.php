<?php

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Vinkla\Hashids\Facades\Hashids;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});

Auth::routes(['register' => false, 'verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/feedback/{question}/{subbranch}', 'Admin\FeedbackController@index');
Route::post('/feedback2/{question}/{subbranch}', function(Request $request){
    $questionId = Hashids::connection()->decode(request()->segment(2))[0];
    $question = Question::find($questionId);
    $isHotel = $question->user->hasRole('Hotel');
    $emojiLevel = $request->emojiLevel;
    return view('feedback.feedback-2', compact('emojiLevel', 'isHotel'));
});

Route::get('/feedback/{question}', 'Admin\FeedbackController@index');
Route::post('/feedback2/{question}', function(Request $request){
    $questionId = Hashids::connection()->decode(request()->segment(2))[0];
    $question = Question::find($questionId);
    $isHotel = $question->user->hasRole('Hotel');
    $emojiLevel = $request->emojiLevel;
    return view('feedback.feedback-2', compact('emojiLevel', 'isHotel'));
});
Route::post('feedback', 'Admin\FeedbackController@store');
    
Route::get('change-password', 'Auth\ChangePasswordController@index');

Route::post('password-change-firt-time', 'Auth\ChangePasswordController@changePassword')->name('first.change-password');
Route::get('terms-and-condition', 'Auth\ChangePasswordController@termsAndCondition')->name('terms-and-condition');
Route::get('feature-video', 'Auth\ChangePasswordController@featureVideo')->name('feature-video');
Route::group([

    'middleware' => ['auth', 'verified', 'firstLogin'],
    'prefix' => 'admin'

], function () {
    Route::get('/', function () {
        if(auth()->user()->isAdmin()) {
            return redirect('admin/dashboard');
        } else {
            return redirect('admin/quick-view');
        }
    });


    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');
    Route::get('/quick-view', 'Admin\QuickViewController@quickView')->name('quick-view');

    Route::get('create-template','Admin\TemplateController@create');
    Route::get('template','Admin\TemplateController@index');
    Route::post('template','Admin\TemplateController@store');
    Route::get('template/getAll','Admin\TemplateController@getAll');
    Route::get('template/getSingle/{template}','Admin\TemplateController@getSingle');
    Route::get('template/getByIndustry/{industry}','Admin\TemplateController@getByIndustry');
    Route::get('template/{template}','Admin\TemplateController@edit');
    Route::post('template/{template}','Admin\TemplateController@update');
    Route::delete('template/{template}','Admin\TemplateController@destory');

    Route::get('analysis','Admin\AnalysisController@index')->name('analysis');
    Route::get('branch','Admin\BranchController@index')->name('branch');
    Route::post('branch/create','Admin\BranchController@store')->name('saveBranch');
    Route::post('branch/delete','Admin\BranchController@destroy')->name('deleteBranch');
    Route::post('branch/edit','Admin\BranchController@edit')->name('editBranch');
    Route::post('branch/update','Admin\BranchController@update')->name('updateBranch');
    Route::post('branch/detail','Admin\BranchController@detail')->name('detailBranch');
    Route::get('branch/get-all','Admin\BranchController@getAll')->name('get-all-branch');
    
    
    Route::get('sub-branch','Admin\SubBranchController@index')->name('Subbranch');
    Route::post('sub-branch/create','Admin\SubBranchController@store')->name('saveSubBranch');
    Route::post('sub-branch/update','Admin\SubBranchController@update')->name('updateSubBranch');
    Route::post('sub-branch/delete','Admin\SubBranchController@delete')->name('deleteSubBranch');
    Route::post('sub-branch/edit','Admin\SubBranchController@edit')->name('editSubBranch');
    Route::get('sub-branch/filter','Admin\SubBranchController@filter')->name('filterSubBranch');
    Route::get('sub-branch/change-status','Admin\SubBranchController@changeStatus')->name('changeSubbranchStatus');
    
    // Route::get('survey','Admin\SurveyController@index')->name('survey');
    
    Route::get('survey','Admin\SurveyController@index');
    Route::get('plansurvey','Admin\SurveyController@plan');
    Route::get('survey/getAll','Admin\SurveyController@getAll');
    Route::get('survey/create','Admin\SurveyController@create');
    Route::get('survey/{question}/edit','Admin\SurveyController@edit');
    Route::get('survey/{question}/preview','Admin\SurveyController@preview');
    Route::get('survey/{question}/qr','Admin\SurveyController@qr');
    Route::post('survey','Admin\SurveyController@store');
    Route::patch('survey/{question}','Admin\SurveyController@update')->name('updateQuestion');
    Route::delete('survey/{question}','Admin\SurveyController@destory');


    Route::get('preanswer/getAll','Admin\SurveyAnswerController@getAll')->name('getPreAnswers');
    Route::get('preanswers','Admin\SurveyAnswerController@index')->name('preAnswer');
    Route::post('preAnswers/create','Admin\SurveyAnswerController@store')->name('savePreAns');
    Route::post('preAnswers/delete','Admin\SurveyAnswerController@delete')->name('deleteAnswer');
    Route::post('preAnswers/edit','Admin\SurveyAnswerController@edit')->name('editAnswer');
    Route::post('preAnswers/update','Admin\SurveyAnswerController@update')->name('updateAnswer');


    
    Route::get('permission','Admin\PermissionController@index')->name('permission');
    Route::post('permission/create','Admin\PermissionController@store')->name('savePermission');
    Route::post('permission/delete','Admin\PermissionController@delete')->name('deletePermission');
    Route::post('permission/edit','Admin\PermissionController@edit')->name('editPermission');
    Route::post('permission/update','Admin\PermissionController@update')->name('updatePermission');
    Route::get('permission/getAll','Admin\PermissionController@getAll');
    
    
    Route::get('role','Admin\RoleController@index')->name('role');
    Route::post('role/create','Admin\RoleController@store')->name('saveRole');
    Route::post('role/delete','Admin\RoleController@delete')->name('deleteRole');
    Route::post('role/edit','Admin\RoleController@edit')->name('editRole');
    Route::post('role/update','Admin\RoleController@update')->name('updateRole');
    Route::post('role/detail','Admin\RoleController@detail')->name('detailRole');
    
    Route::get('role/getAll','Admin\RoleController@getAll');
    
    Route::get('account','Admin\UserController@index')->name('account');
    Route::get('analysis-user','Admin\UserController@analysisUser')->name('analysis-user');
    Route::post('account/create','Admin\UserController@store')->name('saveAccount');
    Route::post('account/delete','Admin\UserController@delete')->name('deleteAccount');
    Route::post('account/edit','Admin\UserController@edit')->name('editAccount');
    Route::post('account/update','Admin\UserController@update')->name('updateAccount');
    Route::get('account/detail','Admin\UserController@detail')->name('userdetail');
    Route::post('account/change-status','Admin\UserController@changeStatus')->name('changeAccStatus');
    Route::get('account/edit-password','Admin\UserController@editPassword')->name('editPassword');
    Route::post('account/update-password-by-admin','Admin\UserController@updatePasswordByAdmin')->name('updatePasswordAdmin');
    Route::get('account/add-new-analysis','Admin\UserController@addNewAnalysis')->name('add-new-analysis');
    Route::post('account/create-new-analysis','Admin\UserController@addAnalysisUser')->name('create_new_analysis');
    
    Route::get('account/getAll','Admin\UserController@getAll');
    Route::get('account/get-all-analysis','Admin\UserController@getAnalysisUser');

    Route::get('country', 'Admin\CountryController@index')->name('country');
    Route::get('country/get-all', 'Admin\CountryController@getAll')->name('country.get-all');
    Route::post('country/save', 'Admin\CountryController@store')->name('country.save');
    Route::post('country/edit', 'Admin\CountryController@edit')->name('country.edit');
    Route::post('country/update', 'Admin\CountryController@update')->name('country.update');

    Route::get('industry', 'Admin\IndustryController@index')->name('industry');
    Route::get('industry/get-all', 'Admin\IndustryController@getAll')->name('industry.get-all');
    Route::post('industry/save', 'Admin\IndustryController@store')->name('industry.save');
    Route::post('industry/edit', 'Admin\IndustryController@edit')->name('industry.edit');
    Route::post('industry/update', 'Admin\IndustryController@update')->name('industry.update');


    Route::get('get-question','Admin\AnalysisController@getQuestion')->name('get-question');

    Route::get('support','Admin\SupportController@index')->name('support');


    // Route::get('test', function () {
    //     $subranchName = 'hello';
    //     event(new App\Events\AlertBadFeedback($subranchName));
    //     return "Event has been sent!";
    // });
    
});

    // Route::get('/admin/analysis','Admin\AnalysisController@index')->name('analysis');
    // Route::get('/admin/branch','Admin\BranchController@index')->name('branch');
    // Route::post('/admin/branch/create','Admin\BranchController@store')->name('saveBranch');
    // Route::post('/admin/branch/delete','Admin\BranchController@destroy')->name('deleteBranch');
    // Route::post('/admin/branch/edit','Admin\BranchController@edit')->name('editBranch');
    // Route::post('/admin/branch/update','Admin\BranchController@update')->name('updateBranch');
    // Route::post('/admin/branch/detail','Admin\BranchController@detail')->name('detailBranch');
    
    
    // Route::get('/admin/sub-branch','Admin\SubBranchController@index')->name('Subbranch');
    // Route::post('/admin/sub-branch/create','Admin\SubBranchController@store')->name('saveSubBranch');
    // Route::post('/admin/sub-branch/update','Admin\SubBranchController@update')->name('updateSubBranch');
    // Route::post('/admin/sub-branch/delete','Admin\SubBranchController@delete')->name('deleteSubBranch');
    // Route::post('/admin/sub-branch/edit','Admin\SubBranchController@edit')->name('editSubBranch');
    
    // Route::get('/admin/survey','Admin\SurveyController@index')->name('survey');
    // Route::get('/admin/preAnswers','Admin\SurveyAnswerController@index')->name('preAnswer');
    // Route::post('/admin/preAnswers/create','Admin\SurveyAnswerController@store')->name('savePreAns');
    // Route::post('/admin/preAnswers/delete','Admin\SurveyAnswerController@delete')->name('deleteAnswer');
    // Route::post('/admin/preAnswers/edit','Admin\SurveyAnswerController@edit')->name('editAnswer');
    // Route::post('/admin/preAnswers/update','Admin\SurveyAnswerController@update')->name('updateAnswer');
    
    // Route::get('admin/permission','Admin\PermissionController@index')->name('permission');
    // Route::post('admin/permission/create','Admin\PermissionController@store')->name('savePermission');
    // Route::post('admin/permission/delete','Admin\PermissionController@delete')->name('deletePermission');
    // Route::post('admin/permission/edit','Admin\PermissionController@edit')->name('editPermission');
    // Route::post('admin/permission/update','Admin\PermissionController@update')->name('updatePermission');
    // Route::get('admin/permission/getAll','Admin\PermissionController@getAll');
    
    
    // Route::get('admin/role','Admin\RoleController@index')->name('role');
    // Route::post('admin/role/create','Admin\RoleController@store')->name('saveRole');
    // Route::post('admin/role/delete','Admin\RoleController@delete')->name('deleteRole');
    // Route::post('admin/role/edit','Admin\RoleController@edit')->name('editRole');
    // Route::post('admin/role/update','Admin\RoleController@update')->name('updateRole');
    // Route::post('admin/role/detail','Admin\RoleController@detail')->name('detailRole');
    
    // Route::get('admin/role/getAll','Admin\RoleController@getAll');
    // Route::get('admin/preanswer/getAll','Admin\SurveyController@getAll')->name('getPreAnswers');
    // Route::post('admin/preanswer/store','Admin\SurveyController@store')->name('storeQuestion');
    
    
    
    
    // Route::get('admin/account','Admin\UserController@index')->name('account');
    // Route::post('admin/account/create','Admin\UserController@store')->name('saveAccount');
    // Route::post('admin/account/delete','Admin\UserController@delete')->name('deleteAccount');
    // Route::post('admin/account/edit','Admin\UserController@edit')->name('editAccount');
    // Route::post('admin/account/update','Admin\UserController@update')->name('updateAccount');
    
    // Route::get('admin/account/getAll','Admin\UserController@getAll');


Route::get('admin/print-pdf', 'Admin\AnalysisController@printPDF');
Route::get('admin/export-merchant-excel', 'Admin\AnalysisController@export');
Route::get('admin/export-admin-excel', 'Admin\DashboardController@export');


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
