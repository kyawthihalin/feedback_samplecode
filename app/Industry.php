<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table='industry';

    protected $fillable = ['id', 'title'];

    public function templates()
    {
        return $this->hasMany(Template::class, 'industry_id');
    }
}
