<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreAnswerQuestion extends Model
{
    protected $table='pre_answer_questions';

    protected $guarded = ['id'];

    public function question()
    {
        return $this->belongsTo('App\Question', 'pre_answer_id');
    }
}
