<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertBackFeedback extends Model
{
    protected $table='alert_bad_feedback';

    public function subbranch()
    {
        return $this->belongsTo('App\SubBranch', 'sub_branch_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
