<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $table = 'templates';

    public function questions()
    { 
        return $this->hasMany('App\Question');
    }

    public function preAnswers()
    { 
        return $this->hasMany('App\TemplatePreAnswer');
    }

    public function industry()
    {
        return $this->belongsTo(Industry::class, 'industry_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
