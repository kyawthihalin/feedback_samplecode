<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table='branches';

    public function subBranch() {
        return $this->hasMany('App\SubBranch');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
