<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedBack extends Model
{
    protected $table='feedbacks';

    public function feedbackType()
    {
        return $this->belongsTo('App\FeedBackType');
    }

    public function subbranch()
    {
        return $this->belongsTo('App\SubBranch', 'sub_branch_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function preanswer()
    {
        return $this->belongsTo('App\PreAnswer');
    }
}
