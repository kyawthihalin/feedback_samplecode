<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    public function subBranch()
    {
        return $this->belongsToMany('App\SubBranch');
    }

    public function feedback()
    { 
        return $this->hasMany('App\FeedBack');
    }

    public function template()
    { 
        return $this->belongsTo('App\Template');
    }

    public function preAnswer()
    {
        return $this->hasMany('App\PreAnswerQuestion', 'question_id');
    }

    public function preAnswers()
    {
        return $this->belongsToMany('App\PreAnswer', 'pre_answer_questions');
    }

    public function mainLanguage()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }

    public function otherLanguage()
    {
        return $this->belongsTo('App\Language', 'other_language_id');
    }

    public function survey()
    {
        return $this->belongsTo('App\Survey', 'survey_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
