<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MerchantMonthExprt implements FromArray, WithStyles, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $analysis;

    public function __construct($analysis) {
        $this->analysis = $analysis;
    }

    // public function headings(): array
    // {
    //     return [
    //        ['First row', 'First row'],
    //        ['Second row', 'Second row'],
    //     ];
    // }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);
        $sheet->mergeCells('A1:AE1');
    }

    public function array(): array
    {
        return $this->analysis;
    }
}
