<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MerchantReportExport implements FromArray, WithStyles, ShouldAutoSize
{
    // /**
    // * @return \Illuminate\Support\Collection
    // */

    protected $analysis;

    public function __construct($analysis) {
        $this->analysis = $analysis;
    }

    // public function headings(): array
    // {
    //     return [
    //        ['First row', 'First row'],
    //        ['Second row', 'Second row'],
    //     ];
    // }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('1')->getFont()->setBold(true);
        $sheet->mergeCells('A1:AE1');
        $sheet->getStyle('8')->getFont()->setBold(true);
        $sheet->mergeCells('A8:C8');
        $sheet->getStyle('15')->getFont()->setBold(true);
        $sheet->mergeCells('A15:C15');
        $sheet->getStyle('22')->getFont()->setBold(true);
        $sheet->mergeCells('A22:C22');
        $sheet->getStyle('29')->getFont()->setBold(true);
        $sheet->mergeCells('A29:C29');
        $sheet->getStyle('37')->getFont()->setBold(true);
        $sheet->mergeCells('A37:C37');
        $sheet->getStyle('42')->getFont()->setBold(true);
        $sheet->mergeCells('A42:C42');
    }

    public function array(): array
    {
        return $this->analysis;
    }
}
