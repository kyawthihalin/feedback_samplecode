<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedBackType extends Model
{
    protected $table='feedback_types';
}
