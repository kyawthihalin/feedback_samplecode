<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplatePreAnswer extends Model
{
    protected $table = 'template_pre_answers';

    protected $guarded = ['id'];

    public function template()
    { 
        return $this->belongsTo('App\Template');
    }
}
