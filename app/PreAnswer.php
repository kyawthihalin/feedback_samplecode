<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreAnswer extends Model
{
    protected $table = "pre_answers";

    public function questions()
    {
        return $this->belongsToMany('App\Question', 'pre_answer_questions');
    }

}
