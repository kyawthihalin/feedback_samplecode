<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\VerifyEmail;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\VerifyEmailCustom;


class User extends Authenticatable implements JWTSubject, MustVerifyEmail {
	use Notifiable;
	use HasRoles;

	// Rest omitted for brevity

	/**
	 * Get the identifier that will be stored in the subject claim of the JWT.
	 *
	 * @return mixed
	 */
	public function getJWTIdentifier() {
		return $this->getKey();
	}

	/**
	 * Return a key value array, containing any custom claims to be added to the JWT.
	 *
	 * @return array
	 */
	public function getJWTCustomClaims() {
		return [];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function isAdmin()
	{
		return $this->hasRole('Admin');
	}

	public function isMerchant()
	{
		return $this->hasRole('Merchant');
	}

	public function isAnalysis()
	{
		return $this->hasRole('Analysis');
	}

	public function isHotel()
	{
		return $this->hasRole('Hotel');
	}

	public function country()
	{
		return $this->belongsTo('App\Country');
	}

	public function industry()
	{
		return $this->belongsTo('App\Industry');
	}

	public function branch()
	{
		return $this->hasMany('App\Branch');
	}

	public function analysisBranch()
	{
		return $this->belongsToMany('App\Branch');
	}

	public function subbranch()
	{
		return $this->hasManyThrough('App\SubBranch', 'App\Branch');
	}

	public function analysisSubbranch() 
	{
		return $this->belongsToMany('App\SubBranch');
	}

	public function templates()
	{
		return $this->hasMany('App\Template', 'user_id');
	}

	public function sendEmailVerificationNotification()
	{
		$this->notify(new VerifyEmailCustom); // my notification
	}

}
