<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSubBranch extends Model
{
    protected $table = 'question_sub_branch';
}
