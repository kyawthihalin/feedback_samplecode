<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table='surveys';

    public function questions(){
        return $this->hasMany(Question::class,'survey_id');
    }
}
