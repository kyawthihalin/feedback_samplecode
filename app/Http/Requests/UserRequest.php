<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|required_with:cpassword|same:cpassword|min:8',
            'cpassword' => 'required',
            'roles' => 'required',
            'country_id' => 'required',
            'industry_id' => 'required',
            'datefilter' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name required*',
            'email.required' => 'Email required*',
            'email.unique' => 'Email Alredy Been Taken*',
            'password.required' => 'Password required*',
            'password.same' => 'Password should match',
            'password.min' => 'Minimum password 8 characters',
            'cpassword.required' => 'Confirm Password required*',
            'roles.required' => 'Role required*',
            'country_id.required' => 'Country required*',
            'industry_id.required' => 'Industry required*',
            'datefilter.required' => 'Date Range required*',
        ];
    }
}
