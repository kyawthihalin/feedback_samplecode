<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branchName' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'userId' => 'required',
            'from' => 'required',
            'to' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'branchName.required' => 'Branch Name required*',
            'email.required' => 'Email required*',
            'phone.required' => 'Phone required*',
            'userId.required' => 'Merchant required*',
            'from.required' => 'Open Time required*',
            'to.required' => 'Close Time required*',
        ];
    }
}
