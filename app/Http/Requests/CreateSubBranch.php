<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSubBranch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'public_name' => 'required',
            'branch' => 'required',
            'datefilter' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name required*',
            'public_name.required' => 'Public Name required*',
            'branch.required' => 'Branch required*',
            'datefilter.required' => 'Date Range required*',
        ];
    }
}
