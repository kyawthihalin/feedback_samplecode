<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SurveyHotelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'survey_id' => $this->id,
            'user_id' => $this->user_id,
            'from' => $this->from,
            'to' => $this->to,
            'questions' => QuestionHotelResource::collection($this->questions),
        ];
    }
}
