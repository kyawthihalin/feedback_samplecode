<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" =>  $this->name,
            "email" =>  $this->email,
            "email_verified_at" =>  $this->email_verified_at,
            "timezone" =>  $this->timezone,
            "created_at" =>  $this->created_at,
            "updated_at" =>  $this->updated_at,
            "country_id" =>  $this->country_id,
            "industry_id" =>  $this->industry_id,
            "status" =>  $this->status,
            "activated_at" =>  $this->activated_at,
            "deactivated_at" =>  $this->deactivated_at,
            "start_date" =>  $this->start_date,
            "end_date" =>  $this->end_date,
            "parent_id" =>  $this->parent_id,
            "smily_notification" =>  $this->smily_notification,
            "weekly_report" =>  $this->weekly_report,
            "daily_report" =>  $this->daily_report,
            "is_admin" => $this->isAdmin(),
            "is_merchant" => $this->isMerchant(),
            "is_analysis" => $this->isAnalysis(),
            "is_hotel" => $this->isHotel(),
        ];
    }
}
