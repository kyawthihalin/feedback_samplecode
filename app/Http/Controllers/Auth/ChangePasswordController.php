<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{
    public function index()
    {
    	return view('auth.passwords.change-password');
    }

    public function changePassword(Request $request)
    {
    	$validate = Validator::make($request->all(), [
            'password'        => 'required|min:8',
            'confirm_password'=> 'required|min:8|same:password',
            'terms_and_condition' => 'accepted'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }

        $user = auth()->user();

        $user->password = bcrypt($request->password);
        $user->change_password = date('Y-m-d');
        $user->save();

        return redirect('feature-video');

    }

    public function termsAndCondition()
    {
    	return view('auth.terms-and-condition');
    }

    public function featureVideo()
    {
    	return view('auth.feature-video');
    }
}
