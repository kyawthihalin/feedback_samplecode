<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Branch;
use App\Question;
use App\PreAnswer;
use App\SubBranch;
use Barryvdh\DomPDF\Facade as PDF;
use App\Exports\MerchantReportExport;
use App\Exports\MerchantMonthExprt;
use App\Exports\MerchantHourExport;
use App\Exports\MerchantComparisonExport;
use App\Exports\MerchantHighlightExport;
use App\Exports\MerchantPainPointExport;
use App\Exports\MerchantWeeekExport;
use App\Exports\MerchantPieExport;
use Maatwebsite\Excel\Facades\Excel;
use App\AlertBackFeedback;

class AnalysisController extends Controller
{
    public function index(Request $request)
    {
        if(!auth()->user()->isMerchant() &&  !auth()->user()->isAnalysis()){
            return redirect('/admin');
        }

        $currentBranch = null;
        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');
        $startDateStr = date('m/d/Y');
        $endDateStr = date('m/d/Y');

        if($request->date) {
            $daterange = explode('-', $request->date);
            $startDate = date('Y-m-d', strtotime($daterange[0]));
            $endDate = date('Y-m-d', strtotime($daterange[1]));
            $startDateStr = date('m/d/Y', strtotime($daterange[0]));
            $endDateStr = date('m/d/Y', strtotime($daterange[1]));
        }

        $comments = [];

        $userid = auth()->user()->id;
        $date = $request->date;
        $branchId = $request->branch ? $request->branch : null;
        $questionId = $request->question != 'all' ? $request->question : null;

        $dayOfMonthList = [];
        $databyMonth = [];
        $databyMonth['excellent'] = [];
        $databyMonth['good'] = [];
        $databyMonth['poor'] = [];
        $databyMonth['bad'] = [];
        $databyMonthExcellent = [];
        $databyMonthGood = [];
        $databyMonthPoor = [];
        $databyMonthBad = [];

        $hourList = [];
        $databyHour = [];
        $databyHour['excellent'] = [];
        $databyHour['good'] = [];
        $databyHour['poor'] = [];
        $databyHour['bad'] = [];
        $hourlyExcellentArr = [];
        $hourlyGoodArr = [];
        $hourlyPoorArr = [];
        $hourlyBadArr = [];

        $weekDayList = [];
        $databyWeek = [];
        $databyWeek['excellent'] = [];
        $databyWeek['good'] = [];
        $databyWeek['poor'] = [];
        $databyWeek['bad'] = [];
        $weekExcellentArr = [];
        $weekGoodArr = [];
        $weekPoorArr = [];
        $weekBadArr = [];

        $percentExcellent = 0;
        $percentGood = 0;
        $percentPoor = 0;
        $percentBad = 0;

        $currentBranchName = '';
        $lastQuestionName = '';
        $totalFeedback = 0;

        $positiveAnsArr = [];
        $comparisonArr = [];
        $negativeAnsArr = [];

        if(strpos($branchId, 'all') !== false)
        {
            $branch = str_replace('all', '', $branchId);
            $branch = Branch::with('subBranch')->find($branch);
            $subBranch = $branch->subBranch->pluck('id');

            $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranch) {
                $query->whereIn('sub_branch_id', $subBranch);
            });

            $questions = Question::whereHas('subBranch', function($query) use ($subBranch) {
                $query->whereIn('sub_branch_id', $subBranch);
            })->get();

            $subBranchId = null;

        } else {
            $subBranchId = $branchId;

            $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                $query->where('sub_branch_id', $subBranchId);
            });

            $questions = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                $query->where('sub_branch_id', $subBranchId);
            })->get();
        }

        if($questionId) {
            $lastQuestion = Question::where('id', $questionId);
        }

        if(count($request->all()) <= 0) {
            $lastQuestion = Question::where('user_id', auth()->user()->id)->orderBy('id', 'desc');
        }

        $branchArr = [];
        $subbranchArr = [];

        if(auth()->user()->isAdmin()) {
            // if(auth()->user()->isAdmin() || auth()->user()->isAnalysis()) {

            $branchs = Branch::with('subBranch')->orderBy('id', 'desc')->get();
            // $questions = Question::orderBy('id', 'desc')->get();
        
        } else if(auth()->user()->isMerchant()) {

            $branchs = Branch::with('subBranch')->where('user_id', $userid)->orderBy('id', 'desc')->get();
            // $questions = Question::orderBy('id', 'desc')->get();
        } else if(auth()->user()->isAnalysis()) {
            $branchs = Branch::with('subBranch')->where('user_id', auth()->user()->parent_id)->orderBy('id', 'desc')->get();

            foreach($branchs as $branch) {
                $branchArr[] = $branch->id;
                foreach($branch->subBranch as $subbranch) {
                    $subbranchArr[] = $subbranch->id;
                }
            }
        }

        // if(count($request->all()) <= 0 || $date == 'yesterday') {
            $lastQuestion = $lastQuestion->with(['feedback' => function($query) use ($subBranchId,$request, $startDate, $endDate) {
                // $query->whereDate('feedback_date', date('Y-m-d', strtotime("-1 days")));
                if(count($request->all()) <= 0) {
                    $query->whereDate('feedback_date', date('Y-m-d'));
                } else {
                    $query->whereBetween('feedback_date', [$startDate, $endDate]);
                }
                if($subBranchId) {
                    $query->where('sub_branch_id', '=', $subBranchId);
                }
            }, 'preAnswer', 'subBranch'])->first();

            $totalDayOfMonth = cal_days_in_month(CAL_GREGORIAN,  date('m', strtotime("-1 days")),  date('Y', strtotime("-1 days")));

            $month = date('n', strtotime("-1 days"));

            $dayCount = 0;
            for ($i=1; $i <= $totalDayOfMonth ; $i++) { 
                $dayOfMonthList[$dayCount] = $i;
                $dayCount++;
            }

            if(count($request->all()) <= 0) {
                $currentDate = date('l d F Y');
                $currentMonth = date('F Y', strtotime("-1 days"));
            } else {
                $currentDate = date('d F Y', strtotime($startDate)) . ' - ' . date('d F Y', strtotime($endDate));
                $currentMonth = date('F Y', strtotime("-1 days"));
            }


        // }

        // if($date == 'last-week') {
        //     $previous_week = strtotime("-1 week +1 day");

        //     $start_week = strtotime("last sunday midnight",$previous_week);
        //     $end_week = strtotime("next saturday",$start_week);

        //     $start_week = date("Y-m-d",$start_week);
        //     $end_week = date("Y-m-d",$end_week);

        //     $currentDate = date('l d F', strtotime($start_week)) . ' - ' . date('l d F Y', strtotime($end_week));

        //     $currentMonth = date('F Y', strtotime($start_week));

        //     $totalDayOfMonth = cal_days_in_month(CAL_GREGORIAN,  date('m', strtotime($start_week)),  date('Y', strtotime($end_week)));

        //     $month = date('n', strtotime("-1 days"));

        //     $dayCount = 0;
        //     for ($i=1; $i <= $totalDayOfMonth ; $i++) { 
        //         $dayOfMonthList[$dayCount] = $i;
        //         $dayCount++;
        //     }

        //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_week, $end_week, $subBranchId) {
        //         $query->whereBetween('feedback_date', [$start_week, $end_week]);
        //         if($subBranchId) {
        //             $query->where('sub_branch_id', '=', $subBranchId);
        //         }
        //     }, 'preAnswer', 'subBranch'])->first();

        //     $lastWeekChart = true;
        // }

        // if($date == 'last-month') {

        //     $start_date = date("Y-m-d", strtotime("first day of previous month"));
        //     $end_date = date("Y-m-d", strtotime("last day of previous month"));

        //     $currentDate = date('d F Y', strtotime($start_date)) . ' - ' . date('d F Y', strtotime($end_date)); 
        //     $currentMonth = date('F Y', strtotime($start_date)); 
        //     $period = \Carbon\CarbonPeriod::create( $start_date, $end_date);

        //     $monthDayCount = 0;
        //     foreach($period as $day){
        //         $dayOfMonthList[$monthDayCount] = $day->format('j');
        //         $monthDayCount++;
        //     }

        //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_date, $end_date, $subBranchId) {
        //         $query->whereBetween('feedback_date', [$start_date, $end_date]);
        //         if($subBranchId) {
        //             $query->where('sub_branch_id', '=', $subBranchId);
        //         }
        //     }, 'preAnswer', 'subBranch'])->first();

        //     $lastMonthChart = true;
        // }
    
        
        if($lastQuestion) {
            
            $currentFeedback = $lastQuestion->feedback;
            $comments = $lastQuestion->feedback->whereNotNull('other');
            $totalFeedback = $currentFeedback ? $currentFeedback->count() : 0;
            $feedbackTypeCount = $currentFeedback->groupBy('feedback_type_id')->map->count();
    
            $currentQuestionPreanswer = $currentFeedback->groupBy('preanswer_id')->map->count();
            if($totalFeedback > 0) {
                $groupbyDayOfMonth = $currentFeedback->groupBy(function($reg){
                    return date('j',strtotime($reg->feedback_date));
                });
    
                $countDay = 0;
                foreach($groupbyDayOfMonth as $dayKey => $day) {
                    $feedback = $day->groupBy('feedback_type_id')->map->count();
        
                    foreach($feedback as $key => $value) {
                        switch ($key) {
                            case 1:
                                $databyMonth['excellent'][$dayKey] = $value;
                                break;
                            
                            case 2:
                                $databyMonth['good'][$dayKey] = $value;
                                break;
        
                            case 3:
                                $databyMonth['poor'][$dayKey] = $value;
                                break;
        
                            case 4:
                                $databyMonth['bad'][$dayKey] = $value;
                                break;
                        }
                    }
        
                    $countDay++;
                }
    
                $countd = 0;
                foreach($dayOfMonthList as $day) {
                    if(array_key_exists($day, $databyMonth['excellent'])){
                        $databyMonthExcellent[$countd] = $databyMonth['excellent'][$day];
                    } else {
                        $databyMonthExcellent[$countd] = 0;
                    }
    
                    if(array_key_exists($day, $databyMonth['good'])){
                        $databyMonthGood[$countd] = $databyMonth['good'][$day];
                    } else {
                        $databyMonthGood[$countd] = 0;
                    }
    
                    if(array_key_exists($day, $databyMonth['poor'])){
                        $databyMonthPoor[$countd] = $databyMonth['poor'][$day];
                    } else {
                        $databyMonthPoor[$countd] = 0;
                    }
    
                    if(array_key_exists($day, $databyMonth['bad'])){
                        $databyMonthBad[$countd] = $databyMonth['bad'][$day];
                    } else {
                        $databyMonthBad[$countd] = 0;
                    }
    
                    $countd++;
                }
    
                $groupByHourly = $currentFeedback->groupBy(function($reg){
                    return date('g A',strtotime($reg->feedback_time));
                });
                // dd($groupByHourly);
                $countHour = 0;
                foreach($groupByHourly as $keyHour => $hour) {
                    $hourList[$countHour] = $keyHour;
                    $feedback = $hour->groupBy('feedback_type_id')->map->count();
        
                    foreach($feedback as $key => $value) {
                        switch ($key) {
                            case 1:
                                $databyHour['excellent'][$keyHour] = $value;
                                break;
                            
                            case 2:
                                $databyHour['good'][$keyHour] = $value;
                                break;
        
                            case 3:
                                $databyHour['poor'][$keyHour] = $value;
                                break;
        
                            case 4:
                                $databyHour['bad'][$keyHour] = $value;
                                break;
                        }
                    }
                    $countHour++;
                }
                usort($hourList, function($a, $b) {
                return (strtotime($a) > strtotime($b));
                });
                $countH = 0;
                foreach($hourList as $hour) {
                    if(array_key_exists($hour, $databyHour['excellent'])){
                        $hourlyExcellentArr[$countH] = $databyHour['excellent'][$hour];
                    } else {
                        $hourlyExcellentArr[$countH] = 0;
                    }
    
                    if(array_key_exists($hour, $databyHour['good'])){
                        $hourlyGoodArr[$countH] = $databyHour['good'][$hour];
                    } else {
                        $hourlyGoodArr[$countH] = 0;
                    }
    
                    if(array_key_exists($hour, $databyHour['poor'])){
                        $hourlyPoorArr[$countH] = $databyHour['poor'][$hour];
                    } else {
                        $hourlyPoorArr[$countH] = 0;
                    }
    
                    if(array_key_exists($hour, $databyHour['bad'])){
                        $hourlyBadArr[$countH] = $databyHour['bad'][$hour];
                    } else {
                        $hourlyBadArr[$countH] = 0;
                    }
    
                    $countH++;
                }
    
                $groupByWeekDay = $currentFeedback->groupBy(function($reg){
                    return date('D',strtotime($reg->feedback_date));
                });
        
                $countWeekDay = 0;
                $weekDayList = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    
                foreach($groupByWeekDay as $keyWeekDay => $hour) {
                    $feedback = $hour->groupBy('feedback_type_id')->map->count();
        
                    foreach($feedback as $key => $value) {
                        switch ($key) {
                            case 1:
                                $databyWeek['excellent'][$keyWeekDay] = $value;
                                break;
                            
                            case 2:
                                $databyWeek['good'][$keyWeekDay] = $value;
                                break;
        
                            case 3:
                                $databyWeek['poor'][$keyWeekDay] = $value;
                                break;
        
                            case 4:
                                $databyWeek['bad'][$keyWeekDay] = $value;
                                break;
                        }
                    }
        
                    $countWeekDay++;
                }
    
                $countW = 0;
                foreach($weekDayList as $day) {
                    if(array_key_exists($day, $databyWeek['excellent'])){
                        $weekExcellentArr[$countW] = $databyWeek['excellent'][$day];
                    } else {
                        $weekExcellentArr[$countW] = 0;
                    }
    
                    if(array_key_exists($day, $databyWeek['good'])){
                        $weekGoodArr[$countW] = $databyWeek['good'][$day];
                    } else {
                        $weekGoodArr[$countW] = 0;
                    }
    
                    if(array_key_exists($day, $databyWeek['poor'])){
                        $weekPoorArr[$countW] = $databyWeek['poor'][$day];
                    } else {
                        $weekPoorArr[$countW] = 0;
                    }
    
                    if(array_key_exists($day, $databyWeek['bad'])){
                        $weekBadArr[$countW] = $databyWeek['bad'][$day];
                    } else {
                        $weekBadArr[$countW] = 0;
                    }
    
                    $countW++;
                }
    
                foreach($feedbackTypeCount as $key=>$count) {
                    switch ($key) {
                        case 1:
                            $percentExcellent = round($count / $totalFeedback * 100, 0);
                            // $positivePercent += round($count / $totalFeedback * 100, 0);
                            break;
                        
                        case 2:
                            $percentGood = round($count / $totalFeedback * 100, 0);
                            // $positivePercent += round($count / $totalFeedback * 100, 0);
                            break;
        
                        case 3:
                            $percentPoor = round($count / $totalFeedback * 100, 0);
                            // $negativePercent += round($count / $totalFeedback * 100, 0);
                            break;
        
                        case 4:
                            $percentBad = round($count / $totalFeedback * 100, 0);
                            // $negativePercent += round($count / $totalFeedback * 100, 0);
                            break;
                    }
                }
    
                $comparisonGroup = $currentFeedback->groupBy('sub_branch_id');
    
                foreach($comparisonGroup as $subId => $comparisonCollection){
                    $subName = SubBranch::find($subId);
                    $subName = $subName->name;
    
                    $comparisonGroupByType = $comparisonCollection->groupBy('feedback_type_id')->map->count();
                    $comparisonArr[$subName]['total'] = 0;
                    foreach($comparisonGroupByType as $typeKey => $comVal) {
    
                        switch ($typeKey) {
                            case 1:
                                $comparisonArr[$subName]['excellent'] = $comVal / $totalFeedback * 100;
                                $comparisonArr[$subName]['total'] += $comVal;
                            break;
    
                            case 2:
                                $comparisonArr[$subName]['good'] = $comVal / $totalFeedback * 100;
                                $comparisonArr[$subName]['total'] += $comVal;
                            break;
    
                            case 3:
                                $comparisonArr[$subName]['poor'] = $comVal / $totalFeedback * 100;
                                $comparisonArr[$subName]['total'] += $comVal;
                            break;
    
                            case 4:
                                $comparisonArr[$subName]['bad'] = $comVal / $totalFeedback * 100;
                                $comparisonArr[$subName]['total'] += $comVal;
                            break;
                        }   
    
                    }
                }
    
                $positiveAnswer = $currentFeedback->whereIn('feedback_type_id', [1, 2]);
                $positiveAnswerCount = $positiveAnswer->count();
        
                $happyIndex = round($positiveAnswerCount / $totalFeedback * 100);
        
                if($positiveAnswer) {
        
                    $groupPositiveAns = $positiveAnswer->groupBy('preanswer_id');
        
                    foreach($groupPositiveAns as $key=>$positiveAns) {
                        $positiveAnsGroup = $positiveAns->groupBy('feedback_type_id')->map->count();    
                        $preanswer = PreAnswer::find($key);
        
                        if($preanswer){
                            $positiveAnsArr[$preanswer->description]['total'] = 0;
                            foreach ($positiveAnsGroup as $key => $value) {
                                switch ($key) {
                                    case 1:
                                        $positiveAnsArr[$preanswer->description]['excellent'] = $value / $totalFeedback * 100;
                                        $positiveAnsArr[$preanswer->description]['total'] += $value;
                                        break;
                                    
                                    case 2:
                                        $positiveAnsArr[$preanswer->description]['good'] = $value / $totalFeedback * 100;
                                        $positiveAnsArr[$preanswer->description]['total'] += $value;
                                        break;
                                }
                            }
                        }
        
                    }
                }
        
                $negativeAnswer = $currentFeedback->whereIn('feedback_type_id', [3, 4]);
                if($negativeAnswer) {
                    $groupNegativeAns = $negativeAnswer->groupBy('preanswer_id');
        
                    foreach($groupNegativeAns as $key=>$negativeAns) {
                        $negativeAnsGroup = $negativeAns->groupBy('feedback_type_id')->map->count();    
                        $preanswer = PreAnswer::find($key);
        
                        if($preanswer) {
                            $negativeAnsArr[$preanswer->description]['total'] = 0;
                            foreach ($negativeAnsGroup as $key => $value) {
                                switch ($key) {
                                    case 3:
                                        $negativeAnsArr[$preanswer->description]['poor'] = $value / $totalFeedback * 100;
                                        $negativeAnsArr[$preanswer->description]['total'] += $value;
                                        break;
                                    
                                    case 4:
                                        $negativeAnsArr[$preanswer->description]['bad'] = $value / $totalFeedback * 100;
                                        $negativeAnsArr[$preanswer->description]['total'] += $value;
                                        break;
                                }
                            }
                        }
        
                    }
                }
    
            }
    
            $currentSubBranch = $lastQuestion->subBranch->first();
            if(strpos($branchId, 'all') !== false) {
                $currentBranch = $currentSubBranch->branch;
            } else {
                $currentBranch = $currentSubBranch;
            }
        }



        // $branchs = Branch::with('subBranch')->get();
        // $questions = Question::orderBy('id', 'desc')->get();

        $alerts = AlertBackFeedback::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->take(10)->get();

        return view('Admin.analysis', compact('branchs', 'currentMonth','branchId', 'questions', 'questionId', 'date','percentExcellent', 'percentGood', 'percentPoor', 'percentBad', 'totalFeedback', 'dayOfMonthList', 'databyMonthExcellent', 'databyMonthGood', 'databyMonthPoor', 'databyMonthBad', 'hourList', 'hourlyExcellentArr', 'hourlyGoodArr', 'hourlyPoorArr', 'hourlyBadArr', 'weekDayList', 'weekExcellentArr', 'weekGoodArr', 'weekPoorArr', 'weekBadArr', 'positiveAnsArr', 'negativeAnsArr', 'comparisonArr', 'currentBranch', 'lastQuestion', 'currentDate', 'lastQuestionName', 'currentBranchName', 'alerts', 'startDateStr', 'endDateStr', 'comments', 'subbranchArr', 'branchArr'));
    }

    public function getQuestion(Request $request) {
        $branchId = $request->branch;
        if(strpos($branchId, 'all') !== false)
        {
            $branch = str_replace('all', '', $branchId);
            $branch = Branch::with('subBranch')->find($branch);
            $subBranch = $branch->subBranch->pluck('id');

            $questions = Question::whereHas('subBranch', function($query) use ($subBranch) {
                $query->whereIn('sub_branch_id', $subBranch);
            })->get();

            $subBranchId = null;

        } else {
            $subBranchId = $branchId;

            $questions = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                $query->where('sub_branch_id', $subBranchId);
            })->get();
        }

        $res = [];
        $count = 0;

        foreach ($questions as $question) {
            $res[$count]['id'] = $question->id;
            $res[$count]['name'] = $question->question_text;

            $count++;
        }

        return response()->json([
            'data' => $res,
            'code' => 200,
            'msg'  => 'Success'
        ]);
    }

    public function export(Request $request) {

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');

        if($request->date) {
            $daterange = explode('-', $request->date);
            $startDate = date('Y-m-d', strtotime($daterange[0]));
            $endDate = date('Y-m-d', strtotime($daterange[1]));
        }

        $userid = auth()->user()->id;
        $date = $request->date;
        $branchId = $request->branch ? $request->branch : null;
        $questionId = $request->question != 'all' ? $request->question : null;

        $dayOfMonthList = [];
        $databyMonth = [];
        $subBranchArr = [];
        $databyMonth['excellent'] = [];
        $databyMonth['good'] = [];
        $databyMonth['poor'] = [];
        $databyMonth['bad'] = [];
        $databyMonthExcellent = [];
        $databyMonthGood = [];
        $databyMonthPoor = [];
        $databyMonthBad = [];

        $hourList = [];
        $databyHour = [];
        $databyHour['excellent'] = [];
        $databyHour['good'] = [];
        $databyHour['poor'] = [];
        $databyHour['bad'] = [];
        $hourlyExcellentArr = [];
        $hourlyGoodArr = [];
        $hourlyPoorArr = [];
        $hourlyBadArr = [];

        $weekDayList = [];
        $databyWeek = [];
        $databyWeek['excellent'] = [];
        $databyWeek['good'] = [];
        $databyWeek['poor'] = [];
        $databyWeek['bad'] = [];
        $weekExcellentArr = [];
        $weekGoodArr = [];
        $weekPoorArr = [];
        $weekBadArr = [];

        $comparisonList = [];
        $dataCompirison['excellent'] = [];
        $dataCompirison['good'] = [];
        $dataCompirison['poor'] = [];
        $dataCompirison['bad'] = [];
        $compirisonExcellentArr = [];
        $compirisonGoodArr = [];
        $compirisonPoorArr = [];
        $compirisonBadArr = [];
        $comparisonTotal = [];

        $percentExcellent = 0;
        $percentGood = 0;
        $percentPoor = 0;
        $percentBad = 0;

        $positiveAnsArr = [];
        $datapositiveAnsArr['excellent'] = [];
        $datapositiveAnsArr['good'] = [];
        $positiveAnsExcellentArr = [];
        $positiveAnsGoodArr = [];

        $negativeAnsArr = [];
        $datanegativeAnsArr['poor'] = [];
        $datanegativeAnsArr['bad'] = [];
        $negativeAnsPoorArr = [];
        $negativeAnsBadArr = [];

        if(strpos($branchId, 'all') !== false)
        {
            $branch = str_replace('all', '', $branchId);
            $branch = Branch::with('subBranch')->find($branch);
            $subBranch = $branch->subBranch->pluck('id');

            $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranch) {
                $query->whereIn('sub_branch_id', $subBranch);
            });

            $questions = Question::whereHas('subBranch', function($query) use ($subBranch) {
                $query->whereIn('sub_branch_id', $subBranch);
            })->get();

            $subBranchId = null;

        } else {
            $subBranchId = $branchId;

            $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                $query->where('sub_branch_id', $subBranchId);
            });

            $questions = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                $query->where('sub_branch_id', $subBranchId);
            })->get();
        }

        if($questionId) {
            $lastQuestion = Question::where('id', $questionId);
        }

        if(count($request->all()) <= 0 || $request->chart) {
            $lastQuestion = Question::where('user_id', auth()->user()->id)->orderBy('id', 'desc');
        }

        if(auth()->user()->isAdmin() || auth()->user()->isAnalysis()) {

            $branchs = Branch::with('subBranch')->orderBy('id', 'desc')->get();
            // $questions = Question::orderBy('id', 'desc')->get();
        
        } else {

            $branchs = Branch::with('subBranch')->where('user_id', $userid)->orderBy('id', 'desc')->get();
            // $questions = Question::orderBy('id', 'desc')->get();
        }

        // if((count($request->all()) <= 0 || $date == 'yesterday') || $request->chart) {

            $lastQuestion = $lastQuestion->with(['feedback' => function($query) use ($subBranchId,$request, $startDate, $endDate) {

                if(count($request->all()) <= 0) {
                    $query->whereDate('feedback_date', date('Y-m-d'));
                } else {
                    $query->whereBetween('feedback_date', [$startDate, $endDate]);
                }

                if($subBranchId) {
                    $query->where('sub_branch_id', '=', $subBranchId);
                }
            }, 'preAnswer', 'subBranch'])->first();

            $totalDayOfMonth = cal_days_in_month(CAL_GREGORIAN,  date('m', strtotime("-1 days")),  date('Y', strtotime("-1 days")));

            $month = date('n', strtotime("-1 days"));

            $dayCount = 0;
            for ($i=1; $i <= $totalDayOfMonth ; $i++) { 
                $dayOfMonthList[$dayCount] = $i . '-' . date('m', strtotime("-1 days")) . '-' . date('Y', strtotime("-1 days"));
                $dayCount++;
            }

            if(count($request->all()) <= 0) {
                $currentDate = date('l d F Y');
                $currentMonth = date('F Y', strtotime("-1 days"));
            } else {
                $currentDate = date('d F Y', strtotime($startDate)) . ' - ' . date('d F Y', strtotime($endDate)); 
            }

        // }

        // if($date == 'last-week') {
        //     $previous_week = strtotime("-1 week +1 day");

        //     $start_week = strtotime("last sunday midnight",$previous_week);
        //     $end_week = strtotime("next saturday",$start_week);

        //     $start_week = date("Y-m-d",$start_week);
        //     $end_week = date("Y-m-d",$end_week);

        //     $currentDate = date('l d F', strtotime($start_week)) . ' - ' . date('l d F Y', strtotime($end_week));

        //     $currentMonth = date('F Y', strtotime($start_week)) . ' - ' . date('l d F Y', strtotime($end_week));

        //     $totalDayOfMonth = cal_days_in_month(CAL_GREGORIAN,  date('m', strtotime($start_week)),  date('Y', strtotime($end_week)));

        //     $month = date('n', strtotime("-1 days"));

        //     $dayCount = 0;
        //     for ($i=1; $i <= $totalDayOfMonth ; $i++) { 
        //         $dayOfMonthList[$dayCount] = $i . '-' . date('m', strtotime("-1 days")) . '-' . date('Y', strtotime("-1 days"));
        //         $dayCount++;
        //     }

        //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_week, $end_week, $subBranchId) {
        //         $query->whereBetween('feedback_date', [$start_week, $end_week]);
        //         if($subBranchId) {
        //             $query->where('sub_branch_id', '=', $subBranchId);
        //         }
        //     }, 'preAnswer', 'subBranch'])->first();

        //     $lastWeekChart = true;
        // }

        // if($date == 'last-month') {

        //     $start_date = date("Y-m-d", strtotime("first day of previous month"));
        //     $end_date = date("Y-m-d", strtotime("last day of previous month"));

        //     $currentDate = date('d F Y', strtotime($start_date)) . ' - ' . date('d F Y', strtotime($end_date)); 
        //     $currentMonth = date('F Y', strtotime($start_date)) . ' - ' . date('d F Y', strtotime($end_date)); 
        //     $period = \Carbon\CarbonPeriod::create( $start_date, $end_date);

        //     $monthDayCount = 0;
        //     foreach($period as $day){
        //         $dayOfMonthList[$monthDayCount] = $day->format('j-m-Y');
        //         $monthDayCount++;
        //     }

        //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_date, $end_date, $subBranchId) {
        //         $query->whereBetween('feedback_date', [$start_date, $end_date]);
        //         if($subBranchId) {
        //             $query->where('sub_branch_id', '=', $subBranchId);
        //         }
        //     }, 'preAnswer', 'subBranch'])->first();

        //     $lastMonthChart = true;
        // }

        $currentFeedback = $lastQuestion->feedback;
        $totalFeedback = $currentFeedback ? $currentFeedback->count() : 0;
        $feedbackTypeCount = $currentFeedback->groupBy('feedback_type_id')->map->count();

        $currentQuestionPreanswer = $currentFeedback->groupBy('preanswer_id')->map->count();

        if($totalFeedback > 0) {
            $groupbyDayOfMonth = $currentFeedback->groupBy(function($reg){
                return date('j-m-Y',strtotime($reg->feedback_date));
            });

            $countDay = 0;
            foreach($groupbyDayOfMonth as $dayKey => $day) {
                $feedback = $day->groupBy('feedback_type_id')->map->count();
    
                foreach($feedback as $key => $value) {
                    switch ($key) {
                        case 1:
                            $databyMonth['excellent'][$dayKey] = $value;
                            break;
                        
                        case 2:
                            $databyMonth['good'][$dayKey] = $value;
                            break;
    
                        case 3:
                            $databyMonth['poor'][$dayKey] = $value;
                            break;
    
                        case 4:
                            $databyMonth['bad'][$dayKey] = $value;
                            break;
                    }
                }
    
                $countDay++;
            }

            $countd = 0;
            foreach($dayOfMonthList as $day) {
                if(array_key_exists($day, $databyMonth['excellent'])){
                    $databyMonthExcellent[$countd] = $databyMonth['excellent'][$day];
                } else {
                    $databyMonthExcellent[$countd] = 0;
                }

                if(array_key_exists($day, $databyMonth['good'])){
                    $databyMonthGood[$countd] = $databyMonth['good'][$day];
                } else {
                    $databyMonthGood[$countd] = 0;
                }

                if(array_key_exists($day, $databyMonth['poor'])){
                    $databyMonthPoor[$countd] = $databyMonth['poor'][$day];
                } else {
                    $databyMonthPoor[$countd] = 0;
                }

                if(array_key_exists($day, $databyMonth['bad'])){
                    $databyMonthBad[$countd] = $databyMonth['bad'][$day];
                } else {
                    $databyMonthBad[$countd] = 0;
                }

                $countd++;
            }

            $groupByHourly = $currentFeedback->groupBy(function($reg){
                return date('g A',strtotime($reg->feedback_time));
            });
            // dd($groupByHourly);
            $countHour = 0;
            foreach($groupByHourly as $keyHour => $hour) {
                $hourList[$countHour] = $keyHour;
                $feedback = $hour->groupBy('feedback_type_id')->map->count();
    
                foreach($feedback as $key => $value) {
                    switch ($key) {
                        case 1:
                            $databyHour['excellent'][$keyHour] = $value;
                            break;
                        
                        case 2:
                            $databyHour['good'][$keyHour] = $value;
                            break;
    
                        case 3:
                            $databyHour['poor'][$keyHour] = $value;
                            break;
    
                        case 4:
                            $databyHour['bad'][$keyHour] = $value;
                            break;
                    }
                }
                $countHour++;
            }
            sort($hourList);

            $countH = 0;
            foreach($hourList as $hour) {
                if(array_key_exists($hour, $databyHour['excellent'])){
                    $hourlyExcellentArr[$countH] = $databyHour['excellent'][$hour];
                } else {
                    $hourlyExcellentArr[$countH] = 0;
                }

                if(array_key_exists($hour, $databyHour['good'])){
                    $hourlyGoodArr[$countH] = $databyHour['good'][$hour];
                } else {
                    $hourlyGoodArr[$countH] = 0;
                }

                if(array_key_exists($hour, $databyHour['poor'])){
                    $hourlyPoorArr[$countH] = $databyHour['poor'][$hour];
                } else {
                    $hourlyPoorArr[$countH] = 0;
                }

                if(array_key_exists($hour, $databyHour['bad'])){
                    $hourlyBadArr[$countH] = $databyHour['bad'][$hour];
                } else {
                    $hourlyBadArr[$countH] = 0;
                }

                $countH++;
            }

            $groupByWeekDay = $currentFeedback->groupBy(function($reg){
                return date('D',strtotime($reg->feedback_date));
            });
    
            $countWeekDay = 0;
            $weekDayList = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

            foreach($groupByWeekDay as $keyWeekDay => $hour) {
                $feedback = $hour->groupBy('feedback_type_id')->map->count();
    
                foreach($feedback as $key => $value) {
                    switch ($key) {
                        case 1:
                            $databyWeek['excellent'][$keyWeekDay] = $value;
                            break;
                        
                        case 2:
                            $databyWeek['good'][$keyWeekDay] = $value;
                            break;
    
                        case 3:
                            $databyWeek['poor'][$keyWeekDay] = $value;
                            break;
    
                        case 4:
                            $databyWeek['bad'][$keyWeekDay] = $value;
                            break;
                    }
                }
    
                $countWeekDay++;
            }

            $countW = 0;
            foreach($weekDayList as $day) {
                if(array_key_exists($day, $databyWeek['excellent'])){
                    $weekExcellentArr[$countW] = $databyWeek['excellent'][$day];
                } else {
                    $weekExcellentArr[$countW] = 0;
                }

                if(array_key_exists($day, $databyWeek['good'])){
                    $weekGoodArr[$countW] = $databyWeek['good'][$day];
                } else {
                    $weekGoodArr[$countW] = 0;
                }

                if(array_key_exists($day, $databyWeek['poor'])){
                    $weekPoorArr[$countW] = $databyWeek['poor'][$day];
                } else {
                    $weekPoorArr[$countW] = 0;
                }

                if(array_key_exists($day, $databyWeek['bad'])){
                    $weekBadArr[$countW] = $databyWeek['bad'][$day];
                } else {
                    $weekBadArr[$countW] = 0;
                }

                $countW++;
            }

            foreach($feedbackTypeCount as $key=>$count) {
                switch ($key) {
                    case 1:
                        $percentExcellent = round($count / $totalFeedback * 100, 0);
                        // $positivePercent += round($count / $totalFeedback * 100, 0);
                        break;
                    
                    case 2:
                        $percentGood = round($count / $totalFeedback * 100, 0);
                        // $positivePercent += round($count / $totalFeedback * 100, 0);
                        break;
    
                    case 3:
                        $percentPoor = round($count / $totalFeedback * 100, 0);
                        // $negativePercent += round($count / $totalFeedback * 100, 0);
                        break;
    
                    case 4:
                        $percentBad = round($count / $totalFeedback * 100, 0);
                        // $negativePercent += round($count / $totalFeedback * 100, 0);
                        break;
                }
            }

            $comparisonGroup = $currentFeedback->groupBy('sub_branch_id');
            // $countSub = 0;

            $subBranchArr = [];
            foreach($comparisonGroup as $subId => $comparisonCollection){
                $subB = SubBranch::find($subId);
                $subName = $subB->name;
                
                $subBranchArr[$subB->id] = $subB->name; 


                $comparisonGroupByType = $comparisonCollection->groupBy('feedback_type_id')->map->count();
                $comparisonTotal[$subName] = 0;
                foreach($comparisonGroupByType as $typeKey => $comVal) {

                    switch ($typeKey) {
                        case 1:
                            $dataCompirison['excellent'][$subName] = $comVal / $totalFeedback * 100 . "%";
                            $comparisonTotal[$subName] += $comVal;
                        break;

                        case 2:
                            $dataCompirison['good'][$subName] = $comVal / $totalFeedback * 100 . "%";
                            $comparisonTotal[$subName] += $comVal;
                        break;

                        case 3:
                            $dataCompirison['poor'][$subName] = $comVal / $totalFeedback * 100 . "%";
                            $comparisonTotal[$subName] += $comVal;
                        break;

                        case 4:
                            $dataCompirison['bad'][$subName] = $comVal / $totalFeedback * 100 . "%";
                            $comparisonTotal[$subName] += $comVal;
                        break;
                    }   

                }
            }

            $countCom = 0;
            foreach($subBranchArr as $subBranch) {
                if(array_key_exists($subBranch, $dataCompirison['excellent'])){
                    $compirisonExcellentArr[$countCom] = $dataCompirison['excellent'][$subBranch];
                } else {
                    $compirisonExcellentArr[$countCom] = 0;
                }

                if(array_key_exists($subBranch, $dataCompirison['good'])){
                    $compirisonGoodArr[$countCom] = $dataCompirison['good'][$subBranch];
                } else {
                    $compirisonGoodArr[$countCom] = 0;
                }

                if(array_key_exists($subBranch, $dataCompirison['poor'])){
                    $compirisonPoorArr[$countCom] = $dataCompirison['poor'][$subBranch];
                } else {
                    $compirisonPoorArr[$countCom] = 0;
                }

                if(array_key_exists($subBranch, $dataCompirison['bad'])){
                    $compirisonBadArr[$countCom] = $dataCompirison['bad'][$subBranch];
                } else {
                    $compirisonBadArr[$countCom] = 0;
                }

                $countCom++;
            }
            

            $positiveAnswer = $currentFeedback->whereIn('feedback_type_id', [1, 2]);
            $positiveAnswerCount = $positiveAnswer->count();
    
            $happyIndex = round($positiveAnswerCount / $totalFeedback * 100);
    
            if($positiveAnswer) {
    
                $groupPositiveAns = $positiveAnswer->groupBy('preanswer_id');
    
                foreach($groupPositiveAns as $key=>$positiveAns) {
                    $positiveAnsGroup = $positiveAns->groupBy('feedback_type_id')->map->count();    
                    $preanswer = PreAnswer::find($key);
                    
                    $positiveAnsArr[$preanswer->id] = $preanswer->description;
                    if($preanswer){
                        foreach ($positiveAnsGroup as $key => $value) {
                            switch ($key) {
                                case 1:
                                    $datapositiveAnsArr['excellent'][$preanswer->description] = $value / $totalFeedback * 100 . "%";
                                    
                                    break;
                                
                                case 2:
                                    $datapositiveAnsArr['good'] [$preanswer->description] = $value / $totalFeedback * 100 . "%";
                                    
                                    break;
                            }
                        }
                    }
    
                }
                $countPositive = 0;
                foreach($positiveAnsArr as $positiveAns) {

                    if(array_key_exists($positiveAns, $datapositiveAnsArr['excellent'])){
                        
                        $positiveAnsExcellentArr[$countPositive] = $datapositiveAnsArr['excellent'][$positiveAns];
                    } else {

                        $positiveAnsExcellentArr[$countPositive] = 0;
                    }
    
                    if(array_key_exists($positiveAns, $datapositiveAnsArr['good'])){
                        $positiveAnsGoodArr[$countPositive] = $datapositiveAnsArr['good'][$positiveAns];
                    } else {
                        $positiveAnsGoodArr[$countPositive] = 0;
                    }
    
                    $countPositive++;
                }
            }

    
            $negativeAnswer = $currentFeedback->whereIn('feedback_type_id', [3, 4]);
            if($negativeAnswer) {
                $groupNegativeAns = $negativeAnswer->groupBy('preanswer_id');
    
                foreach($groupNegativeAns as $key=>$negativeAns) {
                    $negativeAnsGroup = $negativeAns->groupBy('feedback_type_id')->map->count();    
                    $preanswer = PreAnswer::find($key);
                    $negativeAnsArr[$preanswer->id] = $preanswer->description;
                    if($preanswer) {
                        
                        foreach ($negativeAnsGroup as $key => $value) {
                            switch ($key) {
                                case 3:
                                    $datanegativeAnsArr['poor'][$preanswer->description] = $value / $totalFeedback * 100 . "%";
                                    
                                    break;
                                
                                case 4:
                                    $datanegativeAnsArr['bad'][$preanswer->description] = $value / $totalFeedback * 100 . "%";
                                    
                                    break;
                            }
                        }
                    }
    
                }

                $countNegative = 0;
                foreach($negativeAnsArr as $negativeAns) {
                    if(array_key_exists($negativeAns, $datanegativeAnsArr['poor'])){
                        $negativeAnsPoorArr[$countNegative] = $datanegativeAnsArr['poor'][$negativeAns];
                    } else {
                        $negativeAnsPoorArr[$countNegative] = 0;
                    }
    
                    if(array_key_exists($negativeAns, $datanegativeAnsArr['bad'])){
                        $negativeAnsBadArr[$countNegative] = $datanegativeAnsArr['bad'][$negativeAns];
                    } else {
                        $negativeAnsBadArr[$countNegative] = 0;
                    }
    
                    $countNegative++;
                }
            }

        }

        // $pdf = PDF::loadView('pdf.analysis-index', compact('branchs', 'currentMonth','branchId', 'questions', 'questionId', 'date','percentExcellent', 'percentGood', 'percentPoor', 'percentBad', 'totalFeedback', 'dayOfMonthList', 'databyMonthExcellent', 'databyMonthGood', 'databyMonthPoor', 'databyMonthBad', 'hourList', 'hourlyExcellentArr', 'hourlyGoodArr', 'hourlyPoorArr', 'hourlyBadArr', 'weekDayList', 'weekExcellentArr', 'weekGoodArr', 'weekPoorArr', 'weekBadArr', 'positiveAnsArr', 'negativeAnsArr', 'comparisonAnsArr'));
        $currentSubBranch = $lastQuestion->subBranch->first();
        if(strpos($branchId, 'all') !== false) {
            $currentBranch = $currentSubBranch->branch;
        } else {
            $currentBranch = $currentSubBranch;
        }

        array_unshift($dayOfMonthList, 'Day Of Month');
        array_unshift($databyMonthExcellent, 'Excellent');
        array_unshift($databyMonthGood, 'Good');
        array_unshift($databyMonthPoor, 'Poor');
        array_unshift($databyMonthBad, 'Bad');

        array_unshift($hourList, 'Hours');
        array_unshift($hourlyExcellentArr, 'Excellent');
        array_unshift($hourlyGoodArr, 'Good');
        array_unshift($hourlyPoorArr, 'Poor');
        array_unshift($hourlyBadArr, 'Bad');

        array_unshift($weekDayList, 'Day of week');
        array_unshift($weekExcellentArr, 'Excellent');
        array_unshift($weekGoodArr, 'Good');
        array_unshift($weekPoorArr, 'Poor');
        array_unshift($weekBadArr, 'Bad');

        array_unshift($subBranchArr, 'Subbranch Name');
        array_unshift($compirisonExcellentArr, 'Excellent');
        array_unshift($compirisonGoodArr, 'Good');
        array_unshift($compirisonPoorArr, 'Poor');
        array_unshift($compirisonBadArr, 'Bad');
        array_unshift($comparisonTotal, 'Total');

        array_unshift($positiveAnsArr, 'Positive Answer');
        array_unshift($positiveAnsExcellentArr, 'Excellent');
        array_unshift($positiveAnsGoodArr, 'Good');

        array_unshift($negativeAnsArr, 'Negative Answer');
        array_unshift($negativeAnsPoorArr, 'Poor');
        array_unshift($negativeAnsBadArr, 'Bad');



        $totalResponse = ['Total Feedback', $totalFeedback];
        $percentExcellent = ['Excellent', $percentExcellent . '%'];
        $percentGood = ['Good', $percentGood . '%'];
        $percentPoor = ['Poor', $percentPoor . '%'];
        $percentBad = ['Bad', $percentBad . '%'];

        if($request->chart) {
            if($request->chart == 'month') {
                $export = new MerchantMonthExprt([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    $dayOfMonthList,
                    $databyMonthExcellent,
                    $databyMonthGood,
                    $databyMonthPoor,
                    $databyMonthBad,
                ]);
            }

            if($request->chart == 'hour') {
                $export = new MerchantHourExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Hourly Distribution'],
                    $hourList,
                    $hourlyExcellentArr,
                    $hourlyGoodArr,
                    $hourlyPoorArr,
                    $hourlyBadArr,
                ]);
            }

            if($request->chart == 'week') {
                $export = new MerchantWeeekExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Weekly Distribution'],
                    $weekDayList,
                    $weekExcellentArr,
                    $weekGoodArr,
                    $weekPoorArr,
                    $weekBadArr,
                ]);
            }

            if($request->chart == 'pie') {
                $export = new MerchantPieExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Total'],
                    $totalResponse,
                    $percentExcellent,
                    $percentGood,
                    $percentPoor,
                    $percentBad,
                ]);
            }

            if($request->chart == 'comprison') {
                $export = new MerchantComparisonExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Comparison'],
                    $subBranchArr,
                    $compirisonExcellentArr,
                    $compirisonGoodArr,
                    $compirisonPoorArr,
                    $compirisonBadArr,
                    $comparisonTotal,
                ]);
            }

            if($request->chart == 'highlight') {
                $export = new MerchantHighlightExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Highlights'],
                    $positiveAnsArr,
                    $positiveAnsExcellentArr,
                    $positiveAnsGoodArr,
                ]);
            }

            if($request->chart == 'pain-point') {
                $export = new MerchantPainPointExport([
                    [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                    ['Pain Points'],
                    $negativeAnsArr,
                    $negativeAnsPoorArr,
                    $negativeAnsBadArr
                ]);
            }

        } else {

            $export = new MerchantReportExport([
                [$currentBranch->name . ', ' . $lastQuestion->question_text . ', ' . $currentDate],
                $dayOfMonthList,
                $databyMonthExcellent,
                $databyMonthGood,
                $databyMonthPoor,
                $databyMonthBad,
                [''],
                ['Hourly Distribution'],
                $hourList,
                $hourlyExcellentArr,
                $hourlyGoodArr,
                $hourlyPoorArr,
                $hourlyBadArr,
                [''],
                ['Weekly Distribution'],
                $weekDayList,
                $weekExcellentArr,
                $weekGoodArr,
                $weekPoorArr,
                $weekBadArr,
                [''],
                ['Total'],
                $totalResponse,
                $percentExcellent,
                $percentGood,
                $percentPoor,
                $percentBad,
                [''],
                ['Comparison'],
                $subBranchArr,
                $compirisonExcellentArr,
                $compirisonGoodArr,
                $compirisonPoorArr,
                $compirisonBadArr,
                $comparisonTotal,
                [''],
                ['Highlights'],
                $positiveAnsArr,
                $positiveAnsExcellentArr,
                $positiveAnsGoodArr,
                [''],
                ['Pain Points'],
                $negativeAnsArr,
                $negativeAnsPoorArr,
                $negativeAnsBadArr
            ]);
        }


        return Excel::download($export, 'AnalysisReport('. $currentDate .').xlsx');
    }
}
