<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index(){
        $permissions = Permission::all();
        return view('backend.permission.index',compact('permissions'));
    }
    public function getAll(){
        $permissions = Permission::query();
        return DataTables::of($permissions)
                ->addColumn('action',function($permission){
                    return '<a href="#" class="badge badge-danger badge-pill text-white delete" data-id="'.$permission->id.'">Delete</a>
                            <a href="#exampleModal1" class="badge badge-warning badge-pill text-black edit" data-toggle="modal" data-id="'.$permission->id.'">Edit</a>';
                })
                ->make(true);
    }
    public function store(PermissionRequest $request){
       Permission::create(['name' => $request->name ]);
       return response()->json([
           'status' => 'success'
       ]);
    }
    public function delete(Request $request){
        $permission = Permission::find($request->id);
        if($permission->delete()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
    public function edit(Request $request){
        $permission = Permission::find($request->id);
        return response()->json([
            'permission' => $permission->name,
            'id' => $permission->id
        ]);
    }
    public function update(PermissionRequest $request){
        $permission = Permission::find($request->id);
        $permission->name = $request->name;
        if($permission->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
