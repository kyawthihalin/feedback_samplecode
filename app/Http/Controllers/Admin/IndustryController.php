<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Industry;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Validator;

class IndustryController extends Controller
{
    public function index(){
        $industries = Industry::all();
        return view('Admin.industry',compact('industries'));
    }

    public function getAll(){
        $roles = Industry::query();
        return DataTables::of($roles)
                ->addColumn('action',function($role){
                    return '<a href="#exampleModal1" class="badge badge-warning badge-pill text-black edit" data-toggle="modal" data-id="'.$role->id.'">Edit</a>';
                })
                ->make(true);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'title' => 'required|unique:industry'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'msg' => $validator->errors()->all()[0]
            ], 422);
        }
         
        $industry=Industry::create(['title' => $request->title ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function edit(Request $request){
        $industry = Industry::find($request->id);

        return response()->json([
            'name' => $industry->title,
            'id' => $industry->id
        ]);
    }

    public function update(Request $request){
        $industry = Industry::find($request->id);

        $industry->title = $request->title;
        if($industry->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
