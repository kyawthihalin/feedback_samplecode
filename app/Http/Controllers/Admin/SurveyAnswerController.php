<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AnswerRequest;
use App\PreAnswer;
use Illuminate\Http\Request;

class SurveyAnswerController extends Controller
{
    public function index(){
        if(!auth()->user()->isMerchant() && !auth()->user()->isHotel()){
            return redirect('/admin');
        }
        if(auth()->user()->isAdmin()) {
            $answers = PreAnswer::all();
        } else {
            $answers = PreAnswer::where('user_id', auth()->user()->id)->get();
        }
        return view('preAnswers.index',compact('answers'));
    }

    public function getAll()
    {
        if (auth()->user()->isAdmin()) {
            $preanswers = PreAnswer::all();
        } else {
            $preanswers = PreAnswer::where('user_id', auth()->user()->id)->get();
        }
        return view('Admin.survey.pre-answer-list', compact('preanswers'));
    }

    public function store(AnswerRequest $request){
        $preAns = new PreAnswer();
        $preAns->user_id = auth()->user()->id;
        $preAns->description = $request->name;
        if($preAns->save()){
            return response()->json([
                'status'=> 'success'
            ]);
        }
    }

    public function delete(Request $request){
        $answer = PreAnswer::find($request->id);
        if($answer->delete()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function edit(Request $request){
        $answer = PreAnswer::find($request->id);
        return view('details.editPreAnswer',compact('answer'));
    }
    
    public function update(AnswerRequest $request){
        $answer = PreAnswer::find($request->id);
        $answer->description = $request->name;
        if( $answer->save()){
            return response()->json([
                'status'=>'success'
            ]);
        }else{
            return response()->json([
                'status'=>'fail'
            ]);
        }
    }
}
