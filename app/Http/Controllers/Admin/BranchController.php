<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Branch;
use App\User;
use App\Http\Requests\CreateBranchRequest;
use Dotenv\Result\Success;
use Yajra\DataTables\DataTables;

class BranchController extends Controller
{
    public function index(){
        $users = User::with('roles')->get();
        if(auth()->user()->isAdmin())
        {
            $branches = Branch::orderBy('id', 'desc')->get();
        } else {
            $branches = Branch::where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        }

        $first_branch = Branch::with('user')->orderBy('id', 'desc')->first();
        $count = Branch::all()->count();
        return view('Admin.branch',compact('branches','first_branch','count','users'));
    }

    public function getAll() {
        if(auth()->user()->isAdmin()){
            $branches = Branch::with('user')->get();

            return DataTables::of($branches)
                    ->addColumn('action',function($branch){
                        return '<a href="#"><span class="badge badge-primary badge-pill" id="detail'. $branch->id .'" onclick="detail('. $branch->id.')">Detail</span>
                        </a><a href="#"><span class="badge badge-warning badge-pill" id="editSub'. $branch->id .'" onclick="editSub('. $branch->id.')">Edit</span>
                        </a><a href="#"><span class="badge badge-danger badge-pill" id="deleteSub'. $branch->id . '" onclick="deleteSub('.$branch->id.')">Delete</span></a>';
                    })
                    ->addColumn('user',function($branch){
                        return $branch->user->name;
                    })
                    ->make(true);
        } else {
            $branches = Branch::with('user')->where('user_id', auth()->user()->id)->get();

            return DataTables::of($branches)
                    ->addColumn('action',function($branch){
                        return '<a href="#"><span class="badge badge-primary badge-pill" id="detail'. $branch->id .'" onclick="detail('. $branch->id.')">Detail</span>
                        </a><a href="#"><span class="badge badge-warning badge-pill" id="editSub'. $branch->id .'" onclick="editSub('. $branch->id.')">Edit</span>';
                    })
                    ->addColumn('user',function($branch){
                        return $branch->user->name;
                    })
                    ->make(true);
        }
    }

    public function store(CreateBranchRequest $request){
        $branch = new Branch();
        $branch->name = $request->branchName;
        $branch->user_id = $request->userId;
        $branch->phone = $request->phone;
        $branch->email = $request->email;
        $branch->from = $request->from;
        $branch->to = $request->to;
        $branch->active_week_day = json_encode($request->days);
        if($branch->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
    public function update(Request $request){
        $branch = Branch::find($request->id);
        $branch->name = $request->branchName;
        if(auth()->user()->isAdmin()) {
            $branch->user_id = $request->userId;
        }
        $branch->phone = $request->phone;
        $branch->email = $request->email;
        $branch->from = $request->from;
        $branch->to = $request->to;
        $branch->active_week_day = json_encode($request->days);
        if($branch->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
    public function destroy(Request $request){
       $branch = Branch::find($request->id);

       if(count($branch->subBranch) > 0) {
        return response()->json([
            'msg' => 'Cannot delete this experience point.'
        ], 422);
       } else {
           if($branch->delete()){
               return redirect()->back();
           }
       }

    }
    public function detail(Request $request){
        $branch = Branch::with('subBranch')->find($request->id);
        return view('details.branch',compact('branch'));
    }

    public function edit(Request $request){
       $branch = Branch::find($request->id);
       $users = User::get();

       return view('details.editBranch',compact('users','branch'));
    }
}
