<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Industry;
use App\Template;
use App\TemplatePreAnswer;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class TemplateController extends Controller
{

    public function index()
    {
        if (!auth()->user()->isHotel()) {
            $industries = Industry::all();
            return view('Admin.template.list', compact('industries'));
        } else {
            return view('Admin.question.list');
        }
    }

    public function create()
    {
        if (!auth()->user()->isHotel()) {
            $industries = Industry::all();
            return view('Admin.template.create', compact('industries'));
        } else {
            return view('Admin.question.create');
        }
    }

    public function edit(Template $template)
    {
        if (!auth()->user()->isHotel()) {
            $industries = Industry::all();
            return view('Admin.template.edit', compact('industries', 'template'));
        } else {
            return view('Admin.question.edit', compact('template'));
        }
    }

    public function getSingle(Template $template)
    {
        $preAnswers = $template->preAnswers;
        return response(['template' => $template, 'preAnsers' => $preAnswers], 200);
    }

    public function getAll()
    {
        if (!auth()->user()->isHotel()) {
            $templates = DB::table('templates')
                ->join('industry', 'templates.industry_id', 'industry.id')
                ->select('templates.id', 'templates.question_text', 'industry.title')->get();
        } else {
            $templates = DB::table('templates')
                ->where('user_id', auth()->user()->id)
                ->select('id', 'question_text')->get();
        }

        // dd($templates);

        return DataTables::of($templates)
            ->addColumn('action', function ($template) {
                return
                // '<a href=' . url("admin/survey-list/preview", $template->id) . ' class="badge badge-secondary badge-pill text-black">Preview</a>
                '<a href=' . url("admin/template", $template->id) . ' class="badge badge-warning badge-pill text-black">Edit</a>
                <a href="#" class="badge badge-danger badge-pill text-black" id="delete-btn" data-id="' . $template->id . '">Delete</a>';
            })
            ->make(true);
    }

    public function getByIndustry(Industry $industry)
    {
        return response(['templates' => $industry->templates], 200);
    }

    public function store(Request $request)
    {
        if (!auth()->user()->isHotel()) {
            $validation = [
                'industry_id' => 'required',
                'question_text' => 'required',
            ];

            $message = [
                'industry_id.required' => 'Industry is Required.',
                'question_text.required' => 'Question is Required.',
            ];

            foreach ([1, 2, 3, 4] as $num) {
                $validation['photo' . $num] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
                $validation['description' . $num] = 'required';

                $message['photo' . $num . '.required'] = 'Photos are required.';
                $message['description' . $num . '.required'] = 'Follow ups are required.';
            }

            $validator = Validator::make($request->all(), $validation, $message);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors()->all());
            }

            $template = new Template();
            $template->industry_id = $request->industry_id;
            $template->question_text = $request->question_text;
            $template->description = $request->description;
            $template->save();

            foreach ([1, 2, 3, 4] as $num) {
                if ($_REQUEST['description' . $num]) {
                    $file = '';
                    if ($request->hasFile('photo' . $num)) {
                        $file = $request->file('photo' . $num);
                        $file = 'assets/images/followup/' . Str::random(15) . time() . '.' . $request->file('photo' . $num)->getClientOriginalExtension();
                        $request->file('photo' . $num)->move(public_path('assets/images/followup/'), $file);
                    }

                    $template->preAnswers()->create([
                        'photo' => $file,
                        'template_id' => $template->id,
                        'description' => $_REQUEST['description' . $num],
                    ]);
                }
            }

            return redirect()->back()->with('success', 'Template successfully created');
        } else {
            $validator = Validator::make($request->all(), ["question" => "required|array|min:1"]);

            if ($validator->fails()) {
                return response([
                    'error' => $validator->errors()->all(),
                ], 422);
            }

            $existedQuestions = [];
            foreach ($request->question as $question) {
                $isExist = Template::where('question_text', $question)
                    ->where('type', 'custom')->first();
                if ($isExist) {
                    array_push($existedQuestions, $question);
                }
            }
            if ($existedQuestions) {
                return response([
                    'error' => 'Question has already been taken.',
                    'questions' => $existedQuestions,
                ], 422);
            }

            foreach ($request->question as $question) {
                $newQuestion = new Template;
                $newQuestion->question_text = $question;
                $newQuestion->industry_id = 0;
                $newQuestion->type = 'custom';
                $newQuestion->user_id = auth()->user()->id;
                $newQuestion->save();
            }

            return response('success', 200);
        }
    }

    public function update(Template $template, Request $request)
    {
        if(!auth()->user()->isHotel()){
            $preAnswersIds = $template->preAnswers->pluck('id')->toArray();
            $validation = [
                'industry_id' => 'required',
                'question_text' => 'required',
            ];

            $message = [
                'industry_id.required' => 'Industry is Required.',
                'question_text.required' => 'Question is Required.',
            ];

            foreach ($preAnswersIds as $preAnswersId) {
                if (!$_REQUEST['isoldphoto' . $preAnswersId]) {
                    $validation['photo' . $preAnswersId] = 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048';
                }
                $validation['followup' . $preAnswersId] = 'required';

                $message['photo' . $preAnswersId . '.required'] = 'Photos are Required.';
                $message['followup' . $preAnswersId . '.required'] = 'Follow ups are Required.';
            }

            $validator = Validator::make($request->all(), $validation, $message);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors()->all());
            }

            $template->industry_id = $request->industry_id;
            $template->question_text = $request->question_text;
            $template->description = $request->description;
            $template->save();

            foreach ($preAnswersIds as $preAnswersId) {
                $preanswer = TemplatePreAnswer::find($preAnswersId);

                $preanswer->description = $_REQUEST['followup' . $preAnswersId];
                if (!$_REQUEST['isoldphoto' . $preAnswersId]) {
                    File::delete($preanswer->photo);

                    $file = '';
                    if ($request->hasFile('photo' . $preAnswersId)) {
                        $file = $request->file('photo' . $preAnswersId);
                        $file = 'assets/images/followup/' . Str::random(15) . time() . '.' . $request->file('photo' . $preAnswersId)->getClientOriginalExtension();
                        $request->file('photo' . $preAnswersId)->move(public_path('assets/images/followup/'), $file);
                    }

                    $preanswer->photo = $file;
                }
                $preanswer->save();
            }

            return redirect()->back()->with('success', 'Template successfully updated');
            
        }
        else{
            $validator = Validator::make($request->all(), [
                'question_text' => 'required|unique:templates,question_text,'. $template->id,
            ], [
                'question_text.required' => 'Question is Required.',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors()->all());
            }

            $template->question_text = $request->question_text;
            $template->save();

            return redirect()->back()->with('success', 'Question successfully updated');
        
        }
    }

    public function destory(Template $template)
    {
        if(!auth()->user()->isHotel()){
            if (count($template->questions)) {
                return response([
                    "message", "This template is already in use!",
                ], 422);
            } else {
                foreach ($template->preAnswers as $preanswer) {
                    File::delete($preanswer->photo);
    
                    $preanswer->delete();
                }
    
                $template->delete();
            }
        }
        else{
            $template->delete();
        }
    }
}
