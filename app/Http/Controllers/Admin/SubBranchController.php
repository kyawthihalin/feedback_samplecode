<?php

namespace App\Http\Controllers\Admin;

use App\Branch;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSubBranch;
use App\SubBranch;
use Illuminate\Http\Request;

class SubBranchController extends Controller
{
    public function index(){
        if(auth()->user()->isAdmin()) {
            $branches = Branch::all();
            $subBranches = SubBranch::all();
        } else {

            $branches = Branch::where('user_id', auth()->user()->id)->get();
            $subBranches = auth()->user()->subbranch;
        }
        return view('Admin.sub-branch',compact('branches','subBranches'));
    }
    public function store(CreateSubBranch $request){
        $subBranch = new SubBranch();
        $subBranch->name = $request->name;
        $subBranch->public_name = $request->public_name;
        $subBranch->branch_id = $request->branch;
        // $subBranch->active_weekDay = json_encode($request->days);

        $from = substr($request->datefilter, 0, 10); 
        $to = substr($request->datefilter, 14, 10);
        $from = strtotime($from);
        $to = strtotime($to);
        $from = date('Y-m-d',$from);
        $to = date('Y-m-d',$to);

        $subBranch->start_date = $from;
        $subBranch->end_date = $to;

        if( $subBranch->save()){
            return response()->json([
                'status'=>'success'
            ]);
        }else{
            return response()->json([
                'status'=>'fail'
            ]);
        }
    }
    public function delete(Request $request){
        $sub =  SubBranch::find($request->id);

        
        if(count($sub->question) > 0) {
            return response()->json([
                'msg' => 'Cannot delete this subgroup.'
            ], 422);
        } else {
            if($sub->delete()){
                return response()->json([
                    'status' => 'success'
                ]);
            }
            else{
                return response()->json([
                    'status' => 'fail'
                ]);
            }
        }
    }
    public function edit(Request $request){
        $sub = SubBranch::find($request->id);
        $branches = Branch::where('user_id', auth()->user()->id)->get();
        $start_date = date('d/m/Y',strtotime($sub->start_date));
        $end_date = date('d/m/Y',strtotime($sub->end_date));
        return view('details.editSub',compact('sub','branches', 'start_date', 'end_date'));
    }
    public function update(Request $request){
        $subBranch = SubBranch::find($request->id);
        $subBranch->name = $request->name;
        $subBranch->public_name = $request->public_name;
        $subBranch->branch_id = $request->branch;
        // $subBranch->active_weekDay = json_encode($request->days);
        $from = substr($request->datefilter, 0, 10); 
        $to = substr($request->datefilter, 14, 10);
        $from = strtotime($from);
        $to = strtotime($to);
        $from = date('Y-m-d',$from);
        $to = date('Y-m-d',$to);

        $subBranch->start_date = $from;
        $subBranch->end_date = $to;

        if( $subBranch->save()){
            return response()->json([
                'status'=>'success'
            ]);
        }else{
            return response()->json([
                'status'=>'fail'
            ]);
        }
    }

    public function filter(Request $request) {

        if($request->branchId == 'all') {
            if(auth()->user()->isAdmin) {
                $subBranches = SubBranch::get();
            } else {
                $branches = Branch::where('user_id', auth()->user()->id)->pluck('id')->toArray();
                $subBranches = SubBranch::whereIn('branch_id', $branches)->get();
            }
        } else {
            $subBranches = SubBranch::where('branch_id', $request->branchId)->get();
        }

        $res = [];

        $count = 0;
        foreach($subBranches as $subBranch) {
            $res[$count]['id'] = $subBranch->id;
            $res[$count]['name'] = $subBranch->public_name;
            $res[$count]['date'] = date('d/m/Y', strtotime($subBranch->start_date)) . '-' . date('d/m/Y', strtotime($subBranch->end_date));
            $res[$count]['branch'] = $subBranch->branch->name;
            $res[$count]['status'] = $subBranch->status;

            $count++;
        }

        return response()->json($res);
    }

    public function changeStatus(Request $request) {
        $subBranch = SubBranch::find($request->id);

        if($subBranch->status == 1) {
            $subBranch->status = 0;
        } else {
            $subBranch->status = 1;
        }

        $subBranch->save();

        return response()->json([
            'success' => true,
            'msg' => 'Change status successfully'
        ], 201);
    }
}
