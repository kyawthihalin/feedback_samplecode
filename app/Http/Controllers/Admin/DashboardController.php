<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Industry;
use App\FeedBack;
use DB;
use Carbon\Carbon;
use App\Exports\AdminReportExport;
use App\Exports\AdminCountryExport;
use App\Exports\AdminMonthlyExport;
use App\Exports\AdminIndustryExport;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    public function index()
    {
        if(!auth()->user()->isAdmin()){
            return redirect('/admin');
        }
        $date = date('Y-m-d 00:00:00',strtotime("-1 days"));
        // $User::where('created_at', '>=', Carbon::now()->subDay())->get()->groupBy(function($date) {
        //     return Carbon::parse($date->created_at)->format('h');
        // });

        $activeUserYesterday = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where('activated_at', '=' , $date)->get();

        $activeUser = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where('activated_at', '!=' , null)->get();

        $activeUserByMonth = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where("activated_at",">", Carbon::now()->subMonths(6))->groupBy('date')
        ->get([
            DB::raw('MONTH(activated_at) as date'),
            DB::raw('count(id) as total')
        ])
        ->pluck('total', 'date');

        $activeUserByHour = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
            )->where('status', 1)->where("activated_at",">", Carbon::now()->subHours(6)->toDateTimeString())->get()->groupBy('activated_at')->map->count();

            
        $clickByHour = FeedBack::where("created_at",">", Carbon::now()->subHours(6)->toDateTimeString())->get();

        $clickByHour = $clickByHour->groupBy(function($click){
            return date('g A',strtotime($click->feedback_time));
        })->map->count();
        
        $userByCountry = $activeUser->groupBy('country_id')->map->count();
        $userByIndustry = $activeUser->groupBy('industry_id')->map->count();
        $totalActiveUser = $activeUser->count();
        $totalClick = FeedBack::get()->count();

        // Country Start
        $countryCategory = [];
        $countryData = [];
        
        $byCountryCount = 0;
        foreach($userByCountry as $key => $value) {
            $country = Country::find($key);
            $countryCategory[$byCountryCount] = $country->name;
            $countryData[$byCountryCount] = $value;

            $byCountryCount++;
        }
        // Country End

        // Industry Start
        $industryCategory = [];
        $industryData = [];
        
        $byIndustryCount = 0;
        foreach($userByIndustry as $key => $value) {
            $industry = Industry::find($key);
            $industryCategory[$byIndustryCount] = $industry->title;
            $industryData[$byIndustryCount] = $value;

            $byIndustryCount++;
        }
        // Industry End
        
        //User by Month Start
        $userByMonthCategory = [];
        $userByMonthData = [];

        $userByMonthCount = 0;
        foreach($activeUserByMonth as $key => $value) {
            $userByMonthCategory[$userByMonthCount] = date("M", mktime(0, 0, 0, $key, 1));
            $userByMonthData[$userByMonthCount] = $value;

            $userByMonthCount++;
        }
        //User by Month End

        //User by Hour Start
        $userByHourCategory = [];
        $userByHourData = [];

        $userByHourCount = 0;
        foreach($activeUserByHour as $key => $value) {
            $userByHourCategory[$userByHourCount] = date("y/m/d g A", strtotime($key));
            $userByHourData[$userByHourCount] = $value;

            $userByHourCount++;
        }
        //User by Hour End

        //Click by Hour Start
        $clickByHourCategory = [];
        $clickByHourData = [];

        $clickByHourCount = 0;
        foreach($clickByHour as $key => $value) {
            $clickByHourCategory[$clickByHourCount] = date("y/m/d g A", strtotime($key));
            $clickByHourData[$clickByHourCount] = $value;

            $clickByHourCount++;
        }
        //Click by Hour End

        return view('Admin.dashboard', compact('totalActiveUser', 'totalClick', 'countryCategory', 'countryData', 'industryCategory', 'industryData', 'userByMonthCategory', 'userByMonthData', 'userByHourCategory', 'userByHourData', 'clickByHourCategory', 'clickByHourData'));
    }

    public function export(Request $request) {

        $date = date('Y-m-d');

        $activeUserYesterday = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where('activated_at', '=' , $date)->get();

        $activeUser = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where('activated_at', '!=' , null)->get();

        $activeUserByMonth = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
        )->where('status', 1)->where("activated_at",">", Carbon::now()->subMonths(6))->groupBy('date')
        ->get([
            DB::raw('MONTH(activated_at) as date'),
            DB::raw('count(id) as total')
        ])
        ->pluck('total', 'date');

        $activeUserByHour = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Merchant');
            }
            )->where('status', 1)->where("activated_at",">", Carbon::now()->subHours(6)->toDateTimeString())->get()->groupBy('activated_at')->map->count();

            
        $clickByHour = FeedBack::where("created_at",">", Carbon::now()->subHours(6)->toDateTimeString())->get();

        $clickByHour = $clickByHour->groupBy(function($click){
            return date('g A',strtotime($click->feedback_time));
        })->map->count();
        
        $userByCountry = $activeUser->groupBy('country_id')->map->count();
        $userByIndustry = $activeUser->groupBy('industry_id')->map->count();
        $totalActiveUser = $activeUser->count();
        $totalClick = FeedBack::get()->count();

        // Country Start
        $countryCategory = [];
        $countryData = [];
        
        $byCountryCount = 0;
        foreach($userByCountry as $key => $value) {
            $country = Country::find($key);
            $countryCategory[$byCountryCount] = $country->name;
            $countryData[$byCountryCount] = $value;

            $byCountryCount++;
        }
        // Country End

        // Industry Start
        $industryCategory = [];
        $industryData = [];
        
        $byIndustryCount = 0;
        foreach($userByIndustry as $key => $value) {
            $industry = Industry::find($key);
            $industryCategory[$byIndustryCount] = $industry->title;
            $industryData[$byIndustryCount] = $value . '%';

            $byIndustryCount++;
        }
        // Industry End
        
        //User by Month Start
        $userByMonthCategory = [];
        $userByMonthData = [];

        $userByMonthCount = 0;
        foreach($activeUserByMonth as $key => $value) {
            $userByMonthCategory[$userByMonthCount] = date("M", mktime(0, 0, 0, $key, 1));
            $userByMonthData[$userByMonthCount] = $value;

            $userByMonthCount++;
        }
        //User by Month End

        //User by Hour Start
        $userByHourCategory = [];
        $userByHourData = [];

        $userByHourCount = 0;
        foreach($activeUserByHour as $key => $value) {
            $userByHourCategory[$userByHourCount] = date("y/m/d g A", strtotime($key));
            $userByHourData[$userByHourCount] = $value;

            $userByHourCount++;
        }
        //User by Hour End

        //Click by Hour Start
        $clickByHourCategory = [];
        $clickByHourData = [];

        $clickByHourCount = 0;
        foreach($clickByHour as $key => $value) {
            $clickByHourCategory[$clickByHourCount] = date("y/m/d g A", strtotime($key));
            $clickByHourData[$clickByHourCount] = $value;

            $clickByHourCount++;
        }
        //Click by Hour End

        $totalActiveUserArr = ['Total Active User', $totalActiveUser];
        $totalClickArr = ['Total Click', $totalClick];

        array_unshift($countryCategory, 'Country List');
        array_unshift($countryData, 'Number of Users');

        array_unshift($userByMonthCategory, 'Last Six Month');
        array_unshift($userByMonthData, 'Number of Users');


        array_unshift($industryCategory, 'Industry List');
        array_unshift($industryData, 'Number of Users');


        // return view('Admin.dashboard', compact('totalActiveUser', 'totalClick', 'countryCategory', 'countryData', 'industryCategory', 'industryData', 'userByMonthCategory', 'userByMonthData', 'userByHourCategory', 'userByHourData', 'clickByHourCategory', 'clickByHourData'));

        if($request->chart == 'country') {
            $export = new AdminCountryExport([
                ['Country'],
                $countryCategory,
                $countryData,
            ]);
        } elseif($request->chart == 'month') {
            $export = new AdminMonthlyExport([
                ['Last Six Month'],
                $userByMonthCategory,
                $userByMonthData,
            ]);
        } elseif($request->chart == 'industry') {
            $export = new AdminIndustryExport([
                ['Industry'],
                $industryCategory,
                $industryData
            ]);
        } else {
            $export = new AdminReportExport([
                ['Dashboard Report'],
                $totalActiveUserArr,
                [''],
                $totalClickArr,
                [''],
                ['Country'],
                $countryCategory,
                $countryData,
                [''],
                ['Last Six Month'],
                $userByMonthCategory,
                $userByMonthData,
                [''],
                ['Industry'],
                $industryCategory,
                $industryData
            ]);
        }


    return Excel::download($export, 'DashboardReport('. $date .').xlsx');


    }

}
