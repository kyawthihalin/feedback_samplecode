<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Validator;

class CountryController extends Controller
{
    public function index(){
        $countries = Country::all();
        return view('Admin.country',compact('countries'));
    }

    public function getAll(){
        $roles = Country::query();
        return DataTables::of($roles)
                ->addColumn('action',function($role){
                    return '<a href="#exampleModal1" class="badge badge-warning badge-pill text-black edit" data-toggle="modal" data-id="'.$role->id.'">Edit</a>';
                })
                ->make(true);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:country'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'msg' => $validator->errors()->all()[0]
            ], 422);
        }
         
        $country=Country::create(['name' => $request->name ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function edit(Request $request){
        $country = Country::find($request->id);

        return response()->json([
            'name' => $country->name,
            'id' => $country->id
        ]);
    }

    public function update(Request $request){
        $country = Country::find($request->id);

        $country->name = $request->name;
        if($country->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
}
