<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\User;
use App\Branch;
use App\Country;
use App\Industry;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(){

        if(auth()->user()->isAdmin()){
            $users = User::all();
        } else {
            $users = User::find(auth()->user()->id);
        }

        $industries = Industry::all();
        $countries = Country::all();
        return view('backend.admin-user.index',compact('users', 'industries', 'countries'));
    }

    public function analysisUser(){
        if(auth()->user()->isAdmin()){
            $users = User::all();
        } else {
            $users = User::find(auth()->user()->id);
        }

        $industries = Industry::all();
        $countries = Country::all();
        return view('backend.admin-user.analysis-user',compact('users', 'industries', 'countries'));
    }
    public function getAll(){
        
        if(auth()->user()->isAdmin()){
            $users = User::where('parent_id', null)->get();

            return DataTables::of($users)
                ->addColumn('action',function($user){

                    // $cahangeStatus = '<a href="#" class="badge badge-success badge-pill text-white activate" data-id="'.$user->id.'">Activate</a>';

                    // if($user->status == 1) {
                    //     $cahangeStatus = '<a href="#" class="badge badge-info badge-pill text-white deactivate" data-id="'.$user->id.'">Deactivate</a>';
                    // }

                    return '<a href="#" class="badge badge-danger badge-pill text-white delete" data-id="'.$user->id.'">Delete</a>
                    <a href="#" class="badge badge-warning badge-pill text-black edit" id="edit-btn" data-id="'.$user->id.'">Edit</a>
                    <a href="#" class="badge badge-primary badge-pill text-black edit-password" id="edit-password-btn" data-id="'.$user->id.'">Change Password</a><a href="#" class="badge badge-secondary badge-pill text-black" id="detail-btn" data-id="'.$user->id.'">Detail</a>';
                })
                ->addColumn('roles',function($user){
                    $roles=$user->getRoleNames();
                    $roles=str_replace("[","",$roles);
                    $roles=str_replace("]","",$roles);
                    $roles=str_replace('"','',$roles);
                        return $roles;
                })
                ->make(true);
        } else {
            $users = User::where('id', auth()->user()->id)->get();

            return DataTables::of($users)
                ->addColumn('action',function($user){
                    return '<a href="#" class="badge badge-warning badge-pill text-black edit" id="edit-btn" data-id="'.$user->id.'">Edit</a>
                    <a href="#" class="badge badge-primary badge-pill text-black edit-password" id="edit-password-btn" data-id="'.$user->id.'">Change Password</a><a href="#" class="badge badge-secondary badge-pill text-black" id="detail-btn" data-id="'.$user->id.'">Detail</a>';
                })
                ->addColumn('roles',function($user){
                    $roles=$user->getRoleNames();
                    $roles=str_replace("[","",$roles);
                    $roles=str_replace("]","",$roles);
                    $roles=str_replace('"','',$roles);
                        return $roles;
                })
                ->make(true);
        }

    }

    public function getAnalysisUser() {
        $users = User::where('parent_id', auth()->user()->id)->get();

        return DataTables::of($users)
            ->addColumn('action',function($user){
                return '';
            })
            ->addColumn('roles',function($user){
                $roles=$user->getRoleNames();
                $roles=str_replace("[","",$roles);
                $roles=str_replace("]","",$roles);
                $roles=str_replace('"','',$roles);
                    return $roles;
            })
            ->make(true);
    }

    public function store(UserRequest $request){
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = new User();

        $from = substr($request->datefilter, 0, 10); 
        $to = substr($request->datefilter, 14, 10);
        $from = strtotime($from);
        $to = strtotime($to);
        $from = date('Y-m-d',$from);
        $to = date('Y-m-d',$to);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->country_id = $request->country_id;
        $user->industry_id = $request->industry_id;
        $user->password = bcrypt($request->password);
        $user->activated_at = date('Y-m-d H:i:s');
        $user->start_date = $from;
        $user->end_date = $to;
        $user->save();
        $user->assignRole($request->input('roles'));
        

        return response()->json([
           'status' => 'success'
        ]);
        $user->sendEmailVerificationNotification();
    }

    public function addAnalysisUser(Request $request) {

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                'msg' => $validator->errors()->all()[0]
            ], 422);
        }

        $user = new User();
        $user->parent_id = auth()->user()->id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->smily_notification = $request->smileNotification;
        $user->weekly_report = $request->weeklyReport;
        $user->daily_report = $request->dailyReport;
        $user->password = bcrypt($request->password);
        $user->activated_at = date('Y-m-d H:i:s');
        $user->save();
        $user->sendEmailVerificationNotification();

        $user->assignRole(3);
        $user->analysisBranch()->attach($request->branch);
        $user->analysisSubbranch()->attach($request->subbranch);
        return response()->json([
           'status' => 'success'
        ]);
    }

    public function delete(Request $request){
        $user = User::find($request->id);
        if(count($user->branch) > 0) {
            return response()->json([
                'msg' => 'Delete related experience point first.'
            ], 422);
        } else if(count($user->subbranch) > 0) {
            return response()->json([
                'msg' => 'Delete related sub group first.'
            ], 422);
        } else {    
            if($user->delete()){
                return response()->json([
                    'status' => 'success'
                ]);
            }
        }
    }
    public function edit(Request $request){
        $user = User::find($request->id);
        $start_date = date('d/m/Y',strtotime($user->start_date));
        $end_date = date('d/m/Y',strtotime($user->end_date));
        $roleId = $user->roles->first()->id;
        $industries = Industry::all();
        $countries = Country::all();
        
        return view('details.editUser',compact('user', 'industries', 'countries', 'start_date', 'end_date', 'roleId'));

    }
    public function update(UserUpdateRequest $request){
        $input = $request->all();
        $user = User::find($request->id);

        $from = substr($request->datefilter, 0, 10); 
        $to = substr($request->datefilter, 14, 10);
        $from = strtotime($from);
        $to = strtotime($to);
        $from = date('Y-m-d',$from);
        $to = date('Y-m-d',$to);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->country_id = $request->country_id;
        $user->industry_id = $request->industry_id;
        // $user->password = bcrypt($request->password);
        // $user->activated_at = date('Y-m-d');
        $user->start_date = $from;
        $user->end_date = $to;
        $user->save();
        if(auth()->user()->isAdmin()) {
            DB::table('model_has_roles')->where('model_id',$request->id)->delete();
            $user->assignRole($request->roles);
        }
        
        return response()->json([
            'status' => 'success'
        ]);
    }

    public function changeStatus(Request $request) {
        $user = User::find($request->id);

        if($user->status == 1) {
            $user->status = 0;
            $user->deactivated_at  = date('Y-m-d H:i:s');
        } else {
            $user->status = 1;
            $user->activated_at  = date('Y-m-d H:i:s');
        }

        $user->save();

        return response()->json([
            'success' => true,
            'msg' => 'Change status successfully'
        ], 201);
    }

    public function editPassword(Request $request){
        $user = User::find($request->id);
        // if(auth()->user()->isAdmin()){
            return view('details.editUserPasswordAdmin', compact('user'));
        // } else {
        //     return view('details.editUserPassword',compact('user'));
        // }
    }

    public function updatePasswordByAdmin(Request $request) {

        $validator = Validator::make($request->all(), [
            'password' => 'required|required_with:cpassword|same:cpassword|min:8',
            'cpassword' => 'required|same:password',
        ], [
            'password.required' => 'Password required*',
            'password.same' => 'Password should match',
            'password.min' => 'Minimum password 8 characters',
            'cpassword.same' => 'Password should match',
            'cpassword.required' => 'Confirm Password required*',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 422);
        }

        $user = User::find($request->id);

        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function detail(Request $request) {
        $user = User::find($request->id);
        return view('details.detailUser', compact('user'));
    }

    public function addNewAnalysis()
    {
        $branchs = Branch::with('subBranch')->where('user_id', auth()->user()->id)->orderBy('id', 'desc')->get();
        return view('backend.admin-user.merchant-analysis', compact('branchs'));
    }
}
