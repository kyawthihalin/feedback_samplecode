<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Question;
use App\FeedBack;   
use App\FeedBackType;
use App\SubBranch;
use App\Branch;
use App\PreAnswer;
use DB;

class QuickViewController extends Controller
{
    public function quickView(Request $request)
    {

        // if(!auth()->user()->isMerchant() || !auth()->user()->isAnalysis() ){
        //     return redirect('/admin');
        // }

        if(auth()->user()->isAdmin()) {
            return redirect('/admin');
        }

        $currentBranch = null;

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');
        $startDateStr = date('m/d/Y');
        $endDateStr = date('m/d/Y');

        if($request->date) {
            $daterange = explode('-', $request->date);
            $startDate = date('Y-m-d', strtotime($daterange[0]));
            $endDate = date('Y-m-d', strtotime($daterange[1]));
            $startDateStr = date('m/d/Y', strtotime($daterange[0]));
            $endDateStr = date('m/d/Y', strtotime($daterange[1]));
        }

        $currentUser = auth()->user();
        $userid = $currentUser->id;
        
        if(!auth()->user()->isHotel()) {

            $date = $request->date;
            $branchId = $request->branch ? $request->branch : null;
            $questionId = $request->question != 'all' ? $request->question : null;
    
            if(strpos($branchId, 'all') !== false)
            {
                $branch = str_replace('all', '', $branchId);
                $branch = Branch::with('subBranch')->find($branch);
                $subBranch = $branch->subBranch->pluck('id');
    
                $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranch) {
                    $query->whereIn('sub_branch_id', $subBranch);
                });
                $subBranchId = null;
    
                $questions = Question::whereHas('subBranch', function($query) use ($subBranch) {
                    $query->whereIn('sub_branch_id', $subBranch);
                })->get();
    
            } else {
                $subBranchId = $branchId;
    
                $lastQuestion = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                    $query->where('sub_branch_id', $subBranchId);
                });
    
                $questions = Question::whereHas('subBranch', function($query) use ($subBranchId) {
                    $query->where('sub_branch_id', $subBranchId);
                })->get();
            }
    
            if($questionId) {
                $lastQuestion = Question::where('id', $questionId);
            }
    
            if(count($request->all()) <= 0) {
                $lastQuestion = Question::where('user_id', auth()->user()->id)->orderBy('id', 'desc');
            }
    
            $branchArr = [];
            $subbranchArr = [];
    
            if(auth()->user()->isAdmin()) {
    
                $branchs = Branch::with('subBranch')->orderBy('id', 'desc')->get();
                // $questions = Question::orderBy('id', 'desc')->get();
            
            } else if(auth()->user()->isMerchant()) {
    
                $branchs = Branch::with('subBranch')->where('user_id', $userid)->orderBy('id', 'desc')->get();
                // $questions = Question::orderBy('id', 'desc')->get();
            } else if(auth()->user()->isAnalysis()) {
                $branchs = Branch::with('subBranch')->where('user_id', auth()->user()->parent_id)->orderBy('id', 'desc')->get();
    
                foreach($branchs as $branch) {
                    $branchArr[] = $branch->id;
                    foreach($branch->subBranch as $subbranch) {
                        $subbranchArr[] = $subbranch->id;
                    }
                }
    
            }
    
            // dd(auth()->user()->analysisBranch->pluck('id')->toArray());
    
            $hourlyChart = true;
            $lastWeekChart = true;
            $lastMonthChart = true;
    
            $hourList = [];
            $hourlyExcellentArr = [];
            $hourlyGoodArr = [];
            $hourlyPoorArr = [];
            $hourlyBadArr = [];
    
            $lastWeekDayList = [];
            $lastWeekExcellentArr = [];
            $lastWeekGoodArr = [];
            $lastWeekPoorArr = [];
            $lastWeekBadArr = [];
    
            $lastMonthList = [];
    
            $WeekList = [];
            $WeekExcellentArr = [];
            $WeekGoodArr = [];
            $WeekPoorArr = [];
            $WeekBadArr = [];
    
            $positiveAnsArr = [];
            $negativeAnsArr = [];
    
            $hourlyPattern = false;
            $patternHourList = [];
            $patternByDay = [];
            $patternByDay['Sun'] = [];
            $patternByDay['Mon'] = [];
            $patternByDay['Tue'] = [];
            $patternByDay['Wed'] = [];
            $patternByDay['Thu'] = [];
            $patternByDay['Fri'] = [];
            $patternByDay['Sat'] = [];
            
            $happyIndex = 0;
    
            $percentExcellent = 0;
            $percentGood = 0;
            $percentPoor = 0;
            $percentBad = 0;
            $positivePercent = 0;
            $negativePercent = 0;
            
            $totalFeedback = 0;
            
                // $lastQuestion = Question::with(['feedback','preAnswer', 'subBranch'])->orderBy('id', 'desc')->first();
                // if(count($request->all()) <= 0 || $date == 'yesterday') {
                    // dd($subBranch);
                    $lastQuestion = $lastQuestion->with(['feedback' => function($query) use ($subBranchId, $request, $startDate, $endDate) {
                        
                        if(count($request->all()) <= 0) {
                            $query->whereDate('feedback_date', date('Y-m-d'));
                        } else {
                            $query->whereBetween('feedback_date', [$startDate, $endDate]);
                        }
                        if($subBranchId) {
                            $query->where('sub_branch_id', '=', $subBranchId);
                        }
                    }, 'preAnswer', 'subBranch'])->first();
    
                    if(count($request->all()) <= 0) {
                        $currentDate = date('l d F Y');
                    } else {
                        $currentDate = date('d F Y', strtotime($startDate)) . ' - ' . date('d F Y', strtotime($endDate)); 
                    }
    
                    $hourlyChart = true;
                // }
    
                // if($date == 'last-week') {
                //     $previous_week = strtotime("-1 week +1 day");
    
                //     $start_week = strtotime("last sunday midnight",$previous_week);
                //     $end_week = strtotime("next saturday",$start_week);
    
                //     $start_week = date("Y-m-d",$start_week);
                //     $end_week = date("Y-m-d",$end_week);
    
                //     $currentDate = date('l d F', strtotime($start_week)) . ' - ' . date('l d F Y', strtotime($end_week));
    
    
                //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_week, $end_week, $subBranchId) {
                //         $query->whereBetween('feedback_date', [$start_week, $end_week]);
    
                //         if($subBranchId) {
                //             $query->where('sub_branch_id', '=', $subBranchId);
                //         }
                //     }, 'preAnswer', 'subBranch'])->first();
    
                //     $lastWeekChart = true;
                // }
    
                // if($date == 'last-month') {
    
                //     $start_date = date("Y-m-d", strtotime("first day of previous month"));
                //     $end_date = date("Y-m-d", strtotime("last day of previous month"));
    
                //     $currentDate = date('d F Y', strtotime($start_date)) . ' - ' . date('d F Y', strtotime($end_date)); 
    
                //     $lastQuestion = $lastQuestion->with(['feedback' => function($query) use($start_date, $end_date, $subBranchId) {
                //         $query->whereBetween('feedback_date', [$start_date, $end_date]);
                //         if($subBranchId) {
                //             $query->where('sub_branch_id', '=', $subBranchId);
                //         }
                //     }, 'preAnswer', 'subBranch'])->first();
    
                //     $lastMonthChart = true;
                // }
    
                
            if($lastQuestion) {
                $currentSubBranch = $lastQuestion->subBranch->first();
                if(count($request->all()) <= 0 || strpos($branchId, 'all') !== false) {
                    $currentBranch = $currentSubBranch->branch;
                } else {
                    $currentBranch = $currentSubBranch;
                }
        
                
        
        
                $currentFeedback = $lastQuestion->feedback;
                // dd($currentFeedback);
                $totalFeedback = $currentFeedback ? $currentFeedback->count() : 0;
                $feedbackTypeCount = $currentFeedback->groupBy('feedback_type_id')->map->count();
        
                $currentQuestionPreanswer = $currentFeedback->groupBy('preanswer_id')->map->count();
    
                if($totalFeedback > 0) {
                    // if(count($request->all()) <= 0 || $date == 'yesterday') {
        
                        $groupByHourly = $currentFeedback->groupBy(function($reg){
                            return date('g A',strtotime($reg->feedback_time));
                        });
                
                        $countHour = 0;
                        foreach($groupByHourly as $key => $hour) {
                            $hourList[$countHour] = $key;
                            $feedback = $hour->groupBy('feedback_type_id')->map->count();
                
                            foreach($feedback as $key => $value) {
                                switch ($key) {
                                    case 1:
                                        $hourlyExcellentArr[$countHour] = $value;
                                        break;
                                    
                                    case 2:
                                        $hourlyGoodArr[$countHour] = $value;
                                        break;
                
                                    case 3:
                                        $hourlyPoorArr[$countHour] = $value;
                                        break;
                
                                    case 4:
                                        $hourlyBadArr[$countHour] = $value;
                                        break;
                                }
                            }
                
                            $countHour++;
                        }
                    // }
        
                    // if($date == 'last-week') {
                        $groupByWeekDay = $currentFeedback->groupBy(function($reg){
                            return date('D',strtotime($reg->feedback_date));
                        });
                        // dd($groupByWeekDay);
                
                        $countWeekDay = 0;
                        foreach($groupByWeekDay as $key => $hour) {
                            $lastWeekDayList[$countWeekDay] = $key;
                            $feedback = $hour->groupBy('feedback_type_id')->map->count();
                
                            foreach($feedback as $key => $value) {
                                switch ($key) {
                                    case 1:
                                        $lastWeekExcellentArr[$countWeekDay] = $value;
                                        break;
                                    
                                    case 2:
                                        $lastWeekGoodArr[$countWeekDay] = $value;
                                        break;
                
                                    case 3:
                                        $lastWeekPoorArr[$countWeekDay] = $value;
                                        break;
                
                                    case 4:
                                        $lastWeekBadArr[$countWeekDay] = $value;
                                        break;
                                }
                            }
                
                            $countWeekDay++;
                        }
        
                    // }
        
                    // if($date == 'last-month') {
                        $groupByLastMonthDay = $currentFeedback->groupBy(function($reg){
                            return date('j/n',strtotime($reg->feedback_date));
                        });
                
                        $countMonthDay = 0;
                        foreach($groupByLastMonthDay as $key => $day) {
                            $lastMonthList[$countMonthDay]['a'] = $key;
                            $feedback = $day->groupBy('feedback_type_id')->map->count();
                
                            foreach($feedback as $key => $value) {
                                switch ($key) {
                                    case 1:
                                        $lastMonthList[$countMonthDay]['b'] = $value;
                                        break;
                                    
                                    case 2:
                                        $lastMonthList[$countMonthDay]['c'] = $value;
                                        break;
                
                                    case 3:
                                        $lastMonthList[$countMonthDay]['y'] = $value;
                                        break;
                
                                    case 4:
                                        $lastMonthList[$countMonthDay]['z'] = $value;
                                        break;
                                }
                            }
                
                            ksort($lastMonthList[$countMonthDay]);
                            $countMonthDay++;
                        }
        
                    // }
            
                    $positiveAnswer = $currentFeedback->whereIn('feedback_type_id', [1, 2]);
                    $positiveAnswerCount = $positiveAnswer->count();
            
                    $happyIndex = round($positiveAnswerCount / $totalFeedback * 100);
            
                    if($positiveAnswer) {
            
                        $groupPositiveAns = $positiveAnswer->groupBy('preanswer_id');
            
                        foreach($groupPositiveAns as $key=>$positiveAns) {
                            $positiveAnsGroup = $positiveAns->groupBy('feedback_type_id')->map->count();    
                            $preanswer = PreAnswer::find($key);
            
                            if($preanswer){
                                foreach ($positiveAnsGroup as $key => $value) {
                                    switch ($key) {
                                        case 1:
                                            $positiveAnsArr[$preanswer->description]['excellent'] = $value / $totalFeedback * 100;
                                            break;
                                        
                                        case 2:
                                            $positiveAnsArr[$preanswer->description]['good'] = $value / $totalFeedback * 100;
                                            break;
                                    }
                                }
                            }
            
                        }
                    }
            
                    $negativeAnswer = $currentFeedback->whereIn('feedback_type_id', [3, 4]);
                    if($negativeAnswer) {
                        $groupNegativeAns = $negativeAnswer->groupBy('preanswer_id');
            
                        foreach($groupNegativeAns as $key=>$negativeAns) {
                            $negativeAnsGroup = $negativeAns->groupBy('feedback_type_id')->map->count();    
                            $preanswer = PreAnswer::find($key);
            
                            if($preanswer) {
                                foreach ($negativeAnsGroup as $key => $value) {
                                    switch ($key) {
                                        case 3:
                                            $negativeAnsArr[$preanswer->description]['poor'] = $value / $totalFeedback * 100;
                                            break;
                                        
                                        case 4:
                                            $negativeAnsArr[$preanswer->description]['bad'] = $value / $totalFeedback * 100;
                                            break;
                                    }
                                }
                            }
            
                        }
                    }
            
                    foreach($feedbackTypeCount as $key=>$count) {
                        switch ($key) {
                            case 1:
                                $percentExcellent = round($count / $totalFeedback * 100, 0);
                                $positivePercent += round($count / $totalFeedback * 100, 0);
                                break;
                            
                            case 2:
                                $percentGood = round($count / $totalFeedback * 100, 0);
                                $positivePercent += round($count / $totalFeedback * 100, 0);
                                break;
            
                            case 3:
                                $percentPoor = round($count / $totalFeedback * 100, 0);
                                $negativePercent += round($count / $totalFeedback * 100, 0);
                                break;
            
                            case 4:
                                $percentBad = round($count / $totalFeedback * 100, 0);
                                $negativePercent += round($count / $totalFeedback * 100, 0);
                                break;
                        }
                    }
        
                    $patternGroupByDay = $currentFeedback->groupBy(function($reg){
                        return date('D',strtotime($reg->feedback_date));
                    });
        
                    foreach($patternGroupByDay as $dName => $day)
                    {
                        $happyByHour = $day->whereIn('feedback_type_id', [1, 2])->groupBy(function($reg){
                            return date('g A',strtotime($reg->feedback_time));
                        })->map->count();
        
                        $totalFeedbackByHour = $day->groupBy(function($reg){
                            return date('g A',strtotime($reg->feedback_time));
                        })->map->count();
        
                        $happyIndexByHour = [];
        
                        foreach($happyByHour as $key => $byHour) {
                            if(!in_array($key, $patternHourList)) {
                                array_push($patternHourList, $key);
                            }
        
                            $happyIndexByHour[$key] = round($byHour / $totalFeedbackByHour[$key] * 100);
                        }
                        ksort($happyIndexByHour);
                        $patternByDay[$dName] = $happyIndexByHour;
                        
                        sort($patternHourList);
                    }
        
                    $countPattern = 0;
                    foreach($patternHourList as $hour) {
            
                        if(array_key_exists($hour, $patternByDay['Sun'])) {
                            $patternByDay['Sun'][$countPattern] = $patternByDay['Sun'][$hour];
                            unset($patternByDay['Sun'][$hour]);
                        } else {
                            $patternByDay['Sun'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Mon'])) {
                            $patternByDay['Mon'][$countPattern] = $patternByDay['Mon'][$hour];
                            unset($patternByDay['Mon'][$hour]);
                        } else {
                            $patternByDay['Mon'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Tue'])) {
                            $patternByDay['Tue'][$countPattern] = $patternByDay['Tue'][$hour];
                            unset($patternByDay['Tue'][$hour]);
                        } else {
                            $patternByDay['Tue'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Wed'])) {
                            $patternByDay['Wed'][$countPattern] = $patternByDay['Wed'][$hour];
                            unset($patternByDay['Wed'][$hour]);
                        } else {
                            $patternByDay['Wed'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Thu'])) {
                            $patternByDay['Thu'][$countPattern] = $patternByDay['Thu'][$hour];
                            unset($patternByDay['Thu'][$hour]);
                        } else {
                            $patternByDay['Thu'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Fri'])) {
                            $patternByDay['Fri'][$countPattern] = $patternByDay['Fri'][$hour];
                            unset($patternByDay['Fri'][$hour]);
                        } else {
                            $patternByDay['Fri'][$countPattern] = 0;
                        }
            
                        if(array_key_exists($hour, $patternByDay['Sat'])) {
                            $patternByDay['Sat'][$countPattern] = $patternByDay['Sat'][$hour];
                            unset($patternByDay['Sat'][$hour]);
                        } else {
                            $patternByDay['Sat'][$countPattern] = 0;
                        }
            
                        $countPattern++;
                    }
                }
            }
    
            return view('Admin.index', compact('branchs', 'questions', 'currentBranch', 'lastQuestion', 'currentDate', 'percentExcellent', 'percentGood', 'percentPoor', 'percentBad' ,'negativePercent', 'positivePercent', 'totalFeedback', 'negativeAnsArr', 'positiveAnsArr', 'happyIndex', 'hourList', 'hourlyExcellentArr', 'hourlyGoodArr', 'hourlyPoorArr', 'hourlyBadArr', 'lastWeekBadArr', 'lastWeekPoorArr', 'lastWeekGoodArr', 'lastWeekExcellentArr', 'lastWeekDayList', 'date', 'lastWeekChart', 'hourlyChart', 'lastMonthList', 'lastMonthChart', 'branchId', 'questionId', 'patternByDay', 'patternHourList', 'startDateStr', 'endDateStr', 'branchArr', 'subbranchArr'));
        } else {

            $totalSmiley = 0;
            $totalFeedback = 0;

            $percentExcellent = 0;
            $percentGood = 0;
            $percentPoor = 0;
            $percentBad = 0;
            $positivePercent = 0;
            $negativePercent = 0;

            $positiveAnswerCount = 0;
            $negativeAnswerCount = 0;

            $comparisonArr = [];

            $date = $request->date;
            $questionId = $request->question != 'all' ? $request->question : null;

            $questions = Question::where('user_id', $userid)->get();

            if($questionId) {
                $lastQuestion = Question::where('id', $questionId);
            }
    
            if(count($request->all()) <= 0) {
                $lastQuestion = Question::where('user_id', auth()->user()->id)->orderBy('id', 'desc');
            }

            if(count($request->all()) <= 0) {
                $currentDate = date('l d F Y');
            } else {
                $currentDate = date('d F Y', strtotime($startDate)) . ' - ' . date('d F Y', strtotime($endDate)); 
            }

            $lastQuestion = $lastQuestion->with(['feedback' => function($query) use ($request, $startDate, $endDate) {
                        
                if(count($request->all()) <= 0) {
                    $query->whereDate('feedback_date', date('Y-m-d'));
                } else {
                    $query->whereBetween('feedback_date', [$startDate, $endDate]);
                }
            }])->first();

            if($lastQuestion) {
                $currentFeedback = $lastQuestion->feedback;
                $totalFeedback = $currentFeedback ? $currentFeedback->count() : 0;
                $feedbackTypeCount = $currentFeedback->groupBy('feedback_type_id')->map->count();
    
                foreach ($questions as $question) {
                    $comparisonGroupByType = $question->feedback->groupBy('feedback_type_id')->map->count();
                    $comparisonCount = $question->feedback->count();
                    $questionText = '';
                    
                    if(is_array($question->question_text)){
                        foreach (json_decode($question->question_text) as $key=>$text) {
                        $questionText .= $text;
        
                        if(count(json_decode($question->question_text)) > 1 && $key != count(json_decode($question->question_text)) - 1)
                        {
                            $questionText .= ', ';
                        }
                        }
                    }else{
                        $questionText = $question->question_text;
                    }   
    
                    if($comparisonCount > 0) {
                        $comparisonArr[$questionText]['total'] = 0;
                        foreach($comparisonGroupByType as $typeKey => $comVal) {
        
                            switch ($typeKey) {
                                case 1:
                                    $comparisonArr[$questionText]['excellent'] = $comVal / $comparisonCount * 100;
                                    $comparisonArr[$questionText]['total'] += $comVal;
                                break;
        
                                case 2:
                                    $comparisonArr[$questionText]['good'] = $comVal / $comparisonCount * 100;
                                    $comparisonArr[$questionText]['total'] += $comVal;
                                break;
        
                                case 3:
                                    $comparisonArr[$questionText]['poor'] = $comVal / $comparisonCount * 100;
                                    $comparisonArr[$questionText]['total'] += $comVal;
                                break;
        
                                case 4:
                                    $comparisonArr[$questionText]['bad'] = $comVal / $comparisonCount * 100;
                                    $comparisonArr[$questionText]['total'] += $comVal;
                                break;
                            }   
        
                        }
                    }
    
                }
    
                if ($totalFeedback > 0) {
                    $positiveAnswer = $currentFeedback->whereIn('feedback_type_id', [1, 2]);
                    $positiveAnswerCount = $positiveAnswer->count();
    
                    $negativeAnswer = $currentFeedback->whereIn('feedback_type_id', [3, 4]);
                    $negativeAnswerCount = $negativeAnswer->count();
            
                    $totalSmiley = round($positiveAnswerCount / $totalFeedback * 100);
    
                    foreach($feedbackTypeCount as $key=>$count) {
                        switch ($key) {
                            case 1:
                                $percentExcellent = round($count / $totalFeedback * 100, 0);
                                $positivePercent += round($count / $totalFeedback * 100, 0);
                                break;
                            
                            case 2:
                                $percentGood = round($count / $totalFeedback * 100, 0);
                                $positivePercent += round($count / $totalFeedback * 100, 0);
                                break;
            
                            case 3:
                                $percentPoor = round($count / $totalFeedback * 100, 0);
                                $negativePercent += round($count / $totalFeedback * 100, 0);
                                break;
            
                            case 4:
                                $percentBad = round($count / $totalFeedback * 100, 0);
                                $negativePercent += round($count / $totalFeedback * 100, 0);
                                break;
                        }
                    }
                }
            }

            return view('Admin.Hotel.quick-view', compact('questions', 'questionId', 'date', 'startDateStr', 'endDateStr', 'currentDate', 'lastQuestion', 'totalSmiley', 'percentExcellent', 'percentGood', 'percentPoor', 'percentBad', 'positivePercent', 'negativePercent', 'totalFeedback', 'positiveAnswerCount', 'negativeAnswerCount', 'comparisonArr'));
        }

    }

    public function classified($feedbacks)
    {
        $res = [];
        
        $res['postive_normal'] = 0;
        $res['postive_special'] = 0;
        $res['negative_normal'] = 0;
        $res['negative_special'] = 0;

        foreach($feedbacks as $feedback)
        {
            if($feedback->feedback_type_id == 1){
                $res['postive_normal']++;
            } elseif($feedback->feedback_type_id == 2) {
                $res['postive_special']++;
            } elseif($feedback->feedback_type_id == 3) {
                $res['negative_normal']++;
            } elseif($feedback->feedback_type_id == 4) {
                $res['negative_special']++;
            }
        }

        return $res;
    }
}
