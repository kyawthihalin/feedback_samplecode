<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function index(){
        $roles = Role::all();
        return view('backend.role.index',compact('roles'));
    }
    public function getAll(){
        $roles = Role::query();
        return DataTables::of($roles)
                ->addColumn('action',function($role){
                    return '<a href="#exampleModal1" class="badge badge-warning badge-pill text-black edit" data-toggle="modal" data-id="'.$role->id.'">Edit</a>';
                })
                ->make(true);
                // <a href="#" class="badge badge-danger badge-pill text-white delete" data-id="'.$role->id.'">Delete</a>
                            // <a href="#exampleModal2" class="badge badge-primary badge-pill text-black detail" data-toggle="modal" data-id="'.$role->id.'">Details</a>
    }
    public function store(RoleRequest $request){
        $role=Role::create(['name' => $request->name ]);
        $role->syncPermissions($request->permissions);

       return response()->json([
           'status' => 'success'
       ]);
    }
    public function delete(Request $request){
        $role = Role::find($request->id);
        if($role->delete()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
    public function edit(Request $request){
        $role = Role::find($request->id);
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$request->id)->get();
        return response()->json([
            'role' => $role->name,
            'id' => $role->id,
            'rolePermissions'=>$rolePermissions
        ]);
    }
    public function update(RoleRequest $request){
        $role = Role::find($request->id);
        $role->syncPermissions($request->permissions);
        $role->name = $request->name;
        if($role->save()){
            return response()->json([
                'status' => 'success'
            ]);
        }
    }
    public function detail(Request $request){
        $role = Role::find($request->id);
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$request->id)->get();
        $perArray =[];
        foreach($rolePermissions as $roleper){
            $perArray[] +=$roleper->permission_id;
        }
        $permissions = Permission::whereIn('id',$perArray)->get();
        return response()->json([
            'role' => $role->name,
            'id' => $role->id,
            'rolePermissions'=>$permissions
        ]);
    }
}
