<?php

namespace App\Http\Controllers\Admin;

use App\Branch;
use App\Http\Controllers\Controller;
use App\Industry;
use App\PreAnswerQuestion;
use App\Question;
use App\QuestionSubBranch;
use App\Survey;
use App\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class SurveyController extends Controller
{
    public function index()
    {
        return view('Admin.survey.list');
    }

    public function plan()
    {
        if (!auth()->user()->isMerchant() && !auth()->user()->isHotel()) {
            return redirect('/admin');
        }
        $currentUser = auth()->user()->id;
        if (auth()->user()->isAdmin()) {
            $branches = Branch::get();
        } else {
            $branches = Branch::where('user_id', $currentUser)->get();
        }
        return view('Admin.survey.plan', compact('branches'));
    }

    public function create(Request $request)
    {
        if (!auth()->user()->isMerchant() && !auth()->user()->isHotel()) {
            return redirect('/admin');
        }
        $currentUser = auth()->user()->id;
        if (auth()->user()->isAdmin()) {
            $branches = Branch::get();
        } else {
            $branches = Branch::where('user_id', $currentUser)->get();
        }

        if ($request->tp == 'custom') {
            if (!auth()->user()->isHotel()) {
                return view('Admin.survey.create', compact('branches'));
            }else{
                $questions = Template::where('type', 'custom')->pluck('question_text')->toArray();
                return view('Admin.survey.hotel.create', compact('branches', 'questions'));
            }
        } else {
            $industries = Industry::all();
            return view('Admin.survey.template.create', compact('branches', 'industries'));
        }
    }

    public function getAll()
    {

        if(auth()->user()->isHotel()){
            $surveys = DB::table('surveys')
                        ->where('user_id', auth()->user()->id)
                        ->select('id AS sid', DB::raw("CONCAT(DATE_FORMAT(surveys.from, '%d/%m/%Y'), ' - ',DATE_FORMAT(surveys.to, '%d/%m/%Y')) AS survey_period"))->get();
            
            foreach($surveys as $key => $survey){
                $question = Question::where('survey_id', $survey->sid)->first();
                $question_texts = implode(', ', Question::where('survey_id', $survey->sid)->pluck('question_text')->toArray());
                $surveys[$key]->question_text = $question_texts;
                $surveys[$key]->id = $question->id;
            }
        }
        else{
            $surveys = DB::table('questions')
            ->where('questions.user_id', auth()->user()->id)
            ->select('questions.id', 'questions.question_text', 'branch_id', 'type',
                DB::raw("CONCAT(DATE_FORMAT(questions.from, '%d/%m/%Y'), ' - ',DATE_FORMAT(questions.to, '%d/%m/%Y')) AS survey_period"))->get();

            foreach($surveys as $key => $survey){
                $sub_branch = Question::find($survey->id)->subBranch->first();
                $branch = Branch::find($sub_branch->branch_id);
                $surveys[$key]->location = $branch->name;
            }
        }

        return DataTables::of($surveys)
            ->addColumn('action', function ($survey) {
                return
                '<a href=' . url("admin/survey/{$survey->id}/preview") . ' class="badge badge-secondary badge-pill text-black">Preview</a>
                <a href=' . url("admin/survey/{$survey->id}/edit") . ' class="badge badge-warning badge-pill text-black">Edit</a>
                <a href="#" class="badge badge-danger badge-pill text-black" id="delete-btn" data-id="' . $survey->id . '">Delete</a>
                <a href=' . url("admin/survey/{$survey->id}/qr") . ' class="badge badge-primary badge-pill text-black gen-qr" id="gen-qr-btn" data-id="' . $survey->id . '">Generate QR</a>';
            })
            ->make(true);
    }

    public function edit(Question $question)
    {
        $currentUser = auth()->user()->id;
        if (auth()->user()->isAdmin()) {
            $branches = Branch::get();
        } else {
            $branches = Branch::where('user_id', $currentUser)->get();
        }

        //     dd($preAnswers);\
        if(auth()->user()->isHotel()){
            $preQuestions = Template::where('type', 'custom')->pluck('question_text')->toArray();
            $survey = $question->survey;
            return view('Admin.survey.hotel.edit', compact('survey', 'preQuestions'));
        }else{

        }

        if ($question->type == 'template') {
            return view('Admin.survey.template.edit', compact('question', 'branches'));
        } 
        else {
                $feedbackType = '';
                $isNegativeFeedback = false;
                $isDontShow = false;
                $preAnswers = $question->preAnswer;
    
                $isAfterAllFeedback = count($preAnswers->where('feedback_type_id', null));
    
                if (count($preAnswers->where('feedback_type_id', null))) {
                    $feedbackType = 'all';
                }
    
                if (!$isAfterAllFeedback) {
                    $negative = $preAnswers->contains('feedback_type_id', 2);
                    $positive = $preAnswers->contains('feedback_type_id', 1);
    
                    if ($negative && !$positive) {
                        $feedbackType = 'negative';
                    }
                    if ($positive && $negative) {
                        $feedbackType = 'seperate';
                    }
                }
                return view('Admin.survey.edit', compact('question', 'branches', 'feedbackType'));
            
        }

    }

    public function qr(Question $question)
    {
        if(!auth()->user()->isAdmin()){
            if(auth()->user()->id != $question->user_id){
                return redirect()->back();
            }
        }

        return view('Admin.survey.qr', compact('question'));
    }
    
    public function preview(Question $question)
    {
        if (!auth()->user()->isHotel()) {
            return view('Admin.survey.preview', compact("question"));
        } 
        else {
            $survey = $question->survey;
            return view('Admin.survey.hotel.preview', compact("survey"));
        }

    }

    public function store(Request $request)
    {
        if ($request->dat != '') {
            $daterange = explode('-', $request->dat);
            $from = $daterange[0];
            $to = $daterange[1];
            $from = strtotime($from);
            $to = strtotime($to);
            $from = date('Y-m-d', $from);
            $to = date('Y-m-d', $to);
        }

        if(auth()->user()->isHotel()){
            if($request->has('survey_id')){
                $survey = Survey::find($request->survey_id);
            }
            else{
                $survey = new Survey;
                $survey->user_id = auth()->user()->id;
                $survey->from = $from;
                $survey->to = $to;
                $survey->save();
            }
            foreach($request->questionList as $text){
                $question = new Question();
                $question->question_text = $text;
                $question->survey_id = $survey->id;
                $question->from = $from;
                $question->to = $to;
                $question->user_id = auth()->user()->id;
                $question->save();
            }
        }
        else{
            $question = new Question();
            if ($request->type == "template") {
                $question->type = "template";
                $template = Template::find($request->template_id);

                $question->template_id = $request->template_id;
                $question->question_text = $template->question_text;
            } 
            else 
            {
                if ($request->primary != '') {
                    $question->question_text = $request->primary;
                    $question->language_id = $request->language;
                }
                if ($request->second != '') {
                    $question->question_text_other_language = $request->second;
                    $question->other_language_id = $request->other_lang;
                }
            }
            
            $question->from = $from;
            $question->to = $to;

            $question->user_id = auth()->user()->id;
            $question->save();
            $question_last = Question::orderBy('id', 'desc')->first();
            if ($request->subBranch != '' || $request->subBranch != null) {
                foreach ($request->subBranch as $sub) {
                    $QuestionsSub = new QuestionSubBranch();
                    $QuestionsSub->question_id = $question_last->id;
                    $QuestionsSub->sub_branch_id = $sub;
                    $QuestionsSub->save();
                }
            }

            if ($request->type == "custom") {
                if ($request->preAnsNeg != null) {
                    foreach ($request->preAnsNeg as $preNeg) {
                        $preansQuestions = new PreAnswerQuestion();
                        $preansQuestions->question_id = $question_last->id;
                        $preansQuestions->pre_answer_id = $preNeg;
                        $preansQuestions->feedback_type_id = 2;
                        $preansQuestions->save();
                    }
                }

                if ($request->preAns != null) {
                    foreach ($request->preAns as $pre) {
                        $preansQuestions = new PreAnswerQuestion();
                        $preansQuestions->question_id = $question_last->id;
                        $preansQuestions->pre_answer_id = $pre;
                        $preansQuestions->save();
                    }
                }

                if ($request->preanswerForSpNeg != null) {

                    foreach ($request->preanswerForSpNeg as $preanswerForSpN) {
                        if ($preanswerForSpN != null) {
                            $preansQuestions = new PreAnswerQuestion();
                            $preansQuestions->question_id = $question_last->id;
                            $preansQuestions->pre_answer_id = $preanswerForSpN;
                            $preansQuestions->feedback_type_id = 2;
                            $preansQuestions->save();
                        }

                    }
                }
                if ($request->preanswerForSpPos != null) {
                    foreach ($request->preanswerForSpPos as $preanswerForSpP) {
                        if ($preanswerForSpP != null) {
                            $preansQuestions = new PreAnswerQuestion();
                            $preansQuestions->question_id = $question_last->id;
                            $preansQuestions->pre_answer_id = $preanswerForSpP;
                            $preansQuestions->feedback_type_id = 1;
                            $preansQuestions->save();
                        }

                    }
                }
            }
        }
    }

    public function update(Question $question, Request $request)
    {
        if(auth()->user()->isHotel()){
            
            if($request->has('removedQuestions')){
                foreach($request->removedQuestions as $id){
                    $this->destory(Question::find($id));
                }
            }

            if($request->has('date')){
                $daterange = explode('-', $request->date);
                $from = $daterange[0];
                $to = $daterange[1];
                $from = strtotime($from);
                $to = strtotime($to);
                $from = date('Y-m-d', $from);
                $to = date('Y-m-d', $to);
                
                $survey = Survey::find($question->survey_id);
                $survey->from = $from;
                $survey->to = $to;
                $survey->save();
            }

            $question->question_text = $request->text;
            $question->from = $question->survey->from;
            $question->to = $question->survey->to;
            $question->save();
        }
        else{
            if ($request->dat != '') {
                $daterange = explode('-', $request->dat);
                $from = $daterange[0];
                $to = $daterange[1];
                $from = strtotime($from);
                $to = strtotime($to);
                $from = date('Y-m-d', $from);
                $to = date('Y-m-d', $to);
                $question->from = $from;
                $question->to = $to;
            }
            $question->subBranch()->sync($request->subBranch);

            if ($question->type == 'custom') {
                if ($request->primary) {
                    $question->question_text = $request->primary;
                    $question->language_id = $request->language;
                }
                if ($request->second) {
                    $question->question_text_other_language = $request->second;
                    $question->other_language_id = $request->other_lang;
                }

                if (!$request->preAns && !$request->preAnsNeg && !$request->preanswerForSpPos && !$request->preanswerForSpNeg) {
                    PreAnswerQuestion::where('question_id', $question->id)->delete();
                } else {
                    if ($request->preAns && is_array($request->preAns)) {
                        $updatedAns = [];
                        foreach ($request->preAns as $preAns) {
                            $updatedAns[$preAns] = [
                                'feedback_type_id' => null,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                        }
                        $question->preAnswers()->sync($updatedAns);
                    }

                    if ($request->preAnsNeg && is_array($request->preAnsNeg)) {
                        $updatedAns = [];
                        foreach ($request->preAnsNeg as $preAnsNeg) {
                            $updatedAns[$preAnsNeg] = [
                                'feedback_type_id' => 2,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                        }
                        $question->preAnswers()->sync($updatedAns);
                    }

                    if ($request->preAnsNeg && is_array($request->preAnsNeg)) {
                        $updatedAns = [];
                        foreach ($request->preAnsNeg as $preAnsNeg) {
                            $updatedAns[$preAnsNeg] = [
                                'feedback_type_id' => 2,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                        }
                        $question->preAnswers()->sync($updatedAns);
                    }

                    //    dd(array_filter($request->preanswerForSpPos));

                    if ($request->preanswerForSpPos && is_array($request->preanswerForSpPos)) {
                        $preAnsPos = array_filter($request->preanswerForSpPos);
                        PreAnswerQuestion::whereNotIn('pre_answer_id', $preAnsPos)
                            ->where('feedback_type_id', 1)
                            ->where('question_id', $question->id)->delete();

                        foreach ($preAnsPos as $preAns) {
                            if ($preAns) {
                                PreAnswerQuestion::firstOrCreate(
                                    ['feedback_type_id' => 1, 'question_id' => $question->id, 'pre_answer_id' => $preAns],
                                    ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
                                );
                            }
                        }
                    }

                    if ($request->preanswerForSpNeg && is_array($request->preanswerForSpNeg)) {
                        $preAnsNeg = array_filter($request->preanswerForSpNeg);
                        PreAnswerQuestion::whereNotIn('pre_answer_id', $preAnsPos)
                            ->where('feedback_type_id', 1)
                            ->where('question_id', $question->id)->delete();
                        foreach ($preAnsNeg as $preAns) {
                            if ($preAns) {
                                PreAnswerQuestion::firstOrCreate(
                                    ['feedback_type_id' => 2, 'question_id' => $question->id, 'pre_answer_id' => $preAns],
                                    ['created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
                                );
                            }
                        }
                    }
                }
            }

            $question->save();
        }

    }

    public function destory(Question $question)
    {
        $today = Carbon::today();
        if(auth()->user()->isHotel()){
            $survey = $question->survey;
            $isActive = Survey::where('id', $survey->id)->whereRaw('"' . $today . '" between `from` and `to`')->first();

            if($isActive){
                $hasOtherQuestion = Survey::where('id', '!=', $survey->id)->whereRaw('"' . $today . '" between `from` and `to`')->get();

                if (count($hasOtherQuestion)) {
                    foreach($survey->questions as $question){
                        $question->feedback()->delete();
                        $question->delete();
                    }
                    $survey->delete();
                } else {
                    return response([
                        "message", "Survey is already in use!. Please create new active survey to remove this survey.",
                    ], 422);
                }
            }
            else {
                foreach($survey->questions as $question){
                    $question->feedback()->delete();
                    $question->delete();
                }
                $survey->delete();
            }
        }
        else{
            $isActive = Question::where('id', $question->id)->whereRaw('"' . $today . '" between `from` and `to`')->first();

            if ($isActive) {
                $hasNewQuestion = Question::where('id', '!=', $question->id)
                    ->where('branch_id', $question->branch_id)
                    ->whereRaw('"' . $today . '" between `from` and `to`')->get();

                // dd(count($hasNewQuestion));
                if (count($hasNewQuestion)) {
                    $question->subBranch()->detach();
                    $question->feedback()->delete();
                    PreAnswerQuestion::where('question_id', $question->id)->delete();
                    $question->delete();
                } else {
                    return response([
                        "message", "Survey is already in use!. Please create new active survey to remove this survey.",
                    ], 422);
                }
            } else {
                $question->subBranch()->detach();
                $question->feedback()->delete();
                PreAnswerQuestion::where('question_id', $question->id)->delete();
                $question->delete();
            }
        }
        
    }
}
