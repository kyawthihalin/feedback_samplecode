<?php

namespace App\Http\Controllers\Api;

use App\AlertBackFeedback;
use App\Events\AlertBadFeedback;
use App\FeedBack;
use App\Http\Controllers\Controller;
use App\PreAnswer;
use App\Question;
use App\TemplatePreAnswer;
use Illuminate\Http\Request;
use Timezone;
use Validator;

class FeedBackController extends Controller
{
    // public function __construct() {
    //     $this->middleware('auth:api', ['except' => ['login']]);
    // }

    public function store(Request $request)
    {
        $request->validate([
            'isHotel' => 'required|boolean',
        ]);
        if (!$request->isHotel) {
            $validator = Validator::make($request->all(), [
                'question_id' => 'required',
                'feedback_type_id' => 'required',
                'feedback_type_id' => 'numeric|min:1|max:4',
            ], [
                'question_id.required' => 'Question id is Required',
                'feedback_type_id.required' => 'Feedback type id is required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'msg' => $validator->errors()->all()[0],
                    'code' => 422,
                ]);
            }

            $feedBack = new FeedBack();

            $feedBack->question_id = $request->question_id;
            $feedBack->branch_id = $request->branch_id ? $request->branch_id : 0;
            $feedBack->sub_branch_id = $request->sub_branch_id;
            $feedBack->feedback_type_id = $request->feedback_type_id;
            $feedBack->preanswer_id = $request->preanswer_id;
            $feedBack->other = $request->other;
            $feedBack->feedback_time = date('H:m:i');
            $feedBack->feedback_date = date('Y-m-d');

            $feedBack->save();

            if ($feedBack->feedback_type_id == 4) {
                
                $question = Question::find($request->question_id);

                $preanswerMessage = "";
                if(!$request->other){
                    if($question->type === "custom"){
                        $preanswerMessage = PreAnswer::find($request->preanswer_id)->description;
                    }
                    else if($question->type === "template"){
                        $preanswerMessage = TemplatePreAnswer::find($request->preanswer_id)->description;
                    }
                }else{
                    $preanswerMessage = $request->other;
                }
                
                $alert = new AlertBackFeedback();

                $alert->user_id = $question->user_id;
                $alert->sub_branch_id = $feedBack->sub_branch_id;
                $alert->question_id = $feedBack->question_id;
                $alert->message = $preanswerMessage;
                $alert->save();

                $alertObj = [];

                $alertObj['subbranch_name'] = optional($feedBack->subbranch)->name;
                $alertObj['question'] = $feedBack->question->question_text;
                $alertObj['date'] = Timezone::convertToLocal(($alert->created_at), 'd-m-Y');
                $alertObj['time'] = Timezone::convertToLocal(($alert->created_at), 'h:i A');
                $alertObj['user'] = $alert->user_id;

                event(new AlertBadFeedback($alertObj));
            }

            return response('', 200);
        } else {
            $request->validate([
                'survey_id' => 'required',
                'questions' => 'required|array|min:1',
            ]);

            $subQuestions = array_filter($request->questions);

            foreach ($subQuestions as $subQuestion) {
                $feedBack = new FeedBack();

                $feedBack->question_id = $subQuestion['question_id'];
                $feedBack->feedback_type_id = $subQuestion['feedback_type_id'];
                $feedBack->branch_id = 0;
                $feedBack->feedback_time = date('H:m:i');
                $feedBack->feedback_date = date('Y-m-d');

                $feedBack->save();
            }
            return response('', 200);
        }
    }
}
