<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Branch;

class BranchController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function getBranch() {
        $branchs = Branch::where('user_id', auth()->user()->id)->with('subBranch')->orderBy('name', 'asc')->get();

        $results = $this->formatBranchList($branchs);
        
        return response()->json([
            'data' => $results,
            'code' => 200,
            'success' => true
        ]);
    }

    public function formatBranchList($branchs) {
        $result = [];

        $count = 0;
        foreach($branchs as $branch) {
            $result[$count]['id'] = $branch->id;
            $result[$count]['name'] = $branch->name;
            $result[$count]['email'] = $branch->email;
            $result[$count]['public_name'] = $branch->public_name;
            $result[$count]['phone'] = $branch->phone;
            $result[$count]['subBranchs'] = $this->formatSubBranchList($branch->subBranch);

            $count++;
        }

        return $result;
    }

    public function formatSubBranchList($subBranchs) {
        $result = [];

        $count = 0;
        foreach($subBranchs as $subBranch) {
            $result[$count]['id'] = $subBranch->id;
            $result[$count]['name'] = $subBranch->name;
            $result[$count]['public_name'] = $subBranch->public_name;
            $result[$count]['active_weekly'] = $subBranch->active_weekly;
            $result[$count]['name'] = $subBranch->name;

            $count++;
        }

        return $result;
    }
}
