<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionTemplateResource;
use App\Http\Resources\SurveyHotelResource;
use App\PreAnswer;
use App\PreAnswerQuestion;
use App\SubBranch;
use App\Survey;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function getQuestion(Request $request)
    {
        if (!$request->user()->isHotel()) {
            $subBranch = SubBranch::with(['question' => function ($q) {
                $q->whereDate('to', '>=', date('Y-m-d') . ' 00:00:00')
                    ->orderBy('id', 'desc')
                    ->first();
            }])->where('status', 1)->find($request->subbranch_id);

            if (!$subBranch) {
                abort(404);
            }

            $questions = $subBranch->question;
            $questionList = null;
            $count = 0;
            foreach ($questions as $question) {

                if ($question->type === "custom") {

                    $mainLanguage = $question->mainLanguage;
                    $otherLanguage = $question->otherLanguage;
                    $positiveAns = [];
                    $positiveTypes = PreAnswerQuestion::where('question_id', $question->id)->where('feedback_type_id', 1)->get();

                    if ($positiveTypes) {
                        $positiveCount = 0;
                        foreach ($positiveTypes as $positiveType) {
                            $positive = PreAnswer::find($positiveType->pre_answer_id);
                            $positiveAns[$positiveCount]['id'] = $positive->id;
                            $positiveAns[$positiveCount]['answer'] = $positive->description;

                            $positiveCount++;
                        }
                    }

                    $negativeAns = [];
                    $negativeTypes = PreAnswerQuestion::where('question_id', $question->id)->where('feedback_type_id', 2)->get();

                    if ($negativeTypes) {
                        $negativeCount = 0;
                        foreach ($negativeTypes as $negativeType) {
                            $negative = PreAnswer::find($negativeType->pre_answer_id);
                            $negativeAns[$negativeCount]['id'] = $negative->id;
                            $negativeAns[$negativeCount]['answer'] = $negative->description;

                            $negativeCount++;
                        }
                    }

                    $bothAns = [];
                    $bothTypes = PreAnswerQuestion::where('question_id', $question->id)->where('feedback_type_id', null)->get();

                    if ($bothTypes) {
                        $bothCount = 0;
                        foreach ($bothTypes as $bothType) {
                            $both = PreAnswer::find($bothType->pre_answer_id);
                            $bothAns[$bothCount]['id'] = $both->id;
                            $bothAns[$bothCount]['answer'] = $both->description;

                            $bothCount++;
                        }
                    }

                    $questionList['id'] = $question->id;
                    $questionList['main'] = $question->question_text;
                    $questionList['other'] = $question->question_text_other_language;
                    $questionList['type'] = $question->type;
                    $questionList['preAnswer']['positive'] = $positiveAns ? $positiveAns : null;
                    $questionList['preAnswer']['negative'] = $negativeAns ? $negativeAns : null;
                    $questionList['preAnswer']['both'] = $bothAns ? $bothAns : null;
                    $questionList['language']['main'] = $mainLanguage ? $mainLanguage->name : null;
                    $questionList['language']['other'] = $otherLanguage ? $otherLanguage->name : null;

                    $count++;
                }
                else{
                    if($question){
                        return new QuestionTemplateResource($question);
                    }
                    return abort(404);
                }
            }

            return response()->json([
                'data' => $questionList,
                'code' => 200,
                'success' => true,
            ]);
        } else {

            $survey = Survey::where('user_id', $request->user()->id)->orderBy('id', 'desc')->first();

            if ($survey) {
                return new SurveyHotelResource($survey);
            }
            return abort(404);
        }
    }
}
