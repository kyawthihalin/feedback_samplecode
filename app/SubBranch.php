<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class SubBranch extends Model
{
    protected $table='sub_branches';

    public function branch(){
        return $this->belongsTo(Branch::class,'branch_id');
    }

    public function question(){
        return $this->belongsToMany(Question::class);
    }
}
